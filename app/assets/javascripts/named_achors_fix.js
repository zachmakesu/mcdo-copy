// https://github.com/turbolinks/turbolinks/issues/214#issuecomment-264535611
$(document).on('turbolinks:load', function (event) {
  // scroll to matching named element in hash
  if (window.location.hash) {
    var tags = document.getElementsByName(window.location.hash.slice(1))
    if (tags.length > 0) {
      tags[0].scrollIntoView(true)
    }
  }
})
