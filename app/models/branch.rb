# == Schema Information
#
# Table name: branches
#
#  address      :text
#  area_id      :integer
#  created_at   :datetime         not null
#  description  :text             default("")
#  id           :integer          not null, primary key
#  image_source :string
#  lat          :float
#  lng          :float
#  name         :string
#  offerings    :string
#  status       :string
#  updated_at   :datetime         not null
#
# Indexes
#
#  index_branches_on_area_id  (area_id)
#
# Foreign Keys
#
#  fk_rails_...  (area_id => areas.id)
#

# Model for Store Branches
class Branch < ApplicationRecord
  OFFERINGS = [
    "24 Hours",
    "Drive Thru",
    "Mc Cafe",
    "Dessert",
    "Party Venue",
    "Play Place"
  ].freeze

  mount_uploader :image_source, ImageUploader
  geocoded_by :address, latitude: :lat, longitude: :lng

  has_many :delivery_numbers, dependent: :destroy
  accepts_nested_attributes_for :delivery_numbers,
                                :reject_if => :all_blank,
                                :allow_destroy => true
  belongs_to :area

  validates :name, presence: true
  after_validation :geocode, if: lambda { |obj|
    obj.lat.nil? || obj.lng.nil? || obj.adddress_changed?
  }

  def self.search(keyword:)
    search_query = "name iLIKE :keyword OR address iLIKE :keyword OR"\
      " status iLIKE :keyword"
    where(search_query, keyword: "%#{keyword}%")
  end
end
