# == Schema Information
#
# Table name: carts
#
#  created_at  :datetime         not null
#  customer_id :integer
#  expires_at  :datetime
#  id          :integer          not null, primary key
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_carts_on_customer_id  (customer_id)
#
# Foreign Keys
#
#  fk_rails_...  (customer_id => customers.id)
#

# Model for ordering products
class Cart < ApplicationRecord
  belongs_to :customer

  has_one :order, dependent: :destroy
  has_many :items, class_name: "CartsVariance",
                   dependent: :destroy,
                   inverse_of: :cart
  has_many :variances, through: :items
  has_many :alacartes, through: :variances,
                       source: :variable,
                       source_type: "Alacarte"
  has_many :value_meals, through: :variances,
                         source: :variable,
                         source_type: "ValueMeal"

  after_validation :update_expiration, on: %i[create update]

  def all_items
    alacartes + value_meals
  end

  def self.active
    where("expires_at > ? and id not in (select cart_id from orders)",
          Time.zone.now).order(created_at: :desc)
  end

  def sub_total
    items.sum(&:total_price)
  end

  def total_quantity
    items.sum(&:item_options_quantity)
  end

  private

  def update_expiration
    self.expires_at = Time.zone.now + 8.hours
  end
end
