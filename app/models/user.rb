# == Schema Information
#
# Table name: users
#
#  avatar                 :string
#  created_at             :datetime         not null
#  current_sign_in_at     :datetime
#  current_sign_in_ip     :inet
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  first_name             :string           default("")
#  id                     :integer          not null, primary key
#  last_name              :string           default("")
#  last_sign_in_at        :datetime
#  last_sign_in_ip        :inet
#  middle_name            :string           default("")
#  remember_created_at    :datetime
#  reset_password_sent_at :datetime
#  reset_password_token   :string
#  salutation             :string           default("")
#  sign_in_count          :integer          default(0), not null
#  updated_at             :datetime         not null
#  user_id                :string
#
# Indexes
#
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#

# This is the basic user class associated to an account
class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :rememberable, :trackable, :validatable
  include RecordFilter
  mount_uploader :avatar, ImageUploader

  def self.new_this_month
    today = Time.zone.now
    where(created_at: today.beginning_of_month..today.end_of_month)
  end

  def full_name
    user = self
    "#{user.first_name} #{user.last_name}"
  end
end
