# == Schema Information
#
# Table name: carts_variances
#
#  best_with          :boolean          default(FALSE)
#  best_with_quantity :integer          default(0)
#  cart_id            :integer
#  created_at         :datetime         not null
#  id                 :integer          not null, primary key
#  quantity           :integer          default(1)
#  updated_at         :datetime         not null
#  variance_id        :integer
#
# Indexes
#
#  index_carts_variances_on_cart_id      (cart_id)
#  index_carts_variances_on_variance_id  (variance_id)
#
# Foreign Keys
#
#  fk_rails_...  (cart_id => carts.id)
#  fk_rails_...  (variance_id => variances.id)
#

# Joint table for carting variance
class CartsVariance < ApplicationRecord
  belongs_to :cart
  belongs_to :variance

  has_one :alacarte, through: :variance,
                     source: :variable,
                     source_type: "Alacarte"
  has_one :value_meal, through: :variance,
                       source: :variable,
                       source_type: "ValueMeal"
  has_many :cv_options, class_name: "CartsVariancesMealOption",
                        dependent: :destroy,
                        inverse_of: :carts_variance
  has_many :meal_options, through: :cv_options,
                          source: :meal_option
  validates :quantity, numericality: { greater_than_or_equal_to: 1,
                                       only_integer: true }
  validates :best_with_quantity, numericality: {
    greater_than_or_equal_to: proc { |cv| cv.best_with ? 1 : 0 },
    only_integer: true
  }
  validates :best_with, inclusion: {
    in: [true]
  }, if: -> { best_with_quantity > 0 }
  validate :variance_uniq_scope,
           :validate_value_meal_option,
           :validate_alacarte_meal_option

  def total_price
    (
      variance.price +
      cv_options.sum(&:total_price) +
      (variance.best_with_item&.price.to_i * best_with_quantity)
    ) * quantity
  end

  def item_options_quantity
    (cv_options.sum(:quantity) + 1) * quantity
  end

  def self.item_for_option(uniq_params:, poscodes:)
    includes(:meal_options).where(uniq_params).find do |v|
      v.meal_options.pluck(:poscode).sort == poscodes.sort
    end
  end

  private

  def variance_uniq_scope
    uniq = {
      variance_id: variance_id,
      cart_id: cart_id,
      best_with: best_with
    }

    cv = CartsVariance.item_for_option(
      uniq_params: uniq,
      poscodes: cv_options.map { |cvo| cvo.meal_option.poscode }
    )

    msg = "with this combination is already taken"
    errors.add(:variance, msg) if cv.present? && cv.id != id
  end

  def validate_value_meal_option
    return unless variance.value_meal? || variance.bundle?
    invalid_option = (cv_options.map(&:meal_option_id) - meal_option_ids).any?
    invalid_size = cv_options.empty?
    err_req_msg = "is required for value meal and bundle"
    err_not_assoc_msg = "is invalid and not associated with"\
      " this value meal/bundle"
    msg = invalid_size ? err_req_msg : err_not_assoc_msg
    errors.add(:meal_option, msg) if invalid_option || invalid_size
  end

  def meal_option_ids
    if variance.bundle?
      variance.variable.product_bundle.meal_options.ids
    else
      variance.meal_options.ids
    end
  end

  def validate_alacarte_meal_option
    invalid_option = variance.alacarte? &&
                     !variance.bundle? &&
                     !cv_options.empty?
    msg = "is only available for value meal and alacarte with bundle"
    errors.add(:meal_option, msg) if invalid_option
  end
end
