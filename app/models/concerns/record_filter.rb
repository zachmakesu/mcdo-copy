# Model Concern for filtering Records
module RecordFilter
  extend ActiveSupport::Concern

  class_methods do
    def new_today
      today = Time.zone.today
      where(created_at: today.midnight..today.end_of_day)
    end
  end
end
