# == Schema Information
#
# Table name: notifications
#
#  category          :string
#  created_at        :datetime         not null
#  description       :text
#  id                :integer          not null, primary key
#  image_source      :string
#  short_description :string
#  title             :string
#  updated_at        :datetime         not null
#

# Notification Model
class Notification < ApplicationRecord
  CATEGORIES = {
    default: "Default", successful_order: "Successful Order"
  }.freeze
  mount_uploader :image_source, ImageUploader

  enum category: CATEGORIES
  validates :category, presence: true,
                       inclusion: { in: CATEGORIES.keys.collect(&:to_s) }
  validates :title, presence: true
end
