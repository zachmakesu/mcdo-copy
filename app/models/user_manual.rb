# == Schema Information
#
# Table name: user_manuals
#
#  created_at   :datetime         not null
#  description  :string
#  id           :integer          not null, primary key
#  image_source :string
#  sequence_id  :integer
#  updated_at   :datetime         not null
#

# Model for User Manual
class UserManual < ApplicationRecord
  mount_uploader :image_source, ImageUploader
  acts_as_list column: :sequence_id
end
