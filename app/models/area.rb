# == Schema Information
#
# Table name: areas
#
#  created_at  :datetime         not null
#  description :text             default("")
#  id          :integer          not null, primary key
#  name        :string
#  updated_at  :datetime         not null
#

# Model for Area a Branch belongs to
class Area < ApplicationRecord
  has_many :branches, dependent: :destroy
end
