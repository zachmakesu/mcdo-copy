# == Schema Information
#
# Table name: coupon_products
#
#  coupon_id        :integer
#  created_at       :datetime         not null
#  id               :integer          not null, primary key
#  main_product_id  :integer
#  productable_id   :integer
#  productable_type :string
#  updated_at       :datetime         not null
#
# Indexes
#
#  index_coupon_products_on_coupon_id                            (coupon_id)
#  index_coupon_products_on_main_product_id                      (main_product_id)
#  index_coupon_products_on_productable_type_and_productable_id  (productable_type,productable_id)
#

# Coupon Product Model
class CouponProduct < ApplicationRecord
  has_many :sub_products, class_name: "CouponProduct",
                          foreign_key: "main_product_id",
                          dependent: :destroy
  belongs_to :main_product, class_name: "CouponProduct", optional: true
  belongs_to :coupon, optional: true
  belongs_to :productable, polymorphic: true, optional: true
  accepts_nested_attributes_for :sub_products,
                                reject_if: :all_blank,
                                allow_destroy: true
end
