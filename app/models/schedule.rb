# == Schema Information
#
# Table name: schedules
#
#  created_at :datetime         not null
#  end_at     :time
#  id         :integer          not null, primary key
#  start_at   :time
#  title      :string
#  updated_at :datetime         not null
#

# Model for ValueMeal/Alacarte schedule
class Schedule < ApplicationRecord
  has_many :value_meals, dependent: false
  has_many :alacartes, dependent: false
end
