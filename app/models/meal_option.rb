# == Schema Information
#
# Table name: meal_options
#
#  classification :string
#  code_name      :string
#  created_at     :datetime         not null
#  displayed_at   :datetime
#  group_id       :integer
#  id             :integer          not null, primary key
#  image_source   :string
#  is_default     :boolean          default(FALSE), not null
#  long_name      :string
#  name           :string
#  poscode        :string
#  price          :float
#  size           :string
#  updated_at     :datetime         not null
#

# Model for meal options
class MealOption < ApplicationRecord
  mount_uploader :image_source, ImageUploader
  SIZES = {
    small:  "Small",
    medium: "Medium",
    large:  "Large"
  }.freeze
  CLASSIFICATIONS = {
    coke_class:     "CokeGlass",
    dips:           "Dips",
    drink:          "Drink",
    sides:          "Sides",
    fries:          "Fries",
    burger:         "Burger",
    sauce:          "Sauce",
    best_with:      "Best With",
    special_fries:  "Special Fries",
    toys:           "Toys"
  }.freeze
  DEFAULT_CLASSIFICATION = "CokeGlass".freeze

  enum size: SIZES, classification: CLASSIFICATIONS
  has_many :options, class_name: "ProductOption",
                     foreign_key: "choice_id", dependent: :destroy
  has_many :coupon_products, as: :productable, dependent: :destroy

  validates :name, presence: true

  def self.search(keyword:, type:)
    search_query = "name iLIKE :keyword OR long_name iLIKE :keyword "\
                   "OR poscode iLIKE :keyword OR code_name iLIKE :keyword"
    where(classification: type)
      .where(search_query, keyword: "%#{keyword}%")
  end

  def self.fetch_by(type:, size: nil)
    arr = %W[#{type}]
    (arr << size) if size.present?
    arr.inject(self, :try)
  end
end
