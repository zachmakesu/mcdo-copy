# == Schema Information
#
# Table name: delivery_fees
#
#  created_at :datetime         not null
#  enabled    :boolean          default(TRUE)
#  id         :integer          not null, primary key
#  updated_at :datetime         not null
#

# Model for delivery fee
class DeliveryFee < ApplicationRecord
  has_many :history, class_name: "DeliveryFeeHistory", dependent: :destroy

  def self.latest
    obj = includes(:history).first
    obj.enabled ? obj.history.custom.last : obj.history.zero_fee.last
  end

  def self.latest_fee
    latest.value
  end
end
