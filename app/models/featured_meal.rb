# == Schema Information
#
# Table name: featured_meals
#
#  created_at       :datetime         not null
#  featureable_id   :integer
#  featureable_type :string
#  id               :integer          not null, primary key
#  updated_at       :datetime         not null
#
# Indexes
#
#  index_featured_meals_on_featureable_type_and_featureable_id  (featureable_type,featureable_id)
#

# Featured Meal Model
class FeaturedMeal < ApplicationRecord
  TYPES = %w[ValueMeal Alacarte].freeze
  scope :filter_by, ->(type) { where(featureable_type: type) }
  belongs_to :featureable, polymorphic: true
end
