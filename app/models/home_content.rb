# == Schema Information
#
# Table name: home_contents
#
#  availability     :string
#  created_at       :datetime         not null
#  groupable_id     :integer
#  groupable_type   :string
#  id               :integer          not null, primary key
#  image_source_app :string
#  image_source_web :string
#  name             :string
#  sequence_id      :integer
#  updated_at       :datetime         not null
#
# Indexes
#
#  index_home_contents_on_groupable_type_and_groupable_id  (groupable_type,groupable_id)
#

# Home Content Model
class HomeContent < ApplicationRecord
  mount_uploader :image_source_app, ImageUploader
  mount_uploader :image_source_web, ImageUploader
  acts_as_list column: :sequence_id

  belongs_to :groupable, polymorphic: true
end
