# == Schema Information
#
# Table name: categories
#
#  category_id :integer
#  created_at  :datetime         not null
#  id          :integer          not null, primary key
#  is_active   :boolean          default(FALSE), not null
#  name        :string
#  sequence_id :integer
#  updated_at  :datetime         not null
#

# Category Model
class Category < ApplicationRecord
  acts_as_list column: :sequence_id
  validates :name, uniqueness: true, presence: true
  has_many :product_categories, dependent: :destroy
  has_many :value_meals, through: :product_categories, source: :categorizeable,
                         source_type: "ValueMeal"
  has_many :alacartes, through: :product_categories, source: :categorizeable,
                       source_type: "Alacarte"
  has_many :home_contents, as: :groupable, dependent: :destroy
  scope :fries_and_float, -> { find_by(name: "Fries 'N McFloat Combos") }
  scope :burger, -> { find_by(name: "Burger") }
  scope :chicken, -> { find_by(name: "Chicken") }
  scope :mcsaver, -> { find_by(name: "McSaver Meals") }
  scope :alacarte, -> { find_by(name: "A la carte") }
  scope :happy_meal, -> { find_by(name: "Happy Meals") }
  scope :mcshare, -> { find_by(name: "McShare Box") }
  scope :drink, -> { find_by(name: "Drink") }
  scope :extra_value_meal, -> { find_by(name: "Extra Value Meal") }
  scope :add_ons, -> { find_by(name: "Add ons") }
  scope :breakfast, -> { find_by(name: "Breakfast") }
  scope :dessert, -> { find_by(name: "Dessert") }
end
