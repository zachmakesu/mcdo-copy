# == Schema Information
#
# Table name: delivery_numbers
#
#  branch_id  :integer
#  created_at :datetime         not null
#  id         :integer          not null, primary key
#  number     :string
#  updated_at :datetime         not null
#
# Indexes
#
#  index_delivery_numbers_on_branch_id  (branch_id)
#
# Foreign Keys
#
#  fk_rails_...  (branch_id => branches.id)
#

# Model for Branch's Delivery Number
class DeliveryNumber < ApplicationRecord
  belongs_to :branch
end
