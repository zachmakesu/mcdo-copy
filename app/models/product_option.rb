# == Schema Information
#
# Table name: product_options
#
#  choice_id  :integer
#  created_at :datetime         not null
#  id         :integer          not null, primary key
#  meal_id    :integer
#  meal_type  :string
#  section_id :integer
#  updated_at :datetime         not null
#

#  == Schema Information
#
# Table name: product_options
#
#  choice_id  :integer
#  created_at :datetime         not null
#  id         :integer          not null, primary key
#  meal_id    :integer
#  meal_type  :string
#  section_id :integer
#  size       :string
#  updated_at :datetime         not null
#

# Model for Meal Options
class ProductOption < ApplicationRecord
  belongs_to :meal, polymorphic: true
  belongs_to :section
  belongs_to :meal_option, foreign_key: "choice_id"
end
