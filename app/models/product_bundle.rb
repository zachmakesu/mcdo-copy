# == Schema Information
#
# Table name: product_bundles
#
#  alacarte_id :integer
#  created_at  :datetime         not null
#  id          :integer          not null, primary key
#  max_drinks  :integer          default(0), not null
#  max_fries   :integer          default(0), not null
#  max_rice    :integer          default(0), not null
#  updated_at  :datetime         not null
#

# Product Bundle Model
class ProductBundle < ApplicationRecord
  validates :max_drinks, :max_fries, :max_rice,
            presence: true,
            numericality: { greater_than_or_equal_to: 0 }
  belongs_to :alacarte
  has_many :options, class_name: "ProductOption", as: :meal,
                     inverse_of: :meal, dependent: :destroy
  has_many :meal_options, through: :options, source: :meal_option
end
