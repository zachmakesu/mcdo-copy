# == Schema Information
#
# Table name: favorite_meals
#
#  created_at  :datetime         not null
#  customer_id :integer
#  id          :integer          not null, primary key
#  meal_id     :integer
#  meal_type   :string
#  updated_at  :datetime         not null
#

# Model for customer's favorite meals
class FavoriteMeal < ApplicationRecord
  validates_uniqueness_of :meal_id,
                          scope: %i[customer_id meal_type]
  belongs_to :meal, polymorphic: true
  MEAL_TYPES = %w[ValueMeal Alacarte].freeze
  belongs_to :customer
end
