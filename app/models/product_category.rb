# == Schema Information
#
# Table name: product_categories
#
#  categorizeable_id   :integer
#  categorizeable_type :string
#  category_id         :integer
#  created_at          :datetime         not null
#  id                  :integer          not null, primary key
#  updated_at          :datetime         not null
#

# Product Category Model
class ProductCategory < ApplicationRecord
  # Nested Attributes for new records wont save unless `optional: true`
  # https://github.com/rails/rails/issues/29781
  belongs_to :categorizeable, polymorphic: true, optional: true
  belongs_to :category
  validates_uniqueness_of :category_id,
                          scope: %i[categorizeable_id categorizeable_type]
end
