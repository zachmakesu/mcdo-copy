# == Schema Information
#
# Table name: carts_variances_meal_options
#
#  carts_variance_id :integer
#  created_at        :datetime         not null
#  id                :integer          not null, primary key
#  meal_option_id    :integer
#  quantity          :integer          default(1)
#  updated_at        :datetime         not null
#
# Indexes
#
#  index_carts_variances_meal_options_on_carts_variance_id  (carts_variance_id)
#  index_carts_variances_meal_options_on_meal_option_id     (meal_option_id)
#
# Foreign Keys
#
#  fk_rails_...  (carts_variance_id => carts_variances.id)
#  fk_rails_...  (meal_option_id => meal_options.id)
#

# Joint table for carts_variances and meal_options for each variance
class CartsVariancesMealOption < ApplicationRecord
  belongs_to :carts_variance
  belongs_to :meal_option

  validates :meal_option, uniqueness: { scope: :carts_variance }

  def total_price
    meal_option.price * quantity
  end
end
