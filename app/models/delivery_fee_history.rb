# == Schema Information
#
# Table name: delivery_fee_histories
#
#  created_at      :datetime         not null
#  delivery_fee_id :integer
#  id              :integer          not null, primary key
#  label           :integer          default("custom")
#  updated_at      :datetime         not null
#  value           :float            default(0.0)
#
# Indexes
#
#  index_delivery_fee_histories_on_delivery_fee_id  (delivery_fee_id)
#
# Foreign Keys
#
#  fk_rails_...  (delivery_fee_id => delivery_fees.id)
#

# History model for delivery fee
class DeliveryFeeHistory < ApplicationRecord
  LABELS = %w[custom zero_fee].freeze

  enum label: LABELS

  belongs_to :delivery_fee
  has_many :orders, dependent: false

  validates :value, presence: true
  validates :label, inclusion: { in: LABELS }
  validates :label, uniqueness: true, if: -> { zero_fee? }
  validates :value, numericality: { greater_than: 0 }, if: -> { custom? }
end
