# == Schema Information
#
# Table name: value_meals
#
#  active_at       :datetime
#  alacarte_id     :integer
#  availability    :string
#  category_id     :integer
#  created_at      :datetime         not null
#  discounted_rice :boolean
#  displayed_at    :datetime
#  id              :integer          not null, primary key
#  image_source    :string
#  is_breakfast    :boolean
#  legacy_sections :string
#  name            :string
#  order_id        :integer
#  schedule_id     :integer
#  sequence_id     :integer
#  updated_at      :datetime         not null
#

# Model for value meals
class ValueMeal < ApplicationRecord
  mount_uploader :image_source, ImageUploader
  DEFAULT_MENU_ENTRY_CATEGORY = "mcsaver".freeze
  acts_as_list column: :sequence_id

  belongs_to :category, optional: true
  belongs_to :alacarte, optional: true
  belongs_to :schedule, foreign_key: "schedule_id",
                        inverse_of: :value_meals
  has_many :variances, as: :variable,
                       dependent: :destroy,
                       inverse_of: :variable
  has_many :product_categories, as: :categorizeable,
                                dependent: :destroy,
                                inverse_of: :categorizeable
  has_many :categories, through: :product_categories
  has_many :coupon_products, as: :productable,
                             dependent: :destroy,
                             inverse_of: :productable
  has_many :favorite_meals, as: :meal,
                            inverse_of: :meal,
                            dependent: :destroy
  has_one :featured_meal, as: :featureable,
                          dependent: :destroy,
                          inverse_of: :featureable
  accepts_nested_attributes_for :variances,
                                reject_if: :all_blank,
                                allow_destroy: true
  accepts_nested_attributes_for :product_categories,
                                reject_if: :all_blank,
                                allow_destroy: true

  def self.search(keyword:, category:)
    fetched_category = fetch_category(category: category)
    where(category: fetched_category&.id)
      .where("name iLIKE :keyword", keyword: "%#{keyword}%")
  end

  def self.sort_by(category:)
    fetched_category = fetch_category(category: category&.to_sym)
    where(category_id: fetched_category&.id)
  end

  def self.fetch_category(category:)
    Category.send(category)
  rescue NoMethodError
    nil
  end

  def activate!
    update(active_at: Time.zone.now)
  end

  def deactivate!
    update(active_at: nil)
  end

  def bundle?
    false
  end
end
