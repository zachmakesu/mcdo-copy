# == Schema Information
#
# Table name: news
#
#  category     :integer
#  created_at   :datetime         not null
#  description  :text
#  displayed_at :datetime
#  id           :integer          not null, primary key
#  image_source :string
#  start_date   :datetime
#  title        :string
#  updated_at   :datetime         not null
#

# Model for News and Updates
class News < ApplicationRecord
  mount_uploader :image_source, ImageUploader
  enum category: { newsfeed: 0, promo: 1 }
  validates :title, :description, :image_source,
            :start_date, :category, presence: true

  def displayed?
    displayed_at.present?
  end
end
