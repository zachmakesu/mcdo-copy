# == Schema Information
#
# Table name: promos
#
#  category          :string
#  content           :text
#  created_at        :datetime         not null
#  id                :integer          not null, primary key
#  image_source      :string
#  is_active         :boolean          default(FALSE), not null
#  is_published      :boolean          default(FALSE), not null
#  link              :string
#  name              :string
#  tags              :string
#  updated_at        :datetime         not null
#  validity_end_at   :datetime
#  validity_start_at :datetime
#

# Promo Model
class Promo < ApplicationRecord
  mount_uploader :image_source, ImageUploader
  CATEGORIES = { regular: "Regular", breakfast: "Breakfast" }.freeze
  enum category: CATEGORIES

  validates :name, presence: true, uniqueness: true
  validates :category, presence: true

  def self.active_scope(active:)
    active.nil? ? all : where(is_active: active)
  end

  def self.published_scope(published:)
    published.nil? ? all : where(is_published: published)
  end
end
