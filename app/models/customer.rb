# == Schema Information
#
# Table name: customers
#
#  cds_id     :integer
#  created_at :datetime         not null
#  id         :integer          not null, primary key
#  updated_at :datetime         not null
#

# Customer Model
class Customer < ApplicationRecord
  validates :cds_id, presence: true, uniqueness: true
  has_many :favorite_meals, dependent: :destroy
  has_many :favorite_alacartes, through: :favorite_meals,
                                source: :meal,
                                source_type: "Alacarte"
  has_many :favorite_value_meals, through: :favorite_meals,
                                  source: :meal,
                                  source_type: "ValueMeal"
  has_many :carts, dependent: :destroy
  has_many :orders, through: :carts, source: :order
  has_one :active_cart, -> { active }, class_name: "Cart",
                                       inverse_of: :customer
end
