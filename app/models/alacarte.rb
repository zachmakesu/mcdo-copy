# == Schema Information
#
# Table name: alacartes
#
#  best_with        :integer
#  category_id      :integer
#  created_at       :datetime         not null
#  description      :text
#  discounted_rice  :boolean
#  displayed_at     :datetime
#  group_id         :integer
#  id               :integer          not null, primary key
#  image_source     :string
#  legacy_sections  :string
#  name             :string
#  product_order_id :integer
#  schedule_id      :integer
#  sequence_id      :integer
#  updated_at       :datetime         not null
#

# Model for alacarte meals
class Alacarte < ApplicationRecord
  mount_uploader :image_source, ImageUploader
  acts_as_list column: :sequence_id

  belongs_to :category, optional: true
  belongs_to :schedule, foreign_key: "schedule_id",
                        inverse_of: :alacartes
  has_many :variances, as: :variable,
                       dependent: :destroy,
                       inverse_of: :variable
  has_many :value_meals, dependent: :destroy
  has_many :product_categories, as: :categorizeable,
                                dependent: :destroy,
                                inverse_of: :categorizeable
  has_many :categories, through: :product_categories
  has_many :favorite_meals, as: :meal,
                            inverse_of: :meal,
                            dependent: :destroy
  has_many :coupon_products, as: :productable,
                             dependent: :destroy,
                             inverse_of: :productable
  has_one :product_bundle, dependent: :destroy
  has_many :upsells, dependent: :destroy
  has_one :featured_meal, as: :featureable,
                          dependent: :destroy,
                          inverse_of: :featureable
  accepts_nested_attributes_for :variances,
                                reject_if: :all_blank,
                                allow_destroy: true
  accepts_nested_attributes_for :product_categories,
                                reject_if: :all_blank,
                                allow_destroy: true

  def self.search(keyword:)
    where("name iLIKE :keyword", keyword: "%#{keyword}%")
  end

  def bundle?
    product_bundle.present?
  end
end
