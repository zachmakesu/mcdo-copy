# == Schema Information
#
# Table name: coupons
#
#  classification      :string
#  created_at          :datetime         not null
#  description         :text
#  detail_image_source :string
#  discount            :float
#  id                  :integer          not null, primary key
#  image_source        :string
#  name                :string
#  poscode             :string
#  updated_at          :datetime         not null
#  usage_count         :integer
#  usage_type          :string
#  validity_end_at     :datetime
#  validity_start_at   :datetime
#

# Coupon Model
class Coupon < ApplicationRecord
  CLASSIFICATIONS = {
    delivery_only: "delivery",
    store_only:    "store",
    both:          "both"
  }.freeze

  TYPES = {
    single:     "single",
    unlimited:  "unlimited",
    limited:    "limited"
  }.freeze

  mount_uploader :image_source, ImageUploader
  mount_uploader :detail_image_source, ImageUploader

  enum usage_type: TYPES, classification: CLASSIFICATIONS

  has_many :products, class_name: "CouponProduct", dependent: :destroy
  has_many :sub_products, through: :products, source: :sub_products

  accepts_nested_attributes_for :products,
                                reject_if: :all_blank,
                                allow_destroy: true
  validates :name, presence: true
  def self.search(keyword:)
    where("name iLIKE :keyword", keyword: "%#{keyword}%")
  end
end
