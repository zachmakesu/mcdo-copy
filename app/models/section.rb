# == Schema Information
#
# Table name: sections
#
#  created_at :datetime         not null
#  id         :integer          not null, primary key
#  title      :string
#  updated_at :datetime         not null
#

# Model for Meal Sections
class Section < ApplicationRecord
  CATEGORIES = %w[
    beverage
    burger
    dessert
    fries
    sauce
    best_with
    special_fries
    sides
  ].freeze
  CATEGORIES.each do |category|
    title = category.tr("_", " ").titleize
    scope category.to_sym, -> { find_by(title: title) }
  end
  validates :title, presence: true, uniqueness: true
  has_many :product_options, dependent: :destroy
end
