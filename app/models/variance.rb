# == Schema Information
#
# Table name: variances
#
#  best_with     :integer
#  code_name     :string
#  created_at    :datetime         not null
#  group_id      :integer
#  id            :integer          not null, primary key
#  image_source  :string
#  menu_name     :string
#  name          :string
#  poscode       :string
#  price         :float
#  updated_at    :datetime         not null
#  variable_id   :integer
#  variable_type :string
#

# Class for specifying product varianaces i.e. differences in sizes
class Variance < ApplicationRecord
  include RecordFilter
  TYPES = %w[value_meal alacarte].freeze

  mount_uploader :image_source, ImageUploader

  belongs_to :variable, polymorphic: true
  has_many :options, class_name: "ProductOption",
                     as: :meal,
                     inverse_of: :meal,
                     dependent: :destroy
  has_many :meal_options, through: :options,
                          source: :meal_option
  has_many :carts_variances, dependent: :destroy
  accepts_nested_attributes_for :options,
                                reject_if: :all_blank,
                                allow_destroy: true

  def self.search(keyword:)
    query = "name iLIKE :keyword OR menu_name iLIKE :keyword "\
      "OR poscode iLIKE :keyword OR code_name iLIKE :keyword"
    where(query, keyword: "%#{keyword}%")
  end

  TYPES.each do |t|
    define_method "#{t}?" do
      variable_type.underscore == t
    end
  end

  def bundle?
    alacarte? ? variable.product_bundle.present? : false
  end

  def best_with_item
    Variance.find_by(poscode: best_with)
  end
end
