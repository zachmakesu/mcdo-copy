# == Schema Information
#
# Table name: upsells
#
#  alacarte_id :integer
#  created_at  :datetime         not null
#  id          :integer          not null, primary key
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_upsells_on_alacarte_id  (alacarte_id)
#

# Upsell Model
class Upsell < ApplicationRecord
  belongs_to :product, foreign_key: :alacarte_id,
                       class_name: "Alacarte", optional: true

  def self.search(keyword:)
    query = "alacartes.name iLIKE :keyword OR "\
      "alacartes.description iLIKE :keyword "
    joins(:product).where(query, keyword: "%#{keyword}%")
  end
end
