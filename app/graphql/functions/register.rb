# GraphQL function for handling user registration
class Functions::Register < GraphQL::Function
  description "Mutation for user registration"
  argument :credentials,
           !Types::InputObjects::AccountCredentialsInputType
  argument :profile,
           !Types::InputObjects::ProfileInputType
  argument :address, !Types::InputObjects::UserAddressInputType
  argument :contactNumber,
           !Types::InputObjects::UserContactNumberInputType

  type do
    name "RegisterPayload"
    description "Returns a user_id for successful registration"
    field :userId, !types.String,
          description: "ID to reference current user",
          hash_key: "user_id"
  end

  def call(_obj, args, _ctx)
    register = user_register(credentials: args[:credentials])
    unless register.response.success?
      raise CDS::API::Error, register.response.error
    end
    user = register.response.object
    update_details_of(user: user, args: args)
    valid_response_with(user: user)
  end

  private

  def user_register(credentials:)
    api = CDS::API::SimpleUserRegister.new(credentials: credentials)
    CDS_API_REQUEST.new(api: api).tap(&:run)
  end

  def update_details_of(user:, args:)
    api = CDS::API::UserProfileUpdate.new(user_id: user.user_id,
                                          profile: args[:profile])
    CDS_API_REQUEST.new(api: api).tap(&:run)
    register_primary_address(user: user, address: args[:address])
    register_contact_number(user: user, number: args[:contactNumber])
  end

  def register_primary_address(user:, address:)
    api = CDS::API::UserAddressAdd.new(user_id: user.user_id,
                                       addresses: [address])
    CDS_API_REQUEST.new(api: api).tap(&:run)
  end

  def register_contact_number(user:, number:)
    api = CDS::API::ContactNumberAdd.new(user_id: user.user_id,
                                         contact_numbers: [number])
    CDS_API_REQUEST.new(api: api).tap(&:run)
  end

  def valid_response_with(user:)
    OpenStruct.new(
      user_id: user.user_id
    )
  end
end
