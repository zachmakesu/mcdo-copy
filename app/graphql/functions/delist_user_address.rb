# GraphQL function for handling user new address
class Functions::DelistUserAddress < GraphQL::Function
  description "Mutation for delisting user address"
  argument :id, !types.ID

  type do
    name "DelistUserAddressPayload"
    description "Returns profile details"
    field :me, !Types::UserProfileType,
          description: "Returns the details of currently authenticated user"
  end

  # rubocop:disable Metrics/AbcSize
  def call(obj, args, ctx)
    r = lambda do |_o, a, c|
      delist_address = delist_address_for(user: c[:current_user],
                                          id: a[:id])
      unless delist_address.response.success?
        raise CDS::API::Error, delist_address.response.error
      end
      profile = get_profile_details_of(user: c[:current_user])
      user_det = profile.response.object.profile
      valid_response_with(user: user_det)
    end
    Resolvers::AuthRequiredResolver.new(r).call(obj, args, ctx)
  end
  # rubocop:enable Metrics/AbcSize

  private

  def delist_address_for(user:, id:)
    api = CDS::API::UserAddressDelist.new(user_id: user.cds_id,
                                          ids: [id])
    CDS_API_REQUEST.new(api: api).tap(&:run)
  end

  def get_profile_details_of(user:)
    api = CDS::API::UserProfileGet.new(user_id: user.cds_id)
    profile = CDS_API_REQUEST.new(api: api).tap(&:run)
    unless profile.response.success?
      raise CDS::API::Error, profile.response.error
    end
    profile
  end

  def valid_response_with(user:)
    OpenStruct.new(
      me: user
    )
  end
end
