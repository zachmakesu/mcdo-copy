# GraphQL function for removing favorite meal of user
class Functions::RemoveUserFavoriteMeal < GraphQL::Function
  description "Mutation removing favorite meal of user"
  argument :mealId, !types.ID
  argument :mealType, !types.String
  type do
    name "RemoveUserFavoriteMealPayload"
    description "Returns customer's favoriteMeals"
    field :favoriteMeals, !types[Types::CustomerFavoriteMealType],
          property: :favorite_meals
  end

  def call(obj, args, ctx)
    r = lambda do |_o, a, c|
      customer = c[:current_user]
      meal = customer.favorite_meals.find_by!(meal_id: a[:mealId],
                                              meal_type: a[:mealType])
      meal.destroy
      c[:current_user]
    end
    Resolvers::AuthRequiredResolver.new(r).call(obj, args, ctx)
  end
end
