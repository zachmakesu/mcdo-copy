# GraphQL function for reusing search
class Functions::GetPromos < GraphQL::Function
  def initialize
  end

  argument :active, types.Boolean
  argument :published, types.Boolean
  def call(_obj, args, _ctx)
    Promo.active_scope(active: args[:active])
         .published_scope(published: args[:published])
  end
end
