# GraphQL function for handling user contact number update
class Functions::UpdateUserContactNumber < GraphQL::Function
  description "Mutation for contact number update"
  argument :contactNumbers,
           !types[Types::InputObjects::UserContactNumberInputType]

  type do
    name "UpdateUserContactNumberPayload"
    description "Returns profile details"
    field :me, !Types::UserProfileType,
          description: "Returns the details of currently authenticated user"
  end

  # rubocop:disable Metrics/AbcSize
  def call(obj, args, ctx)
    r = lambda do |_o, a, c|
      update_number = update_number_of(user: c[:current_user],
                                       numbers: a[:contactNumbers])
      unless update_number.response.success?
        raise CDS::API::Error, update_number.response.error
      end
      profile_response = get_profile_details_of(user: c[:current_user])
      user_det = profile_response.response.object.profile
      valid_response_with(user: user_det)
    end
    Resolvers::AuthRequiredResolver.new(r).call(obj, args, ctx)
  end
  # rubocop:enable Metrics/AbcSize

  private

  def update_number_of(user:, numbers:)
    api = CDS::API::ContactNumberUpdate.new(user_id: user.cds_id,
                                            numbers: numbers)
    CDS_API_REQUEST.new(api: api).tap(&:run)
  end

  def get_profile_details_of(user:)
    api = CDS::API::UserProfileGet.new(user_id: user.cds_id)
    CDS_API_REQUEST.new(api: api).tap(&:run)
  end

  def valid_response_with(user:)
    OpenStruct.new(
      me: user
    )
  end
end
