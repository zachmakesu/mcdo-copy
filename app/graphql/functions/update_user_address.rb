# GraphQL function for handling user address update
class Functions::UpdateUserAddress < GraphQL::Function
  description "Mutation for address update"
  argument :address,
           !types[Types::InputObjects::UserAddressInputType]

  type do
    name "UpdateUserAddressPayload"
    description "Returns profile details"
    field :me, !Types::UserProfileType,
          description: "Returns the details of currently authenticated user"
  end

  # rubocop:disable Metrics/AbcSize
  def call(obj, args, ctx)
    r = lambda do |_o, a, c|
      update_address = update_address_of(user: c[:current_user],
                                         addresses: a[:address])
      unless update_address.response.success?
        raise CDS::API::Error, update_address.response.error
      end
      profile_response = get_profile_details_of(user: c[:current_user])
      user_det = profile_response.response.object.profile
      valid_response_with(user: user_det)
    end
    Resolvers::AuthRequiredResolver.new(r).call(obj, args, ctx)
  end
  # rubocop:enable Metrics/AbcSize

  private

  def update_address_of(user:, addresses:)
    api = CDS::API::UserAddressUpdate.new(user_id: user.cds_id,
                                          addresses: addresses)
    CDS_API_REQUEST.new(api: api).tap(&:run)
  end

  def get_profile_details_of(user:)
    api = CDS::API::UserProfileGet.new(user_id: user.cds_id)
    CDS_API_REQUEST.new(api: api).tap(&:run)
  end

  def valid_response_with(user:)
    OpenStruct.new(
      me: user
    )
  end
end
