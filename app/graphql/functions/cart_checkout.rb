# A function for handling check out
class Functions::CartCheckout < GraphQL::Function
  using ::Refinements::HashExt
  description "AuthRequired: Mutation for checking out the cart"

  argument :customerInfo, !Types::InputObjects::CustomerInfoInputType
  argument :deliveryAddress, !Types::InputObjects::DeliveryAddressInputType

  type do
    name "CheckOutPayload"
    description "return check out info"
    field :orderId,           !types.ID,      hash_key: "order_id"
    field :deliveryAddressId, !types.ID,      hash_key: "delivery_address_id"
    field :referenceNumber,   types.String,   hash_key: "reference_number"
    field :paymentType,       !types.String,  hash_key: "payment_type"
  end

  def call(obj, args, ctx)
    Resolvers::AuthRequiredResolver.new(resolve).call(obj, args, ctx)
  end

  private

  def process_checkout
    api = CDS::API::CartCheckout.new(params: @params)
    CDS_API_REQUEST.new(api: api).tap do |checkout|
      checkout.run
      unless checkout.response.success?
        raise CDS::API::Error, checkout.response.error
      end
      @cds_order_id = checkout.response.object.order_id
    end
  end

  def cart_find_or_no_available
    @cart = @customer.active_cart
    no_active_cart_found if @cart.nil?
  end

  def no_active_cart_found
    raise(
      ActiveRecord::RecordNotFound,
      "Can't find active cart for customer with id #{@customer.id}"
    )
  end

  def hash_items
    @cart.items.each_with_object(order_items: []) do |item, obj|
      obj[:order_items] = [{
        poscode:  item.variance.poscode,
        name:     item.variance.variable.name,
        qty:      item.quantity
      }].push(*array_hash_option(item: item))
    end
  end

  def array_hash_option(item:)
    item.cv_options.map do |cvo|
      {
        poscode:  cvo.meal_option.poscode,
        name:     cvo.meal_option.name,
        qty:      cvo.quantity * item.quantity
      }
    end
  end

  def formatted_args
    @args.to_h.deep_transform_keys! { |h| h.underscore.to_sym }
  end

  def build_params
    @params = formatted_args.tap do |h|
      h[:customer_info].merge!(
        user_id: @customer.cds_id,
        tendered_amount: (@cart.sub_total + DeliveryFee.latest_fee).to_s
      )

      if h[:customer_info][:payment_type] == "CC"
        h[:customer_info][:client_reference_number] = reference_number
      end
    end
  end

  def reference_number
    loop do
      reference_number = SecureRandom.hex(16)
      ref_exists = Order.exists?(reference_number: reference_number)
      break reference_number unless ref_exists
    end
  end

  def create_order
    params = @params.to_deep_open_struct
    @cart.create_order!(
      payment_type: params.customer_info.payment_type,
      reference_number: params.customer_info.client_reference_number,
      delivery_address_id: params.delivery_address.id,
      order_id: @cds_order_id,
      delivery_fee_history_id: DeliveryFee.latest.id,
      platform: @platform
    )
  end

  def resolve
    lambda do |_obj, args, ctx|
      @customer = ctx[:current_user]
      @args     = args
      @platform = ctx[:browser].platform.name

      cart_find_or_no_available

      build_params

      @params.merge!(hash_items)

      process_checkout

      create_order
    end
  end
end
