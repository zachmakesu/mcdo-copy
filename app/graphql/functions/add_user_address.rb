# GraphQL function for handling user new address
class Functions::AddUserAddress < GraphQL::Function
  description "Mutation for adding user address"
  argument :addresses,
           !types[Types::InputObjects::UserAddressInputType]

  type do
    name "AddUserAddressPayload"
    description "Returns profile details"
    field :me, !Types::UserProfileType,
          description: "Returns the details of currently authenticated user"
  end

  # rubocop:disable Metrics/AbcSize
  def call(obj, args, ctx)
    r = lambda do |_o, a, c|
      add_address = add_address_for(user: c[:current_user],
                                    addresses: a[:addresses])
      unless add_address.response.success?
        raise CDS::API::Error, add_address.response.error
      end
      profile_response = get_profile_details_of(user: c[:current_user])
      user_det = profile_response.response.object.profile
      valid_response_with(user: user_det)
    end
    Resolvers::AuthRequiredResolver.new(r).call(obj, args, ctx)
  end
  # rubocop:enable Metrics/AbcSize

  private

  def add_address_for(user:, addresses:)
    api = CDS::API::UserAddressAdd.new(user_id: user.cds_id,
                                       addresses: addresses)
    CDS_API_REQUEST.new(api: api).tap(&:run)
  end

  def get_profile_details_of(user:)
    api = CDS::API::UserProfileGet.new(user_id: user.cds_id)
    CDS_API_REQUEST.new(api: api).tap(&:run)
  end

  def valid_response_with(user:)
    OpenStruct.new(
      me: user
    )
  end
end
