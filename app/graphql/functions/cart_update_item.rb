# GraphQL function for adding items to cart
# rubocop:disable Metrics/ClassLength
# TODO: Move this function into spec for testing and optimization
class Functions::CartUpdateItem < GraphQL::Function
  description "AuthRequired: Mutation for adding items to cart"

  argument :item, !Types::InputObjects::CartItemInputType

  type do
    name "CartUpdateItemPayload"
    description "Return cart info"
    field :id,          types.Int, description: "Item id"
    field :totalPrice, types.Float do
      description "total price of all items"
      property :total_price
    end
    field :quantity, types.Int, description: "Quantity of item"
    field :options, types[Types::CvMealOptionType] do
      description "Meal options"
      property :cv_options
    end
    field :bestWithQuantity, types.Int do
      description "Quantity of best with item"
      property :best_with_quantity
    end
    field :variance, Types::VarianceType, description: "Variance of item"
    field :bestWithItem, Types::VarianceType do
      description "Best with item"
      resolve ->(obj, _args, _ctx) { obj.variance.best_with_item }
    end
    field :isRemoved, types.Boolean do
      description "Check if item is removed"
      resolve ->(obj, _args, _ctx) { obj.destroyed? }
    end
  end

  def call(obj, args, ctx)
    Resolvers::AuthRequiredResolver.new(resolve).call(obj, args, ctx)
  end

  private

  def resolve
    lambda do |_obj, args, ctx|
      initialize_variables(args: args, ctx: ctx)
      find_or_build_item

      ActiveRecord::Base.transaction do
        @item.cv_options.destroy_all if @item.variance_id_changed?
        manipulate_item_meal_options
        finalize_item
      end
      @item.reload
    end
  end

  def initialize_variables(args:, ctx:)
    @params   = args.item.to_h.deep_transform_keys(&:underscore)
    @variance = Variance.find_by!(poscode: @params["variance_poscode"])
    @cart     = ctx[:current_user].active_cart
    @cart ||= ctx[:current_user].create_active_cart
  end

  def find_or_build_item
    # check if params id for cart item is existing and if item can be found
    # return item
    if @params.key?("id") && (@item = @cart.items.find_by(id: @params["id"]))
      return @item.tap { |i| i.variance_id = @variance.id }
    end

    # get item with meal options
    build_item_with_options
  end

  def build_item_with_options
    # call CartsService instance method to get item base
    # on uniq params and meal_option_ids
    @item = @cart.items.item_for_option(
      uniq_params: uniq_params,
      poscodes: meal_option_poscodes
    )

    @item ||= @cart.items.build(item_attrs)
    @item.tap { |i| i.variance_id = @variance.id }
  end

  def uniq_params
    {
      variance_id: @variance.id,
      best_with: @params["best_with_quantity"] > 0
    }
  end

  def meal_option_poscodes
    @params["meal_options"].map do |m|
      m["poscode"] if m["quantity"] > 0
    end.compact
  end

  def manipulate_item_meal_options
    # create or update meal_options per item
    @item.tap do |item|
      cleanup_options_for(item: item)
      @params["meal_options"].each do |mo|
        next unless (meal_option = MealOption.find_by(poscode: mo["poscode"]))
        cvo = item.cv_options.find_or_initialize_by(
          meal_option_id: meal_option.id
        )
        cvo.quantity = mo["quantity"]
        cvo.save
      end
    end
  end

  def cleanup_options_for(item:)
    item.cv_options.map do |cvo|
      cvo.destroy unless meal_option_poscodes.any?(cvo.meal_option.poscode)
    end
  end

  def finalize_item
    @item.assign_attributes(item_attrs)

    if @params["quantity"].to_i == 0
      @item.destroy
    else
      @item.best_with = @params["best_with_quantity"] > 0
      @item.tap(&:save!)
    end
  end

  def item_attrs
    @params.except("id", "meal_options", "variance_poscode")
  end
end
# rubocop:enable Metrics/ClassLength
