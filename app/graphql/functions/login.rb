# GraphQL function for handling user api login
class Functions::Login < GraphQL::Function
  argument :email, !types.String
  argument :password, !types.String
  description "Mutation for logging a user in using email/password"

  type do
    name "LoginPayload"
    description "Returns a JWT for successful authentication"
    field :jwt, types.String, description: "JSON Web Token containing\
    'user_id' and 'exp' in its payload"
    field :userId, types.String, description: "User ID to reference \
    the currentuser", hash_key: "user_id"
    field :customerId, types.ID, description: "Customer ID",
                                 hash_key: "customer_id"
  end

  def call(_obj, args, _ctx)
    login = user_login(args: args)
    if login.response.success?
      user = login.response.object.profile
      valid_response_with(user: user)
    else
      GraphQL::ExecutionError.new(login.response.error)
    end
  end

  private def user_login(args:)
    api = CDS::API::UserLogin.new(email: args[:email],
                                  password: args[:password])
    CDS_API_REQUEST.new(api: api).tap(&:run)
  end

  private def valid_response_with(user:)
    customer = customer_record_for(user: user)
    jwt = generate_jwt_for(user: user.details)
    OpenStruct.new(
      jwt: jwt,
      user_id: user.details.user_id,
      customer_id: customer.id
    )
  end

  private def generate_jwt_for(user:)
    expiration = 8.hours.from_now.getutc.to_i
    payload = {
      user_id: user.user_id,
      exp: expiration
    }
    JWTManager.encode(payload: payload).jwt
  end

  private def customer_record_for(user:)
    user_id = user.details.user_id
    Customer.find_or_create_by(cds_id: user_id)
  end
end
