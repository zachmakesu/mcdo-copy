# GraphQL function for handling fb user api login
# TODO : Update FB Login flow with token instead of email?
class Functions::FBLogin < GraphQL::Function
  argument :email, !types.String
  description "Mutation for logging a user in via facebook"

  type do
    name "FBLoginPayload"
    description "Returns a JWT for successful authentication"
    field :jwt, types.String, description: "JSON Web Token containing\
    'user_id' and 'exp' in its payload"
    field :userId, types.String, description: "User ID to reference \
    the currentuser", hash_key: "user_id"
  end

  # rubocop:disable Metrics/AbcSize
  def call(_obj, args, _ctx)
    login = fb_login(args: args)
    unless login.response.success?
      err = login.response.error
      raise CDS::API::Error, login.response.error unless new_user?(err: err)
      r = fb_register(args: args)
      raise CDS::API::Error, r.response.error unless r.response.success?
    end
    login_user_id = login.response.object.profile&.details&.user_id
    user_id = login_user_id || r&.response&.object&.user_id
    valid_response_with(user_id: user_id)
  end
  # rubocop:enable Metrics/AbcSize

  private

  def new_user?(err:)
    expected_err = "Email Address not found"
    err.downcase == expected_err.downcase
  end

  def fb_login(args:)
    api = CDS::API::FacebookLogin.new(email_address: args[:email])
    CDS_API_REQUEST.new(api: api).tap(&:run)
  end

  def fb_register(args:)
    api = CDS::API::FBUserRegistration.new(email_address: args[:email])
    CDS_API_REQUEST.new(api: api).tap(&:run)
  end

  def valid_response_with(user_id:)
    customer = customer_record_for(user_id: user_id)
    jwt = generate_jwt_for(user_id: user_id)
    OpenStruct.new(
      jwt: jwt,
      user_id: user_id,
      customer_id: customer.id
    )
  end

  def generate_jwt_for(user_id:)
    expiration = 8.hours.from_now.getutc.to_i
    payload = {
      user_id: user_id,
      exp: expiration
    }
    JWTManager.encode(payload: payload).jwt
  end

  def customer_record_for(user_id:)
    Customer.find_or_create_by(cds_id: user_id)
  end
end
