# GraphQL function for reset password
class Functions::ResetPassword < GraphQL::Function
  description "Mutation for reset password"
  argument :email, !types.String
  type do
    name "ResetPasswordPayload"
    description "Returns email and user id"
    field :email, !types.String, property: :email_address
    field :userId, !types.Int, property: :id
  end

  def call(_obj, args, _ctx)
    reset_password = reset_password_of(email: args[:email])
    unless reset_password.response.success?
      raise CDS::API::Error, reset_password.response.error
    end
    reset_password.response.object
  end

  private def reset_password_of(email:)
    api = CDS::API::UserPasswordReset.new(email_address: email)
    CDS_API_REQUEST.new(api: api).tap(&:run)
  end
end
