# GraphQL function for handling user profile update
class Functions::UpdateProfile < GraphQL::Function
  description "Mutation for profile update"
  argument :profile,
           !Types::InputObjects::ProfileInputType

  type do
    name "UpdateProfilePayload"
    description "Returns profile details"
    field :me, !Types::UserProfileType,
          description: "Returns the details of currently authenticated user"
  end

  # rubocop:disable Metrics/AbcSize
  def call(obj, args, ctx)
    r = lambda do |_o, a, c|
      update_profile = update_profile_of(user: c[:current_user],
                                         profile: a[:profile])
      unless update_profile.response.success?
        raise CDS::API::Error, update_profile.response.error
      end
      profile_response = get_profile_details_of(user: c[:current_user])
      user_det = profile_response.response.object.profile
      valid_response_with(user: user_det)
    end
    Resolvers::AuthRequiredResolver.new(r).call(obj, args, ctx)
  end
  # rubocop:enable Metrics/AbcSize

  private

  def update_profile_of(user:, profile:)
    api = CDS::API::UserProfileUpdate.new(user_id: user.cds_id,
                                          profile: profile)
    CDS_API_REQUEST.new(api: api).tap(&:run)
  end

  def get_profile_details_of(user:)
    api = CDS::API::UserProfileGet.new(user_id: user.cds_id)
    CDS_API_REQUEST.new(api: api).tap(&:run)
  end

  def valid_response_with(user:)
    OpenStruct.new(
      me: user
    )
  end
end
