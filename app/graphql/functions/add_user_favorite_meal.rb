# GraphQL function for adding favorite meal of user
class Functions::AddUserFavoriteMeal < GraphQL::Function
  description "Mutation adding favorite meal of user"
  argument :mealId,   !types.ID
  argument :mealType, !types.String
  type do
    name "AddUserFavoriteMealPayload"
    description "Returns customer's favoriteMeals"
    field :favoriteMeals, !types[Types::CustomerFavoriteMealType],
          property: :favorite_meals
  end

  def call(obj, args, ctx)
    r = lambda do |_o, a, c|
      meal = validate_meal_type(meal_type: a[:mealType], meal_id: a[:mealId])
      meal.favorite_meals.create!(customer_id: c[:current_user].id)
      c[:current_user]
    end
    Resolvers::AuthRequiredResolver.new(r).call(obj, args, ctx)
  end

  private def validate_meal_type(meal_type:, meal_id:)
    types = FavoriteMeal::MEAL_TYPES
    msg = "Invalid meal type."
    raise ActiveRecord::RecordNotFound, msg unless types.include?(meal_type)
    class_name = Object.const_get meal_type
    class_name.find_by!(id: meal_id)
  end
end
