# GraphQL function for handling user new contact number
class Functions::AddUserContactNumber < GraphQL::Function
  description "Mutation for adding user contact number"
  argument :contactNumbers,
           !types[Types::InputObjects::UserContactNumberInputType]

  type do
    name "AddUserContactNumberAddressPayload"
    description "Returns profile details"
    field :me, !Types::UserProfileType,
          description: "Returns the details of currently authenticated user"
  end

  # rubocop:disable Metrics/AbcSize
  def call(obj, args, ctx)
    r = lambda do |_o, a, c|
      add_contact_number = add_contact_number_for(user: c[:current_user],
                                                  numbers: a[:contactNumbers])
      unless add_contact_number.response.success?
        raise CDS::API::Error, add_contact_number.response.error
      end
      profile_response = get_profile_details_of(user: c[:current_user])
      user_det = profile_response.response.object.profile
      valid_response_with(user: user_det)
    end
    Resolvers::AuthRequiredResolver.new(r).call(obj, args, ctx)
  end
  # rubocop:enable Metrics/AbcSize

  private

  def add_contact_number_for(user:, numbers:)
    api = CDS::API::ContactNumberAdd.new(user_id: user.cds_id,
                                         contact_numbers: numbers)
    CDS_API_REQUEST.new(api: api).tap(&:run)
  end

  def get_profile_details_of(user:)
    api = CDS::API::UserProfileGet.new(user_id: user.cds_id)
    CDS_API_REQUEST.new(api: api).tap(&:run)
  end

  def valid_response_with(user:)
    OpenStruct.new(
      me: user
    )
  end
end
