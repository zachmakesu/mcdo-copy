# GraphQL function for reusing search
class Functions::Searchable < GraphQL::Function
  attr_reader :klass, :auth_required
  def initialize(klass:, auth_required: false)
    @klass = klass
    @auth_required = auth_required
  end
  argument :query, types.String
  def call(obj, args, ctx)
    r = lambda do |_o, _a, _c|
      if args[:query]
        klass.constantize.where("name ILIKE ?", "%#{args[:query]}%")
      else
        klass.constantize.all
      end
    end
    resolver = r
    resolver = Resolvers::AuthRequiredResolver.new(r) if auth_required
    resolver.call(obj, args, ctx)
  end
end
