# Global resolve type
# refs: http://graphql-ruby.org/types/abstract_types.html
module ResolveType
  def self.call(type, obj, ctx)
  end
end

McdoWebSchema = GraphQL::Schema.define do
  resolve_type ResolveType
  mutation(Types::MutationType)
  query(Types::QueryType)
end

# rubocop:disable Metrics/BlockLength
GraphQL::Errors.configure(McdoWebSchema) do
  rescue_from ActiveRecord::RecordNotFound do |exception|
    message = "Resource not found."
    ExceptionNotifier.notify_exception(exception)
    GraphQL::ExecutionError.new(message)
  end

  rescue_from NameError do |exception|
    GraphQL::ExecutionError.new(exception)
  end

  rescue_from ActiveRecord::RecordInvalid do |exception|
    errors = exception.record.errors.full_messages
    GraphQL::ExecutionError.new(errors.join("\n"))
  end

  rescue_from Resolvers::AuthRequiredResolverUnauthorizedError do |exception|
    GraphQL::ExecutionError.new(exception)
  end

  rescue_from Resolvers::AuthRequiredResolverJWTError do |exception|
    GraphQL::ExecutionError.new(exception)
  end

  rescue_from CDS::API::Error do |exception|
    message = "Error interacting with external API, please try again later."
    ExceptionNotifier.notify_exception(exception)
    GraphQL::ExecutionError.new(message)
  end

  rescue_from StandardError do |exception|
    message = "Please try to execute the query for this field later."
    ExceptionNotifier.notify_exception(exception)
    GraphQL::ExecutionError.new(message)
  end
end
# rubocop:enable Metrics/BlockLength
