Resolvers::AuthRequiredResolverUnauthorizedError = Class.new(StandardError)
Resolvers::AuthRequiredResolverJWTError = Class.new(StandardError)
# This class acts as a resolve wrapper for requiring auth
# for certain graphql queries and mutations
# source: http://graphql-ruby.org/fields/resolve_wrapper.html
# placed inside app/lib due to autoloading issues
# reference: https://stackoverflow.com/a/39024296
class Resolvers::AuthRequiredResolver
  AUTH_REQUIRED_ERROR = "Authorization is required for this operation.".freeze
  def initialize(resolve_func)
    @resolve_func = resolve_func
  end

  def call(obj, args, ctx)
    if (jwt_error = ctx[:jwt_error])
      raise Resolvers::AuthRequiredResolverJWTError, jwt_error
    end
    if ctx[:current_user].blank?
      raise(
        Resolvers::AuthRequiredResolverUnauthorizedError,
        AUTH_REQUIRED_ERROR
      )
    end
    @resolve_func.call(obj, args, ctx)
  end
end
