Types::QueryType = GraphQL::ObjectType.define do
  name "Query"
  description "The query root of this Schema"

  field :me do
    type Types::UserProfileType
    description "Returns the currently authenticated user"
    r = lambda do |_obj, _args, ctx|
      user_id = ctx[:current_user]&.cds_id
      api = CDS::API::UserProfileGet.new(user_id: user_id)
      cds = CDS_API_REQUEST.new(api: api).tap(&:run)
      cds.response.object.profile
    end
    resolve Resolvers::AuthRequiredResolver.new(r)
  end

  field :cities do
    type !types[Types::CityType]
    description "Returns the list of cities"
    r = lambda do |_obj, _args, _ctx|
      CDS::Model::City.all
    end
    resolve r
  end

  field :province do
    type Types::ProvinceType
    argument :id, !types.ID
    description "Returns a single province based on id"
    r = lambda do |_obj, args, _ctx|
      CDS::Model::Province.find(args[:id])
    end
    resolve r
  end

  field :provinces do
    type !types[Types::ProvinceType]
    description "Returns the list of provinces"
    r = lambda do |_obj, _args, _ctx|
      CDS::Model::Province.all
    end
    resolve r
  end

  field :deliveryAddress do
    argument :id, types.ID
    type Types::DeliveryAddressType
    description "Returns the delivery address"
    r = lambda do |_obj, args, _ctx|
      if (id = args[:id])
        CDS::Model::Address.find(id)
      end
    end
    resolve Resolvers::AuthRequiredResolver.new(r)
  end

  field :contactNumber do
    argument :id, types.ID
    type Types::ContactNumberType
    description "Returns the contact number"
    r = lambda do |_obj, args, _ctx|
      if (id = args[:id])
        CDS::Model::ContactNumber.find(id)
      end
    end
    resolve Resolvers::AuthRequiredResolver.new(r)
  end

  field :areas do
    type !types[Types::AreaType]
    description "Returns the areas branches belong to"
    r = lambda do |_obj, _args, _ctx|
      Area.includes(branches: :delivery_numbers).order(:name)
    end
    resolve r
  end

  field :branches do
    type !types[Types::BranchType]
    description "Returns the branches"
    r = lambda do |_obj, _args, _ctx|
      Branch.includes(:area, :delivery_numbers).order(:name)
    end
    resolve r
  end

  field :deliveryNumbers do
    type !types[Types::DeliveryNumberType]
    description "Returns the store delivery numbers"
    r = lambda do |_obj, _args, _ctx|
      DeliveryNumber.includes(branch: :area).all
    end
    resolve r
  end

  field :categories do
    function    Functions::Searchable.new(klass: "Category")
    type        !types[Types::CategoryType]
    description "Returns product categories"
    resolve ->(_obj, _args, _ctx) { Category.all.order(:sequence_id) }
  end

  field :category do
    description "Returns category"
    type !Types::CategoryType
    argument :id, !types.ID
    resolve ->(_obj, args, _ctx) { Category.find_by!(id: args[:id]) }
  end

  field :alacartes, function: Functions::Searchable.new(klass: "Alacarte") do
    type        !types[Types::AlacarteType]
    description "Returns the alacarte products"
  end

  field :alacarte do
    description "Returns alacarte"
    type !Types::AlacarteType
    argument :id, !types.ID
    resolve ->(_obj, args, _ctx) { Alacarte.find_by!(id: args[:id]) }
  end

  field :mealOptions do
    function    Functions::Searchable.new(klass: "MealOption")
    type        !types[Types::MealOptionType]
    description "Returns the meal options"
  end

  field :valueMeals, function: Functions::Searchable.new(klass: "ValueMeal") do
    type        !types[Types::ValueMealType]
    description "Returns the value meal products"
  end

  field :valueMeal do
    description "Returns value meal product"
    type !Types::ValueMealType
    argument :id, !types.ID
    resolve ->(_obj, args, _ctx) { ValueMeal.find_by!(id: args[:id]) }
  end

  field :cart do
    description "AuthRequired: Retuns cart details"
    type Types::CartType
    r = lambda do |_obj, _args, ctx|
      ctx[:current_user].active_cart
    end
    resolve Resolvers::AuthRequiredResolver.new(r)
  end

  field :orderStatus do
    description "AuthRequired: Returns status of specific order"
    type types.String
    argument :order_id, !types.ID
    r = lambda do |_obj, args, ctx|
      order = ctx[:current_user].orders.find_by!(order_id: args[:order_id])
      api = CDS::API::DeliveryStatusCheck.new(order_id: order.order_id)
      cds = CDS_API_REQUEST.new(api: api).tap(&:run)
      cds.response.object.order_status
    end
    resolve Resolvers::AuthRequiredResolver.new(r)
  end

  field :favoriteAlacartes do
    description "Customer's favorite alacarte meals"
    type types[Types::AlacarteType]
    r = lambda do |_obj, _args, ctx|
      ctx[:current_user].favorite_alacartes
    end
    resolve Resolvers::AuthRequiredResolver.new(r)
  end

  field :favoriteValueMeals do
    description "Customer's favorite value meals"
    type types[Types::ValueMealType]
    r = lambda do |_obj, _args, ctx|
      ctx[:current_user].favorite_value_meals
    end
    resolve Resolvers::AuthRequiredResolver.new(r)
  end

  field :userManuals,
        function: Functions::Searchable.new(klass: "UserManual") do
    type !types[Types::UserManualType]
    description "Returns user manuals"
  end

  field :userManual do
    description "Returns specific user manual"
    type !Types::UserManualType
    argument :id, !types.ID
    resolve ->(_obj, args, _ctx) { UserManual.find_by!(id: args[:id]) }
  end

  field :promos, function: Functions::GetPromos.new do
    type !types[Types::PromoType]
    description "Returns promos"
  end

  field :promo do
    description "Returns promo"
    type !Types::PromoType
    argument :id, !types.ID
    resolve ->(_obj, args, _ctx) { Promo.find_by!(id: args[:id]) }
  end

  field :homeLayouts,
        function: Functions::Searchable.new(klass: "HomeContent") do
    description "Returns all home layouts"
    type !types[Types::HomeLayoutType]
  end

  field :homeLayout do
    description "Returns specific Layout"
    type !Types::HomeLayoutType
    argument :id, !types.ID
    resolve ->(_obj, args, _ctx) { HomeContent.find_by!(id: args[:id]) }
  end

  field :coupons, function: Functions::Searchable.new(klass: "Coupon") do
    description "Returns all coupons"
    type !types[Types::CouponType]
  end

  field :coupon do
    description "Return specific coupon"
    type !Types::CouponType
    argument :id, !types.ID
    resolve ->(_obj, args, _ctx) { Coupon.find_by!(id: args[:id]) }
  end
end
