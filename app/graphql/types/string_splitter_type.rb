# http://www.rubydoc.info/gems/graphql/1.7.7/GraphQL/ScalarType
Types::StringSplitterType = GraphQL::ScalarType.define do
  name "StringSplitter"
  description "split strings"
  coerce_input  ->(value, _ctx) { value.to_s }
  coerce_result ->(value, _ctx) { value.split(",") }
end
