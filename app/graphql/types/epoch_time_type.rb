# http://www.rubydoc.info/gems/graphql/1.7.7/GraphQL/ScalarType
Types::EpochTimeType = GraphQL::ScalarType.define do
  name "EpochTime"
  description "Time since epoch in seconds"
  c_i = lambda do |value, _ctx|
    begin
      Time.zone.at(Float(value))
    rescue ArgumentError
      raise GraphQL::CoercionError, "cannot coerce `#{value.inspect}` to Float"
    end
  end
  coerce_input c_i
  coerce_result ->(value, _ctx) { value.to_f }
end
