# GraphQL type for branch delivery numbers
Types::DeliveryNumberType = GraphQL::ObjectType.define do
  name "DeliveryNumber"
  description "Branch delivery numbers"

  field :branch,   Types::BranchType
  field :branchId, !types.ID, hash_key: "branch_id"
  field :id,       !types.ID
  field :number,   !types.String
end
