Types::CustomerType = GraphQL::ObjectType.define do
  name "Customer"
  description "Customer details"

  field :cdsId,              !types.ID, hash_key: "cds_id"
  field :favoriteAlacartes,  types[Types::AlacarteType],
        property: :favorite_alacartes
  field :favoriteValueMeals, types[Types::ValueMealType],
        property: :favorite_value_meals
end
