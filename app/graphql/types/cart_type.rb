Types::CartType = GraphQL::ObjectType.define do
  name "Cart"
  description "Cart with items to be ordered "

  field :id, !types.ID
  field :items, types[Types::CartVarianceType]
  field :subTotal, types.Float, property: :sub_total
  field :deliveryFee do
    type types.Float
    resolve ->(_obj, _args, _ctx) { DeliveryFee.latest_fee }
  end
end
