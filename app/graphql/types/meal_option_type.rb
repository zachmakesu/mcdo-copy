Types::MealOptionType = GraphQL::ObjectType.define do
  name "MealOption"
  description "Options for meal orders"
  field :classification, !types.String
  field :codeName,       !types.String,         hash_key: "code_name"
  field :groupId,        !types.ID,             hash_key: "group_id"
  field :id,             !types.ID
  field :imageSource,    !types.String do
    resolve ->(obj, _args, _ctx) { obj.image_source.url }
  end
  field :isDefault,      !types.Boolean,        hash_key: "is_default"
  field :longName,       !types.String,         hash_key: "long_name"
  field :name,           !types.String
  field :poscode,        !types.String
  field :price,          !types.Float
  field :size,           !types.String
  field :updatedAt,      !Types::EpochTimeType, hash_key: "updated_at"
  field :displayedAt,    Types::EpochTimeType,  hash_key: "displayed_at"
end
