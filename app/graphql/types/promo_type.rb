Types::PromoType = GraphQL::ObjectType.define do
  name "Promo"
  field :id,              !types.ID
  field :category,        types.String
  field :isActive,        types.Boolean, hash_key: "is_active"
  field :isPublished,     types.Boolean, hash_key: "is_published"
  field :link,            types.String
  field :name,            types.String
  field :tags,            Types::StringSplitterType
  field :validityStartAt, Types::EpochTimeType, hash_key: "validity_start_at"
  field :validityEndAt,   Types::EpochTimeType, hash_key: "validity_end_at"
  field :imageSource, types.String, hash_key: "image_source" do
    resolve ->(obj, _args, _ctx) { obj.image_source.url }
  end
end
