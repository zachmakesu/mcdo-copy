Types::ProductBundleType = GraphQL::ObjectType.define do
  name "ProductBundle"
  description "McShare Box Bundle"

  field :maxFries,  !types.Int, hash_key: "max_fries"
  field :maxDrinks, !types.Int, hash_key: "max_drinks"
  field :maxRice,   !types.Int, hash_key: "max_rice"
  field :options,   types[Types::ProductOptionType]
end
