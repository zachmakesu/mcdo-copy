Types::AlacarteType = GraphQL::ObjectType.define do
  name "Alacarte"
  description "Alacarte meals"
  field :bestWith,          Types::VarianceType do
    resolve ->(obj, _args, _ctx) { Variance.find_by(poscode: obj.best_with) }
  end
  field :categories,        !types[Types::CategoryType]
  field :description,       types.String
  field :discountedRice,    !types.Boolean,
        hash_key: "discounted_rice"
  field :groupId,           !types.ID,                   hash_key: "group_id"
  field :imageSource,       !types.String do
    resolve ->(obj, _args, _ctx) { obj.image_source.url }
  end
  field :id,                !types.ID
  field :name,              types.String
  field :productOrderId,    types.ID,
        hash_key: "product_order_id"
  field :schedule,          Types::ScheduleType
  field :legacySections,    types.String,
        hash_key: "legacy_sections"
  field :updatedAt,         !Types::EpochTimeType,       hash_key: "updated_at"
  field :productBundle,     Types::ProductBundleType,
        property: :product_bundle
  field :displayedAt,       Types::EpochTimeType,
        hash_key: "displayed_at"
  field :variances,         !types[Types::VarianceType]
  field :valueMealId,       types.ID do
    r = lambda do |obj, _args, _ctx|
      alacarte_id = obj.variances.first.poscode
      ValueMeal.find_by(alacarte_id: alacarte_id)&.id
    end
    resolve r
  end
  field :isFavorite,        !types.Boolean do
    r = lambda do |obj, _args, ctx|
      current_user = ctx[:current_user]
      return false if current_user.blank?
      favorite_meals = current_user.favorite_meals
      meal = favorite_meals.find_by(meal_id: obj.id, meal_type: "Alacarte")
      meal.present?
    end
    resolve r
  end
end
