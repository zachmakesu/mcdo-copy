# GraphQL type for user's delivery address
Types::DeliveryAddressType = GraphQL::ObjectType.define do
  name "DeliveryAddress"
  description "Delivery address to send orders to"
  field :id,           !types.ID
  field :area,         !types.String
  field :building,     !types.String
  field :cityId,       !types.ID,             hash_key: "city"
  field :companyName,  !types.String,         hash_key: "company_name"
  field :createdAt,    !Types::EpochTimeType, hash_key: "date_created"
  field :isDelisted,   !types.Boolean do
    resolve ->(obj, _args, _ctx) { obj.delisted.to_s == "1" }
  end
  field :floor,        !types.String
  field :geotagX,      !types.String,         hash_key: "geotag_x"
  field :geotagY,      !types.String,         hash_key: "geotag_y"
  field :id,           !types.ID
  field :isVerified,   !types.Boolean,        hash_key: "is_verified"
  field :label,        !types.String,         hash_key: "address_label"
  field :landmark,     !types.String
  field :province,     !types.ID,             hash_key: "province"
  field :remarks,      !types.String,         hash_key: "address_remarks"
  field :status,       !types.String
  field :street,       !types.String
  field :type,         !types.String
  field :updatedAt,    !Types::EpochTimeType, hash_key: "date_updated"
  field :userId,       !types.ID
end
