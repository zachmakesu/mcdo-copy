Types::SectionType = GraphQL::ObjectType.define do
  name "Section"
  description "Meal options sections"
  field :id,        !types.ID
  field :title,     !types.String
  field :createdAt, !Types::EpochTimeType,  hash_key: "created_at"
  field :updatedAt, !Types::EpochTimeType,  hash_key: "updated_at"
end
