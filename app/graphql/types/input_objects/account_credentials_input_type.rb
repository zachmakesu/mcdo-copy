module Types
  module InputObjects
    AccountCredentialsInputType = GraphQL::InputObjectType.define do
      name "AccountCredentialsInput"
      description "A input fields for credentials in registration"

      argument :email,           !types.String
      argument :password,        !types.String
      argument :confirmPassword, !types.String
    end
  end
end
