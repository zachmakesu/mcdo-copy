module Types
  module InputObjects
    CartItemInputType = GraphQL::InputObjectType.define do
      name "CartItemInput"
      description "A input fields for cart item"

      argument :id,               types.Int
      argument :variancePoscode,  !types.ID
      argument :quantity,         !types.Int
      argument :bestWithQuantity, !types.Int
      argument :mealOptions do
        type types[Types::InputObjects::CartMealOptionsInputType]
      end
    end
  end
end
