Types::InputObjects::CustomerInfoInputType = GraphQL::InputObjectType.define do
  name "CustomerInfoInput"
  description "A input fields for customer info"

  argument :firstname,        !types.String
  argument :lastname,         !types.String
  argument :emailAddress,     !types.String
  argument :contactNumberId,  types.Int
  argument :contactNumber,    !types.String
  argument :orderRemarks,     types.String
  argument :paymentType,      !types.String
end
