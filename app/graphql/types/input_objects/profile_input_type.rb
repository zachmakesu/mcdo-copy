module Types
  module InputObjects
    ProfileInputType = GraphQL::InputObjectType.define do
      name "ProfileInput"
      description "A input fields for registration"

      argument :firstName,       !types.String
      argument :middleName,      types.String
      argument :lastName,        !types.String
      argument :salutation,      types.String
      argument :birthdate,       !types.String
      argument :subscription,    types.Int
    end
  end
end
