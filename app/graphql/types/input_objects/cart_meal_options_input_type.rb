module Types
  module InputObjects
    CartMealOptionsInputType = GraphQL::InputObjectType.define do
      name "CartMealOptionsInputType"
      description "A input field for cart items meal option"

      argument :poscode,  !types.ID
      argument :quantity, !types.Int
    end
  end
end
