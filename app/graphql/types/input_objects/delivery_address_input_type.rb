module Types
  module InputObjects
    DeliveryAddressInputType = GraphQL::InputObjectType.define do
      name "DeliveryAddressInput"
      description "A input fields for delivery address"

      argument :id,             !types.Int
      argument :companyName,    !types.String
      argument :building,       !types.String
      argument :floor,          !types.String
      argument :area,           !types.String
      argument :street,         !types.String
      argument :landmark,       !types.String
      argument :cityId,         !types.Int
      argument :addressRemarks, !types.String
    end
  end
end
