module Types
  module InputObjects
    UserAddressInputType = GraphQL::InputObjectType.define do
      name "AddressInput"
      description "Input fields for customer address"

      argument :addressId,       types.ID
      argument :cityId,          !types.ID
      argument :provinceId,      !types.ID
      argument :addressLabel,    types.String
      argument :floor,           !types.String
      argument :street,          !types.String
      argument :area,            !types.String
      argument :building,        types.String
      argument :type,            types.String, default_value: "residential"
      argument :companyName,     types.String
      argument :landmark,        types.String
      argument :addressRemarks,  types.String
      argument :geotagX,         types.String
      argument :geotagY,         types.String
      argument :isVerified,      types.Int
      argument :status,          types.String, default_value: "primary"
    end
  end
end
