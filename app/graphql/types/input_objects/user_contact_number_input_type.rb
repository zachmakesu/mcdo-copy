module Types
  module InputObjects
    UserContactNumberInputType = GraphQL::InputObjectType.define do
      name "ContactNumberInput"
      description "Input fields for customer contact number"

      argument :id,              types.ID
      argument :contactNumber,   !types.String
      argument :extensionNumber, types.String
    end
  end
end
