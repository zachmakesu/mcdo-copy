Types::CategoryType = GraphQL::ObjectType.define do
  name "Category"
  description "Meal category"
  field :id,          !types.ID
  field :name,        !types.String
  field :sequenceId,  !types.Int, hash_key: "sequence_id"
  field :alacartes,   !types[Types::AlacarteType]
  field :valueMeals,  !types[Types::ValueMealType] do
    resolve ->(obj, _args, _ctx) { obj.value_meals }
  end
end
