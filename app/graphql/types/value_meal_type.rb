Types::ValueMealType = GraphQL::ObjectType.define do
  name "ValueMeal"
  description "Value meals"
  field :bestWith,          !types.ID,             hash_key: "best_with"
  field :alacarteId,        types.ID do
    r = lambda do |obj, _args, _ctx|
      Variance.find_by(poscode: obj.alacarte_id)&.variable_id
    end
    resolve r
  end
  field :categories,        !types[Types::CategoryType]
  field :discountedRice,    !types.Boolean,        hash_key: "discounted_rice"
  field :id,                !types.ID
  field :name,              types.String
  field :orderId,           types.ID,              hash_key: "order_id"
  field :schedule,          Types::ScheduleType
  field :legacySections,    types.String,          hash_key: "legacy_sections"
  field :imageSource,       !types.String do
    resolve ->(obj, _args, _ctx) { obj.image_source.url }
  end
  field :updatedAt,         !Types::EpochTimeType, hash_key: "update_at"
  field :displayedAt,       Types::EpochTimeType,  hash_key: "displayed_at"
  field :variances,         !types[Types::VarianceType]
  field :isFavorite,        !types.Boolean do
    r = lambda do |obj, _args, ctx|
      current_user = ctx[:current_user]
      return false if current_user.blank?
      favorite_meals = current_user.favorite_meals
      meal = favorite_meals.find_by(meal_id: obj.id, meal_type: "ValueMeal")
      meal.present?
    end
    resolve r
  end
end
