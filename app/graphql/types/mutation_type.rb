Types::MutationType = GraphQL::ObjectType.define do
  name "Mutation"

  field :login,                 function: Functions::Login.new
  field :fbLogin,               function: Functions::FBLogin.new
  field :register,              function: Functions::Register.new
  field :updateProfile,         function: Functions::UpdateProfile.new
  field :updateAddress,         function: Functions::UpdateUserAddress.new
  field :updateContactNumber,   function: Functions::UpdateUserContactNumber.new
  field :addUserAddresses,      function: Functions::AddUserAddress.new
  field :addUserContactNumbers, function: Functions::AddUserContactNumber.new
  field :addFavoriteMeal,       function: Functions::AddUserFavoriteMeal.new
  field :resetPassword,         function: Functions::ResetPassword.new
  field :removeFavoriteMeal,    function: Functions::RemoveUserFavoriteMeal.new
  field :delistUserAddress,     function: Functions::DelistUserAddress.new
  field :delistUserContactNumber,
        function: Functions::DelistUserContactNumber.new
  field :cartUpdateItem,        function: Functions::CartUpdateItem.new
  field :cartCheckout,          function: Functions::CartCheckout.new
end
