# GraphQL type for coupons
Types::CouponType = GraphQL::ObjectType.define do
  name "Coupon"
  description "Coupon for products"

  field :id,                !types.ID
  field :name,              !types.String
  field :classification,    types.String
  field :usageType,         !types.String,        hash_key: "usage_type"
  field :description,       types.String
  field :usageCount,        !types.Int,           hash_key: "usage_count"
  field :validityStartAt,   Types::EpochTimeType, hash_key: "validity_start_at"
  field :validityEndAt,     Types::EpochTimeType, hash_key: "validity_end_at"
  field :imageSource,       types.String,         hash_key: "image_source"
  field :detailImageSource, types.String,
        hash_key: "detail_image_source"
  field :discount,          !types.Float
  field :poscode,           !types.String
end
