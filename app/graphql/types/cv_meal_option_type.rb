Types::CvMealOptionType = GraphQL::ObjectType.define do
  name "CvMealOption"
  description "Carts Variances Meal Option join table"

  field :quantity,    types.Int
  field :totalPrice,  types.Float, property: :total_price
  field :mealOption,  Types::MealOptionType, property: :meal_option
end
