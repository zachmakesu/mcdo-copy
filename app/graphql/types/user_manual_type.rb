Types::UserManualType = GraphQL::ObjectType.define do
  name "UserManual"
  field :id,          !types.ID
  field :sequenceId,  !types.ID,    hash_key: "sequence_id"
  field :description, types.String
  field :imageSource, types.String, hash_key: "image_source" do
    resolve ->(obj, _args, _ctx) { obj.image_source.url }
  end
end
