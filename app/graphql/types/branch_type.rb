# GraphQL type for Branch
Types::BranchType = GraphQL::ObjectType.define do
  name "Branch"
  description "Branch a delivery number belongs to"

  field :area,        Types::AreaType
  field :areaId,      !types.ID, hash_key: "area_id"
  field :description, types.String
  field :deliveryNumbers, !types[Types::DeliveryNumberType] do
    resolve ->(obj, _args, _ctx) { obj.delivery_numbers }
  end
  field :id,          !types.ID
  field :name,        !types.String
  field :address,     types.String
  field :offerings,   types.String
  field :lat,         types.String
  field :lng,         types.String
  field :status,      types.String
  field :imageSource, types.String do
    resolve ->(obj, _args, _ctx) { obj.image_source.url }
  end
end
