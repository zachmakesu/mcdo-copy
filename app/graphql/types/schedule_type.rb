Types::ScheduleType = GraphQL::ObjectType.define do
  name "Schedule"
  description "Product Schedules"
  field :id,        !types.ID
  field :title,     !types.String
  field :startAt,   !Types::EpochTimeType,  hash_key: "start_at"
  field :endAt,     !Types::EpochTimeType,  hash_key: "end_at"
  field :createdAt, !Types::EpochTimeType,  hash_key: "created_at"
  field :updatedAt, !Types::EpochTimeType,  hash_key: "updated_at"
end
