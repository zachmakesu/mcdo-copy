module Types
  module UnionTypes
    FavoriteMealUnionType = GraphQL::UnionType.define do
      name "FavoriteMealUnion"
      description "Meal types of favorite meal"
      possible_types [Types::AlacarteType, Types::ValueMealType]

      r = lambda do |obj, _ctx|
        if obj.is_a?(Alacarte)
          Types::AlacarteType
        else
          Types::ValueMealType
        end
      end
      resolve_type r
    end
  end
end
