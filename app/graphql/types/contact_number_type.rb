# GraphQL type for user's contact number
Types::ContactNumberType = GraphQL::ObjectType.define do
  name "ContactNumber"
  description "Contact number to reach a user with"

  field :id,         !types.ID
  field :number,     !types.String,  hash_key: "contact_number"
  field :extension,  !types.String,  hash_key: "extension_number"
  field :userId,     !types.ID,      hash_key: "user_id"
  field :isDelisted, !types.Boolean do
    resolve ->(obj, _args, _ctx) { obj.delisted.to_s == "1" }
  end
end
