Types::ProvinceType = GraphQL::ObjectType.define do
  name "Province"
  description "Province referenced by city"

  field :id,          !types.ID
  field :name,        !types.String
  field :description, types.String
  field :cities,      !types[Types::CityType] do
    r = lambda do |obj, _args, _ctx|
      CDS::Model::City.all.select { |c| c.province_id == obj.id }
    end
    resolve r
  end
end
