module Types
  CustomerDetailsType = GraphQL::ObjectType.define do
    name "CustomerDetails"
    description "Customer's profile details"

    field :birthDate,    types.String, hash_key: "birth_date"
    field :firstName,    types.String, hash_key: "firstname"
    field :id,           !types.ID,     hash_key: "user_id"
    field :lastName,     types.String, hash_key: "lastname"
    field :middleName,   types.String, hash_key: "middlename"
    field :salutation,   types.String
    field :subscription, types.String
  end
end
