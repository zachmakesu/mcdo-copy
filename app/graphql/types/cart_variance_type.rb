Types::CartVarianceType = GraphQL::ObjectType.define do
  name "CartVariance"
  description "Joint relation of cart and variance"

  field :id,               !types.ID
  field :alacarte,         Types::AlacarteType
  field :valueMeal,        Types::ValueMealType, property: :value_meal
  field :quantity,         types.Int
  field :totalPrice,       types.Float,          property: :total_price
  field :variance,         Types::VarianceType
  field :bestWithQuantity, types.Int,            property: :best_with_quantity
  field :options do
    type types[Types::CvMealOptionType]
    property :cv_options
  end
  field :bestWith do
    type Types::VarianceType
    r = lambda do |obj, _args, _ctx|
      obj.variance.best_with_item if obj.best_with_quantity > 0
    end
    resolve r
  end
end
