Types::CustomerFavoriteMealType = GraphQL::ObjectType.define do
  name "CustomerFavoriteMeal"
  description "Customer's Favorite Meal"

  field :meal,     !Types::UnionTypes::FavoriteMealUnionType
  field :customer, !Types::CustomerType
end
