Types::VarianceType = GraphQL::ObjectType.define do
  name "Variance"
  description "Variance of meal and alacarte products"
  field :name,              !types.String
  field :codeName,          !types.String, hash_key: "code_name"
  field :poscode,           !types.String
  field :price,             !types.Float
  field :bestWith,          types.ID,     hash_key: "best_with"
  field :groupId,           types.ID,     hash_key: "group_id"
  field :menuName,          types.String, hash_key: "menu_name"
  field :options,           types[Types::ProductOptionType] do
    r = lambda do |obj, _args, _ctx|
      variable = obj.variable
      if variable.bundle?
        variable.product_bundle.options
      else
        obj.options
      end
    end
    resolve r
  end
  field :imageSource,       types.String do
    resolve ->(obj, _args, _ctx) { obj.image_source.url }
  end
  field :bestWith,          Types::VarianceType do
    resolve ->(obj, _args, _ctx) { Variance.find_by(poscode: obj.best_with) }
  end
end
