# GraphQL type for user profile
Types::UserProfileType = GraphQL::ObjectType.define do
  name "UserProfile"
  description "User profile associated to an account"

  field :emailAddress,   !types.String, hash_key: "email_address"
  field :emailValidated, !types.String, hash_key: "email_validated"
  field :details,        !Types::CustomerDetailsType do
    resolve ->(obj, _args, _ctx) { obj.details }
  end
  field :addresses,      !types[Types::DeliveryAddressType] do
    # Empty address returns empty string instead of empty array
    r = lambda do |obj, _args, _ctx|
      obj.delivery_address.present? ? obj.delivery_address : []
    end
    resolve r
  end
  field :contactNumbers, !types[Types::ContactNumberType] do
    # Empty contact number returns empty string instead of empty array
    r = lambda do |obj, _args, _ctx|
      obj.contact_numbers.present? ? obj.contact_numbers : []
    end
    resolve r
  end
end
