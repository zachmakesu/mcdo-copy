Types::HomeLayoutType = GraphQL::ObjectType.define do
  name "HomeLayout"

  field :id,           types.ID
  field :availability, types.String
  field :name,         types.String
  field :sequenceId,   types.ID, hash_key: "sequence_id"
  field :category,     !Types::CategoryType do
    resolve ->(obj, _arg, _ctx) { obj.groupable }
  end
  field :imageSourceApp, types.String, hash_key: "image_source_app" do
    resolve ->(obj, _arg, _ctx) { obj.image_source_app.url }
  end
  field :imageSourceWeb, types.String, hash_key: "image_source_web" do
    resolve ->(obj, _arg, _ctx) { obj.image_source_web.url }
  end
end
