Types::CityType = GraphQL::ObjectType.define do
  name "City"
  description "City referenced by delivery address"

  field :id,          !types.ID
  field :name,        !types.String
  field :description, types.String
  field :provinceId,  !types.ID, hash_key: "province_id"
  field :province,    Types::ProvinceType do
    r = lambda do |obj, _args, _ctx|
      CDS::Model::Province.find(obj[:province_id])
    end
    resolve r
  end
end
