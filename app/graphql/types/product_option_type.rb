Types::ProductOptionType = GraphQL::ObjectType.define do
  name "ProductOption"
  description "Meal options"
  field :id,          !types.ID
  field :mealOption,  !Types::MealOptionType do
    resolve ->(obj, _args, _ctx) { obj.meal_option }
  end
  field :section,     !Types::SectionType
  field :mealId,      !types.ID,              hash_key: "meal_id"
  field :mealType,    !types.String,          hash_key: "meal_type"
  field :createdAt,   !Types::EpochTimeType,  hash_key: "created_at"
  field :updatedAt,   !Types::EpochTimeType,  hash_key: "updated_at"
end
