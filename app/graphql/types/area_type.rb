# GraphQL Type for Area
Types::AreaType = GraphQL::ObjectType.define do
  name "Area"
  description "Area a Branch belongs to"

  field :branches, !types[Types::BranchType] do
    resolve ->(obj, _args, _ctx) { obj.branches }
  end
  field :description, types.String
  field :id,          !types.ID
  field :name,        !types.String
end
