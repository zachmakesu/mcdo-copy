# Class for representing response from CDS API
class CDS::Response
  delegate :error, to: :object
  def initialize(data:)
    @data = data
  end

  def object
    @object ||= JSON.parse(data.to_json, object_class: OpenStruct)
  end

  def success?
    object.status.to_i == 200
  end

  private

  attr_reader :data
end
