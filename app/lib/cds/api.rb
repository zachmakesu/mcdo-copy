# Base class for Central Database System API
# interaction meant to be subclassed
class CDS::API
  def initialize
  end

  def action
    ""
  end

  def body
    {
      data: data,
      signature: signature
    }
  end

  def config
    @config ||= CDS::Config.new
  end

  def data
    {
      api_key: config.public_key,
      timestamp: config.timestamp,
      source: config.source_name,
      hostname: config.host_name,
      action: action,
      contents: params
    }
  end

  def params
    {}
  end

  def signature
    key = config.private_key
    json_data = data.to_json
    digest = OpenSSL::Digest.new("sha256")
    hmac = OpenSSL::HMAC.digest(digest, key, json_data)
    Base64.strict_encode64(hmac)
  end
end
