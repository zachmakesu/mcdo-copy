# Class for building HTTP requests to Production CMS
class CDS::ProxyRequest
  attr_reader :api, :request

  def initialize(api:)
    @api = api
    @request = Typhoeus::Request.new(
      url,
      method: :post,
      body: rawify(params: body)
    )
  end

  def body
    @body ||= api.params.merge(source: source_name)
  end

  def run
    request.on_complete do |res|
      if res.success?
        logger.info("ProxyRequest success!")
      else
        logger.warn("HTTP request failed: " + res.code.to_s)
      end
    end
    @internal_response = request.run
  end

  def url
    path = CDS::ProxyMapping.new(action: api.action).path
    @url ||= "#{base_url}/#{path}"
  end

  def response
    body = internal_response.body
    data = if body.empty?
             { status: internal_response.code,
               error: internal_response.return_message }
           else
             parsed_body = JSON.parse(body)
             parsed_body["gadc_data"]
           end
    @response ||= CDS::Response.new(data: data)
  end

  private

  attr_reader :internal_response

  def base_url
    @base_url ||= ENV.fetch("CDS_PROXY_API_URL")
  end

  def source_name
    ENV.fetch("CDS_PROXY_SOURCE_NAME")
  end

  def logger
    Rails.logger
  end

  # rubocop:disable Style/ConditionalAssignment
  def rawify(params:)
    "".tap do |x|
      params.each_pair do |k, v|
        if v.is_a?(Array)
          x << "#{k}=#{CGI.escape(v.to_json)}&"
        else
          x << "#{k}=#{v}&"
        end
      end
    end[0..-2]
  end
  # rubocop:enable Style/ConditionalAssignment
end
