# Class for encapsulating configuration for CDS API interaction
class CDS::Config
  def public_key
    ENV.fetch("CDS_PUBLIC_KEY")
  end

  def private_key
    ENV.fetch("CDS_PRIVATE_KEY")
  end

  def host_name
    ENV.fetch("CDS_HOST_NAME")
  end

  # https://reservedwords.herokuapp.com/words/source?q[word_or_notes_cont]=source
  def source_name
    ENV.fetch("CDS_SOURCE_NAME")
  end

  def timestamp
    @timestamp ||= Time.current.utc.to_f.round(4)
  end
end
