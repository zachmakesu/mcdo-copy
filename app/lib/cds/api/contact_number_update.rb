# Subclass of CDS::Request for Updating Contact Number
class CDS::API::ContactNumberUpdate < CDS::API
  attr_reader :user_id, :contact_numbers

  def initialize(user_id:, numbers:)
    @user_id = user_id
    @contact_numbers = numbers
  end

  def action
    "update_user_contact_no"
  end

  def params
    {
      user_id: user_id,
      contact_numbers: mapped_contact_numbers
    }
  end

  private def mapped_contact_numbers
    contact_numbers.map { |num| num.to_h.deep_transform_keys(&:underscore) }
  end
end
