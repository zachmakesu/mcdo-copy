# Subclass of CDS::Request for Checking Store Status
class CDS::API::StoreStatusCheck < CDS::API
  attr_reader :store_id

  def initialize(store_id:)
    @store_id = store_id
  end

  def action
    "check_store_status"
  end

  def params
    {
      store_id: store_id
    }
  end
end
