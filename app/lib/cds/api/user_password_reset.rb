# Subclass of CDS::Request for Resetting User Password
class CDS::API::UserPasswordReset < CDS::API
  attr_reader :email_address

  def initialize(email_address:)
    @email_address = email_address
  end

  def action
    "reset_user_password"
  end

  def params
    {
      email_address: email_address
    }
  end
end
