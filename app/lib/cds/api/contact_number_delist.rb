# Subclass of CDS::Request for Delisting Contact Number
class CDS::API::ContactNumberDelist < CDS::API
  attr_reader :user_id, :ids

  def initialize(user_id:, ids:)
    @user_id = user_id
    @ids = ids
  end

  def action
    "delist_user_contact_no"
  end

  def params
    {
      user_id: user_id,
      contact_numbers: mapped_ids
    }
  end

  private def mapped_ids
    ids.map { |id| { id: id } }
  end
end
