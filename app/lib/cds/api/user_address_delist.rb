# Subclass of CDS::Request for Delisting Address
class CDS::API::UserAddressDelist < CDS::API
  attr_reader :user_id, :ids

  def initialize(user_id:, ids:)
    @user_id = user_id
    @ids = ids
  end

  def action
    "delist_user_address"
  end

  def params
    {
      user_id: user_id,
      address: mapped_ids
    }
  end

  private def mapped_ids
    ids.map { |id| { id: id.to_i } }
  end
end
