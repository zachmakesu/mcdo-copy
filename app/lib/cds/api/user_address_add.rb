# Subclass of CDS::Request for Adding User Address
class CDS::API::UserAddressAdd < CDS::API
  attr_reader :user_id, :addresses

  def initialize(user_id:, addresses:)
    @user_id = user_id
    @addresses = addresses
    change_city_id_to_city(addresses: @addresses)
  end

  def action
    "add_user_address"
  end

  def params
    {
      user_id: user_id,
      address: mapped_addresses
    }
  end

  private

  def change_city_id_to_city(addresses:)
    addresses.each do |address|
      address.to_h["city"] = address.to_h.delete("cityId")
    end
  end

  def mapped_addresses
    addresses.map { |address| address.to_h.deep_transform_keys(&:underscore) }
  end
end
