# Subclass of CDS::Request for Adding Contact Number
class CDS::API::ContactNumberAdd < CDS::API
  attr_reader :user_id, :contact_numbers

  def initialize(user_id:, contact_numbers:)
    @user_id = user_id
    @contact_numbers = contact_numbers
  end

  def action
    "add_user_contact_no"
  end

  def params
    {
      user_id: user_id,
      contact_numbers: mapped_contact_numbers
    }
  end

  private def mapped_contact_numbers
    contact_numbers.map { |num| num.to_h.deep_transform_keys(&:underscore) }
  end
end
