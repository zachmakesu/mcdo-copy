# Subclass of CDS::Request for Checking Delivery Status
class CDS::API::DeliveryStatusCheck < CDS::API
  attr_reader :order_id

  def initialize(order_id:)
    @order_id = order_id
  end

  def action
    "customer_delivery_order_status"
  end

  def params
    {
      order_id: order_id
    }
  end
end
