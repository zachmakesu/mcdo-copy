# Subclass of CDS::Request for User Registration
class CDS::API::UserRegister < CDS::API
  attr_reader :email, :password, :confirm_password

  def initialize(email:, password:, confirm_password:)
    @email = email
    @password = password
    @confirm_password = confirm_password
  end

  def action
    "user_registration"
  end

  def params
    {
      email: email,
      password: password,
      confirm_password: password
    }
  end
end
