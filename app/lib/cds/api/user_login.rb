# Subclass of CDS::Request for User Login
class CDS::API::UserLogin < CDS::API
  attr_reader :email, :password

  def initialize(email:, password:)
    @email = email
    @password = password
  end

  def action
    "user_login"
  end

  def params
    {
      email: email,
      password: password
    }
  end
end
