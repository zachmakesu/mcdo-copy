# Subclass of CDS::Request for Getting Store List
class CDS::API::StoreListGet < CDS::API
  def initialize
  end

  def action
    "get_store_list"
  end

  def params
    {
      request: "list"
    }
  end
end
