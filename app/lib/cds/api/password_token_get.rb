# Subclass of CDS::Request for Getting Password Reset Token
class CDS::API::PasswordTokenGet < CDS::API
  attr_reader :email_address

  def initialize(email_address:)
    @email_address = email_address
  end

  def action
    "forget_password_token"
  end

  def params
    {
      email_address: email_address
    }
  end
end
