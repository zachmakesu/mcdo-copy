# Subclass of CDS::Request for Assigning Store based on City/Address
class CDS::API::StoreAssign < CDS::API
  attr_reader :city_name, :address

  def initialize(city_name:, address:)
    @city_name = city_name
    @address = address
  end

  def action
    "assign_store"
  end

  def params
    {
      city_name: city_name,
      full_address: address
    }
  end
end
