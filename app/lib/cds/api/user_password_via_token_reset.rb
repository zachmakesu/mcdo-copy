# Subclass of CDS::Request for Changing Password via Token
class CDS::API::UserPasswordViaTokenReset < CDS::API
  attr_reader :token, :password, :confirm_password

  def initialize(token:, password:, confirm_password:)
    @token = token
    @password = password
    @confirm_password = confirm_password
  end

  def action
    "forget_password"
  end

  def params
    {
      password_reset_token: token,
      password: password,
      confirm_password: confirm_password
    }
  end
end
