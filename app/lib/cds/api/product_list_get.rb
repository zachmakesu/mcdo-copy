# Subclass of CDS::Request for Getting Product List
class CDS::API::ProductListGet < CDS::API
  def initialize
  end

  def action
    "get_product_list"
  end

  def params
    {
      request: "list"
    }
  end
end
