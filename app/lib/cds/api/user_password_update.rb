# Subclass of CDS::Request for Updating User Password
class CDS::API::UserPasswordUpdate < CDS::API
  attr_reader :user_id, :old_password, :new_password, :confirm_password

  def initialize(user_id:,
                 old_password:,
                 new_password:,
                 confirm_password:)
    @user_id = user_id
    @old_password = old_password
    @new_password = new_password
    @confirm_password = confirm_password
  end

  def action
    "update_user_password"
  end

  def params
    {
      user_id: user_id,
      old_password: old_password,
      new_password: new_password,
      confirm_password: confirm_password
    }
  end
end
