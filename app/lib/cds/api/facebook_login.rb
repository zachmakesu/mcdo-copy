# Subclass of CDS::Request for Facebook Login
class CDS::API::FacebookLogin < CDS::API
  attr_reader :email_address

  def initialize(email_address:)
    @email_address = email_address
  end

  def action
    "fb_login"
  end

  def params
    {
      email_address: email_address
    }
  end
end
