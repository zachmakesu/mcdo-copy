# Subclass of CDS::Request for FB User Registration
class CDS::API::FBUserRegistration < CDS::API
  attr_reader :email_address

  def initialize(email_address:)
    @email_address = email_address
  end

  def action
    "fb_user_registration"
  end

  def params
    {
      email_address: email_address
    }
  end
end
