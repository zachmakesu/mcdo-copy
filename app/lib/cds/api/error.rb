# Class for cds api response error
class CDS::API::Error < StandardError
  def initialize(msg="Error with cds api request")
    super
  end
end
