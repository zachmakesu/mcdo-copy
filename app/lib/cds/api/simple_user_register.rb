# Subclass of CDS::Request for User Registration
class CDS::API::SimpleUserRegister < CDS::API
  attr_reader :credentials

  def initialize(credentials:)
    @credentials = credentials
  end

  def action
    "user_registration"
  end

  def params
    credentials.to_h.deep_transform_keys(&:underscore)
  end
end
