# Subclass of CDS::Request for Getting Province List
class CDS::API::ProvinceListGet < CDS::API
  def initialize
  end

  def action
    "get_province_list"
  end

  def params
    {
      request: "list"
    }
  end
end
