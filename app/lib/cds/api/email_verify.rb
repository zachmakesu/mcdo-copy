# Subclass of CDS::Request for Checking if Email is Verified
class CDS::API::EmailVerify < CDS::API
  attr_reader :email_address

  def initialize(email_address:)
    @email_address = email_address
  end

  def action
    "is_verified_email"
  end

  def params
    {
      email_address: email_address
    }
  end
end
