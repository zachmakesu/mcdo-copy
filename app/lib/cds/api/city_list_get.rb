# Subclass of CDS::Request for Getting City List
class CDS::API::CityListGet < CDS::API
  def initialize
  end

  def action
    "get_city_list"
  end

  def params
    {
      request: "list"
    }
  end
end
