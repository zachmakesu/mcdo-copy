# Subclass of CDS::Request for Updating User Profile
class CDS::API::UserProfileUpdate < CDS::API
  attr_reader :user_id, :salutation, :first_name, :middle_name,
              :last_name, :birthdate, :subscription

  def initialize(user_id:, profile:)
    @user_id      = user_id
    @salutation   = profile[:salutation]
    @first_name   = profile[:firstName]
    @middle_name  = profile[:middleName]
    @last_name    = profile[:lastName]
    @birthdate    = profile[:birthdate]
    @subscription = profile[:subscription]
  end

  def action
    "update_user_profile"
  end

  def params
    {
      user_id:      user_id,
      salutation:   salutation,
      firstname:    first_name,
      middlename:   middle_name,
      lastname:     last_name,
      birth_date:   birthdate,
      subscription: subscription
    }
  end
end
