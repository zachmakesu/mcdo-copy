# Subclass of CDS::Request for Geocoding Address
class CDS::API::AddressGeocode < CDS::API
  attr_reader :lat, :lng, :address_id

  def initialize(lat:, lng:, address_id:)
    @lat = lat
    @lng = lng
    @address_id = address_id
  end

  def action
    "save_latlong"
  end

  def params
    {
      lat: lat,
      lng: lng,
      address_id: address_id
    }
  end
end
