# Subclass of CDS::Request for Getting User Profile
class CDS::API::UserProfileGet < CDS::API
  attr_reader :user_id

  def initialize(user_id:)
    @user_id = user_id
  end

  def action
    "get_user_profile"
  end

  def params
    {
      user_id: user_id
    }
  end
end
