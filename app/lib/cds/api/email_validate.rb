# Subclass of CDS::Request for Validating Email
class CDS::API::EmailValidate < CDS::API
  attr_reader :user_id, :email_address, :validate

  def initialize(user_id:, email_address:, validate:)
    @user_id = user_id
    @email_address = email_address
    @validate = validate
  end

  def action
    "email_validation"
  end

  def params
    {
      user_id: user_id,
      email_address: email_address,
      validate: validate
    }
  end
end
