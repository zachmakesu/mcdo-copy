# Subclass of CDS::Request for cart checkout
class CDS::API::CartCheckout < CDS::API
  attr_reader :checkout_params

  def initialize(params:)
    @checkout_params = params
  end

  def action
    "customer_delivery_order"
  end

  def params
    checkout_params
  end
end
