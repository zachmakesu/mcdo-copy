# Class for building HTTP requests to CDS API
class CDS::Request
  attr_reader :api, :request

  def initialize(api:)
    @api = api
    @request = Typhoeus::Request.new(
      api_url,
      method: :post,
      body: api.body.to_json,
      headers: default_headers
    )
  end

  def default_headers
    {
      "Content-Type" => "application/json"
    }
  end

  def run
    request.on_complete do |res|
      if res.success?
        logger.info("Request success!")
      else
        logger.warn("HTTP request failed: " + res.code.to_s)
      end
    end
    @internal_response = request.run
  end

  def response
    body = internal_response.body
    data = if body.empty?
             { status: internal_response.code,
               error: internal_response.return_message }
           else
             JSON.parse(body)
           end
    @response ||= CDS::Response.new(data: data)
  end

  private

  attr_reader :internal_response

  def api_url
    ENV.fetch("CDS_API_URL")
  end

  def logger
    Rails.logger
  end
end
