# Class for representing a province
class CDS::Model::Province < CDS::Model
  def id
    params[:id]
  end

  def prefix
    "province"
  end

  def self.all_key
    "provinces"
  end

  def self.all
    cached_provinces = Redis::Set.new(all_key, marshal: true)
    if cached_provinces.empty?
      provinces.each do |province|
        cached_provinces << province
        cache_individual_province(province: province)
      end
    end
    value = cached_provinces.value
    cached_provinces.clear
    value
  end

  private_class_method

  def self.provinces
    api = CDS::API::ProvinceListGet.new
    request = CDS_API_REQUEST.new(api: api).tap(&:run)
    if request.response.success?
      request.response.object.province_list
    else
      []
    end
  end

  def self.cache_individual_province(province:)
    new(params: province)
  end
end
