# Class for representing user address
class CDS::Model::Address < CDS::Model
  def id
    params[:id]
  end

  def prefix
    "address"
  end
end
