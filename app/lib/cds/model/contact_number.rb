# Class for representing CDS Contact Number
class CDS::Model::ContactNumber < CDS::Model
  def id
    params[:id]
  end

  def prefix
    "contact_number"
  end
end
