# Class for representing a city
class CDS::Model::City < CDS::Model
  def id
    params[:id]
  end

  def prefix
    "city"
  end

  def self.all_key
    "cities"
  end

  def self.all
    cached_cities = Redis::Set.new(all_key, marshal: true)
    if cached_cities.empty?
      cities.each do |city|
        cached_cities << city
        cache_individual_city(city: city)
      end
    end
    value = cached_cities.value
    cached_cities.clear
    value
  end

  private_class_method

  def self.cities
    api = CDS::API::CityListGet.new
    request = CDS_API_REQUEST.new(api: api).tap(&:run)
    if request.response.success?
      request.response.object.city_list
    else
      []
    end
  end

  def self.cache_individual_city(city:)
    new(params: city)
  end
end
