# Class for representing user profile
class CDS::Model::UserProfile < CDS::Model
  def id
    params[:user_id]
  end

  def prefix
    "user_profile"
  end
end
