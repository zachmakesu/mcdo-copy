# Class for Action-Path Mapping for API Proxy
class CDS::ProxyMapping
  MAPPING = {
    "add_user_address" => "Address/add",
    "add_user_contact_no" => "Contact/add",
    "update_user_address" => "Address/update",
    "update_user_contact_no" => "Contact/update",
    "delist_user_address" => "Address/delist",
    "delist_user_contact_no" => "Contact/delist",
    "email_validation" => "User/validate_email",
    "fb_login" => "User/fb_login",
    "fb_user_registration" => "User/fb_register",
    "get_city_list" => "City/all",
    "get_province_list" => "Province/all",
    "get_user_profile" => "User",
    "reset_user_password" => "User/reset_password",
    "update_user_profile" => "User/update",
    "user_login" => "User/login",
    "user_registration" => "User/register",
    # Duplicate endpoint for fetching user profile?
    # "get_user_profile" => "User/profile",
    "update_user_password" => "User/update_password",
    "customer_delivery_order" => "Order/add"
  }.freeze

  attr_reader :action
  def initialize(action: "")
    @action = action
  end

  def path
    MAPPING[action]
  end
end
