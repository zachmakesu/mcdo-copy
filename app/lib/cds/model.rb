# Base class for representing CDS Models
class CDS::Model
  attr_reader :params

  def initialize(params:)
    @params = params
    hk = Redis::HashKey.new(key)
    params.each_pair do |k, v|
      hk[k] = v
    end
  end

  def id
    raise NotImplementedError
  end

  def prefix
    raise NotImplementedError
  end

  def key
    "#{prefix}#{id}"
  end

  def self.prefix
    new(params: {}).prefix
  end

  def self.find(id)
    key = "#{prefix}#{id}"
    hk = Redis::HashKey.new(key)
    hk.value.empty? ? nil : OpenStruct.new(hk.value)
  end
end
