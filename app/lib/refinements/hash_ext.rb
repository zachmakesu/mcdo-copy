module Refinements
  # A refinements for Hash
  module HashExt
    refine Hash do
      def to_deep_open_struct
        JSON.parse to_json, object_class: OpenStruct
      end
    end
  end
end
