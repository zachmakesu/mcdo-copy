# Controller for managing Alacarte model
class Admin::AlacartesController < AdminController
  set_tab :alacarte
  before_action :set_alacarte, only: %i[show edit update destroy]

  def index
    @alacartes = if (search_keyword = params[:search])
                   Alacarte.search(keyword: search_keyword)
                 else
                   Alacarte.includes(:category, :variances).all
                 end
    @alacartes = Kaminari.paginate_array(@alacartes).page(params[:page]).per(10)
  end

  def show
  end

  def new
    @alacarte = Alacarte.new
    @alacarte.variances.new
    @alacarte.variances.each do |v|
      v.options.build
    end
  end

  def create
    @alacarte = Alacarte.new(alacarte_params)
    if @alacarte.save
      redirect_to admin_alacartes_path, notice: "Success"
    else
      render :new
    end
  end

  def edit
  end

  def update
    return toggle_display if params[:display].present?
    if @alacarte.update_attributes(alacarte_params)
      redirect_to admin_alacartes_path, notice: "Success"
    else
      render :edit
    end
  end

  def destroy
    @alacarte.destroy
    message = "Successfully deleted"
    redirect_to admin_alacartes_path, notice: message
  end

  private

  def alacarte_params
    permitted_params = :name, :description, :discounted_rice,
                       :image_source, :category_id, :schedule_id
    option_attr = %i[id choice_id section_id _destroy]
    variance_attr = [:id, :menu_name, :name, :price, :poscode,
                     :image_source, :_destroy, options_attributes: option_attr]
    product_categories_attr = %i[id category_id categorizeable_type _destroy]
    params.require(:alacarte).permit(
      permitted_params,
      variances_attributes: variance_attr,
      product_categories_attributes: product_categories_attr
    )
  end

  def set_alacarte
    @alacarte = Alacarte.find_by(id: params[:id])
  end

  def toggle_display
    value = params[:display] == "true" ? Time.zone.today : nil
    @alacarte.update(displayed_at: value)
    render json: { message: "Success." }, status: :ok
  end
end
