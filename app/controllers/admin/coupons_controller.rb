# Controller for managing Coupons model
class Admin::CouponsController < AdminController
  set_tab :coupon
  before_action :set_coupon, only: %i[show edit update destroy]

  def index
    @coupons = if (search_keyword = params[:search])
                 Coupon.search(keyword: search_keyword)
               else
                 Coupon.all
               end
    @coupons = @coupons.page(params[:page]).per(10)
  end

  def new
    @coupon = Coupon.new
    @coupon.products.new
  end

  def create
    @coupon = Coupon.new(coupon_params)
    if @coupon.save
      redirect_to admin_coupons_path, notice: "Successfully Created"
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @coupon.update(coupon_params)
      redirect_to admin_coupons_path, notice: "Success"
    else
      render :edit
    end
  end

  def destroy
    @coupon.destroy
    redirect_to admin_coupons_path, notice: "Successfully deleted"
  end

  private

  def set_coupon
    @coupon = Coupon.find(params[:id])
  end

  def permitted_params
    %i[
      classification description detail_image_source image_source name
      usage_type usage_count validity_start_at validity_end_at discount poscode
    ]
  end

  def coupon_params
    coupon_product_attr = :id, :productable_id, :productable_type, :_destroy
    params.require(:coupon).permit(
      permitted_params,
      products_attributes:
      [
        coupon_product_attr,
        sub_products_attributes: [coupon_product_attr]
      ]
    )
  end
end
