# Controller for managing User model
class Admin::UsersController < AdminController
  before_action :set_user, only: %i[edit update destroy]
  set_tab :users
  def index
    @users = User.all.order(created_at: :asc).page(params[:page]).per(10)
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      redirect_to admin_users_path, notice: "Success"
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @user.update(user_params)
      redirect_to admin_users_path, notice: "Success"
    else
      render :edit
    end
  end

  def destroy
    @user.destroy
    redirect_to admin_users_path, notice: "Success"
  end

  private

  def set_user
    @user = User.find(params[:id])
  end

  def user_params
    permitted_params = %i[first_name
                          last_name
                          email
                          password
                          password_confirmation
                          avatar]
    params.require(:user).permit(permitted_params)
  end
end
