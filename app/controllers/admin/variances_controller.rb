# Variances Controller
class Admin::VariancesController < AdminController
  set_tab :menu_items
  before_action :set_variance, only: %i[edit update destroy]

  def index
    page = params[:page]
    @variances = if (search_keyword = params[:search])
                   Variance.search(keyword: search_keyword)
                 else
                   Variance.all
                 end
    @variances = Kaminari.paginate_array(@variances).page(page).per(10)
  end

  def new
    @variance = Variance.new
  end

  def create
    @variance = Variance.new(variance_params)
    if @variance.save
      redirect_to admin_variances_path, notice: "Success"
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @variance.update(variance_params)
      redirect_to admin_variances_path, notice: "Success"
    else
      render :edit
    end
  end

  def destroy
    @variance.destroy
    message = "Successfully deleted"
    redirect_to admin_variances_path, notice: message
  end

  private

  def set_variance
    @variance = Variance.find_by(id: params[:id])
  end

  def variance_params
    permitted_params = :name, :menu_name, :code_name, :poscode, :price,
                       :variable_id, :variable_type, :group_id, :image_source,
                       :best_with
    option_attr = %i[id choice_id section_id _destroy]
    params.require(:variance).permit(
      permitted_params,
      options_attributes: option_attr
    )
  end
end
