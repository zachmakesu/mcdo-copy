# Controller for displaying Product Information
class Admin::ProductsController < AdminController
  set_tab :featured_meals, only: :featured_meals
  before_action :validate_type, only: %i[featured_meal unfeatured_meal]

  def index
    @alacartes_count = Alacarte.count
    @value_meals_count = ValueMeal.count
    @meal_options_count = MealOption.count
  end

  def fetch_by_type
    collections = fetch_products(type: params[:type])
    products = collections.map do |collection|
      {
        id: collection.id, name: collection.name
      }
    end
    render json: { products: products }
  end

  def featured_meal
    meal = params[:type].constantize.find(params[:id])
    response = {}
    response[:success] = false
    if meal.featured_meal.nil?
      FeaturedMeal.create(featured_meal_params)
      response[:success] = true
      response[:message] = "#{meal.name} Successfully set as featured!"
    else
      response[:message] = "#{meal.name} is already a Featured Meal"
    end
    render json: response
  end

  def unfeatured_meal
    filtered_featured_meal = FeaturedMeal.filter_by(params[:type])
    featured_meal = filtered_featured_meal&.find_by(featureable_id: params[:id])
    meal = featured_meal&.featureable
    response = {}
    response[:success] = true
    response[:message] = "#{meal.name} Successfully set as unfeatured!"
    featured_meal.destroy

    render json: response
  end

  def featured_meals
    featured_meals = FeaturedMeal.all.map(&:featureable)
    @featured_meals = Kaminari.paginate_array(featured_meals)
                              .page(params[:page]).per(10)
  end

  private

  def validate_type
    types = FeaturedMeal::TYPES
    type = params[:type]
    return invalid_type_response(type: type) unless types.include?(type)
  end

  def invalid_type_response(type:)
    response = {}
    response[:success] = false
    response[:message] = "#{type} is not a valid featured meal type!"
    render json: response
  end

  def featured_meal_params
    {
      featureable_id: params[:id],
      featureable_type: params[:type]
    }
  end

  def fetch_products(type:)
    return unless type
    begin
      type.constantize.all
    rescue StandardError => e
      Rail.logger.info(e)
      []
    end
  end
end
