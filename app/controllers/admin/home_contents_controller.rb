# Home Content Controller
class Admin::HomeContentsController < AdminController
  set_tab :cms
  set_tab :home, :cms
  before_action :set_content, only: %i[edit update destroy]

  def index
    @contents = HomeContent.all.order(sequence_id: :asc)
                           .page(params[:page]).per(10)
  end

  def new
    @content = HomeContent.new
  end

  def create
    @content = HomeContent.new(content_params)
    if @content.save
      @content.insert_at(params[:sequence].to_i)
      redirect_to admin_home_contents_path, notice: "Success"
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @content.update(content_params)
      @content.insert_at(params[:sequence].to_i) if params[:sequence].present?
      redirect_to admin_home_contents_path, notice: "Success"
    else
      render :edit
    end
  end

  def destroy
    @content.destroy
    redirect_to admin_home_contents_path, notice: "Success"
  end

  private

  def set_content
    @content = HomeContent.find(params[:id])
  end

  def content_params
    permitted_params = %i[
      name sequence image_source_app
      image_source_web groupable_id groupable_type availability
    ]
    params.require(:home_content).permit(permitted_params)
  end
end
