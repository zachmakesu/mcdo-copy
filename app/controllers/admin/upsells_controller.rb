# Controller for managing Upsell Model
class Admin::UpsellsController < AdminController
  before_action :set_upsell, only: %i[edit show update destroy]

  def index
    @upsells = if (search = params[:search])
                 Upsell.search(keyword: search)
               else
                 Upsell.all
               end
    @upsells = @upsells.page(params[:page]).per(10)
  end

  def new
    @upsell = Upsell.new
  end

  def create
    @upsell = Upsell.new(upsell_params)
    if @upsell.save
      redirect_to admin_upsells_path, notice: "Successfully Created"
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @upsell.update(upsell_params)
      redirect_to admin_upsells_path(@upsell), notice: "Successfully Updated"
    else
      render :edit
    end
  end

  def destroy
    @upsell.destroy
    redirect_to admin_upsells_path, notice: "Successfully deleted"
  end

  private

  def set_upsell
    @upsell = Upsell.find_by(id: params[:id])
  end

  def upsell_params
    params.require(:upsell).permit(:name, :alacarte_id)
  end
end
