# Controller for User Manual
class Admin::UserManualsController < AdminController
  set_tab :cms
  set_tab :user_manual, :cms

  before_action :set_user_manual, only: %i[edit update destroy]

  def index
    @user_manuals = UserManual.all.page(params[:page]).per(10)
  end

  def new
    @user_manual = UserManual.new
  end

  def create
    @user_manual = UserManual.new(user_manual_params)
    if @user_manual.save
      @user_manual.insert_at(params[:sequence].to_i)
      redirect_to admin_user_manuals_path, notice: "Success!"
    else
      error_message = @user_manual.errors.full_messages.to_sentence
      redirect_to admin_user_manuals_path, alert: "Error!, #{error_message}"
    end
  end

  def edit
  end

  def update
    if @user_manual.update(user_manual_params)
      @user_manual.insert_at(params[:sequence].to_i) if params[:sequence]
      redirect_to admin_user_manuals_path, notice: "Success!"
    else
      error_message = @user_manual.errors.full_messages.to_sentence
      redirect_to admin_user_manuals_path, alert: "Error!, #{error_message}"
    end
  end

  def destroy
    @user_manual.destroy
    redirect_to admin_user_manuals_path, notice: "Success!"
  end

  private

  def user_manual_params
    params.require(:user_manual).permit(:image_source, :description)
  end

  def set_user_manual
    @user_manual = UserManual.find(params[:id])
    error_message = "Error!, record not found"
    path = admin_user_manuals_path
    redirect_to path, alert: error_message unless @user_manual
  end
end
