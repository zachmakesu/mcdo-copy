# Controller for managing Promo Model
class Admin::PromosController < AdminController
  set_tab :cms
  set_tab :promo, :cms
  before_action :set_promo, only: %i[edit update destroy]

  def index
    @promos = Promo.all.page(params[:page]).per(10)
  end

  def new
    @promo = Promo.new
  end

  def create
    @promo = Promo.new(promo_params)
    if @promo.save
      redirect_to admin_promos_path, notice: "Success"
    else
      render :new
    end
  end

  def edit
  end

  def update
    mod_params = promo_params
    mod_params[:is_active] = mod_params.fetch("is_active", false)
    mod_params[:is_published] = mod_params.fetch("is_published", false)
    if @promo.update(mod_params)
      redirect_to admin_promos_path, notice: "Success"
    else
      render :edit
    end
  end

  def destroy
    @promo.destroy
    redirect_to admin_promos_path, notice: "Success"
  end

  private

  def set_promo
    @promo = Promo.find(params[:id])
  end

  def promo_params
    permitted_params = %i[
      name category is_active link tags is_published
      validity_start_at validity_end_at content image_source
    ]
    params.require(:promo)
          .permit(permitted_params)
  end
end
