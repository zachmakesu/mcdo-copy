# Notifications Controller
class Admin::NotificationsController < AdminController
  set_tab :cms
  set_tab :notification, :cms

  before_action :set_notification, only: %i[edit update destroy]
  def index
    @notifications = Notification.all.page(params[:page]).per(10)
  end

  def new
    @notification = Notification.new
  end

  def create
    @notification = Notification.new(notifications_params)
    if @notification.save
      redirect_to admin_notifications_path, notice: "Success"
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @notification.update(notifications_params)
      redirect_to admin_notifications_path, notice: "Success"
    else
      render :edit
    end
  end

  def destroy
    @notification.destroy
    redirect_to admin_notifications_path, notice: "Success"
  end

  private

  def set_notification
    @notification = Notification.find(params[:id])
  end

  def notifications_params
    params.require(:notification)
          .permit(
            :title, :short_description, :description, :image_source, :category
          )
  end
end
