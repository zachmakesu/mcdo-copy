# Controller for managing Meal Options
class Admin::MealOptionsController < AdminController
  set_tab :with_items
  before_action :set_meal_option, only: %i[show edit update destroy]

  def index
    @default_classification = MealOption::DEFAULT_CLASSIFICATION
    meals = fetch_meal_for(
      meal_type: params[:meal_type],
      search_term: params[:search],
      default_menu: @default_classification
    )
    meals_and_tab(meals: meals)
  end

  def new
    @meal_option = MealOption.new
  end

  def create
    @meal_option = MealOption.new(meal_option_params)
    if @meal_option.save
      redirect_to admin_meal_option_path(@meal_option), notice: "Success"
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @meal_option.update(meal_option_params)
      redirect_to admin_meal_options_path, notice: "Success"
    else
      render :edit
    end
  end

  def destroy
    @meal_option.destroy
    message = "Successfully deleted"
    redirect_to admin_meal_options_path, notice: message
  end

  def fetch_sorted_meals
    collections = MealOption.fetch_by(type: params[:type], size: params[:size])
    meals = collections.map do |collection|
      { id: collection.id, name: collection.name }
    end
    render json: { meals: meals }
  end

  private

  def set_meal_option
    @meal_option = MealOption.find_by(id: params[:id])
  end

  def meal_option_params
    permitted_params = :name, :long_name, :code_name, :poscode, :price, :size,
                       :classification, :group_id, :image_source, :is_default
    params.require(:meal_option).permit(permitted_params)
  end

  def fetch_meal_for(meal_type:, search_term:, default_menu:)
    type = meal_type ? meal_type : default_menu
    {}.tap do |meals|
      meals[:data] = if search_term
                       MealOption.search(keyword: search_term, type: type)
                     else
                       MealOption.fetch_by(type: type)
                     end
      meals[:type] = meal_type ? type : :coke_class
    end
  end

  def meals_and_tab(meals:)
    data = meals[:data].to_a.sort_by(&:created_at)
    @meal_options = Kaminari.paginate_array(data).page(params[:page]).per(10)
    set_tab meals[:type], :with_items
  end
end
