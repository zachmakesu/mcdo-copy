# Categories Controller
class Admin::CategoriesController < AdminController
  set_tab :cms
  set_tab :category, :cms
  before_action :set_category, only: %i[edit update destroy]

  def index
    @categories = Category.all.order(sequence_id: :asc)
                          .page(params[:page]).per(10)
  end

  def new
    @category = Category.new
  end

  def create
    @category = Category.new(category_params)
    if @category.save
      redirect_to admin_categories_path, notice: "Success"
    else
      render :new
    end
  end

  def edit
  end

  def update
    mod_params = category_params
    mod_params[:is_active] = mod_params.fetch("is_active", false)
    if @category.update(mod_params)
      redirect_to admin_categories_path, notice: "Success"
    else
      render :edit
    end
  end

  def destroy
    @category.destroy
    redirect_to admin_categories_path, notice: "Success"
  end

  private

  def set_category
    @category = Category.find(params[:id])
  end

  def category_params
    params.require(:category).permit(:name, :sequence_id, :is_active)
  end
end
