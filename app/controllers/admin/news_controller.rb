# Controller for managing News model
class Admin::NewsController < AdminController
  before_action :set_news, only: %i[show edit update destroy]
  before_action :set_categories, only: %i[new create edit update]

  def index
    @news = News.all
  end

  def show
  end

  def new
    @news = News.new
  end

  def create
    @news = News.new(news_params)
    if @news.save
      redirect_to admin_news_path(@news), notice: "Success"
    else
      render :new
    end
  end

  def edit
  end

  def update
    return toggle_display if params[:display].present?
    if @news.update_attributes(news_params)
      redirect_to admin_news_path(@news), notice: "Success"
    else
      render :new
    end
  end

  def destroy
    @news.destroy
    message = "Successfully deleted"
    redirect_to admin_news_index_path, notice: message
  end

  private

  def news_params
    permitted_params = :title, :description, :image_source,
                       :start_date, :category
    params.require(:news).permit(permitted_params)
  end

  def set_news
    @news = News.find_by(id: params[:id])
  end

  def set_categories
    @categories = News.categories
  end

  def toggle_display
    value = params[:display] == "true" ? Time.zone.today : nil
    @news.update(displayed_at: value)
    render json: { message: "Success." }, status: :ok
  end
end
