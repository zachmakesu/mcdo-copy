# Controller for managing Branch model
class Admin::BranchesController < AdminController
  set_tab :store
  before_action :set_branch, only: %i[show edit update destroy]
  before_action :set_areas, only: %i[new create edit update]
  def index
    @branches = if (search_keyword = params[:search])
                  Branch.search(keyword: search_keyword)
                else
                  Branch.all
                end
    @branches = @branches.order(created_at: :asc).page(params[:page]).per(10)
  end

  def new
    @branch = Branch.new
    @branch.delivery_numbers.new
  end

  def show
  end

  def create
    @branch = Branch.new(branch_params)
    if @branch.save
      redirect_to admin_branch_path(@branch), notice: "Success"
    else
      render :new
    end
  end

  def edit
  end

  def update
    mod_params = branch_params
    mod_params[:offerings] = params[:offerings]&.join(";")
    if @branch.update_attributes(mod_params)
      redirect_to admin_branches_path, notice: "Success"
    else
      render :new
    end
  end

  def destroy
    @branch.destroy
    message = "Successfully deleted"
    redirect_to admin_branches_path, notice: message
  end

  private

  def branch_params
    permitted_params = :name, :description, :area_id,
                       :address, :image_source
    num_attr = %i[id number _destroy]
    params.require(:branch).permit(permitted_params,
                                   delivery_numbers_attributes: num_attr)
  end

  def set_branch
    @branch = Branch.find_by(id: params[:id])
  end

  def set_areas
    @areas = Area.all
  end
end
