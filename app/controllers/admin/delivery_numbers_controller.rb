# Controller for Managing Delivery Number Model
class Admin::DeliveryNumbersController < AdminController
  before_action :set_delivery_number

  def edit
  end

  def update
    if @delivery_number.update(permitted_params)
      redirect_to admin_branches_path,
                  notice: "Successfully updated delivery number!"
    else
      render :edit
    end
  end

  private

  def set_delivery_number
    @delivery_number = DeliveryNumber.find(params[:id])
  end

  def permitted_params
    params.require(:delivery_number).permit(:number)
  end
end
