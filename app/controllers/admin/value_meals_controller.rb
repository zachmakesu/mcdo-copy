# Controller for managing ValueMeals
class Admin::ValueMealsController < AdminController
  using StringToBoolRefinement
  before_action :set_value_meal, only: %i[
    show edit update destroy
    update_status
  ]
  set_tab :menu_entries

  def index
    @default_category = ValueMeal::DEFAULT_MENU_ENTRY_CATEGORY
    meals = fetch_meal_for(
      category: params[:category],
      search: params[:search],
      default_menu: @default_category
    )
    @value_meals = meals[:data].order(created_at: :asc)
                               .page(params[:page]).per(10)
    set_tab meals[:category], :menu_entries
  end

  def show
  end

  def new
    @value_meal = ValueMeal.new
    @value_meal.variances.new
    @value_meal.variances.each do |v|
      v.options.build
    end
  end

  def create
    mod_params = value_meal_params
    mod_params[:active_at] = active?(status: params.fetch("is_active", false))
    @value_meal = ValueMeal.new(mod_params)

    if @value_meal.save
      redirect_to admin_value_meals_path, notice: "Success"
    else
      render :new
    end
  end

  def edit
  end

  def update
    return toggle_display if params[:display].present?
    mod_params = value_meal_params
    mod_params[:active_at] = active?(status: params.fetch("is_active", false))
    if @value_meal.update_attributes(mod_params)
      redirect_to admin_value_meals_path, notice: "Success"
    else
      render :edit
    end
  end

  def destroy
    @value_meal.destroy
    message = "Successfully deleted"
    redirect_to admin_value_meals_path, notice: message
  end

  def update_status
    if params[:status].to_s.to_bool
      @value_meal.activate!
    else
      @value_meal.deactivate!
    end
    render json: { message: "Success!", status: :ok }
  end

  private

  def value_meal_params
    option_attr = %i[id choice_id section_id _destroy]
    variance_attr = [:id, :menu_name, :name, :price,
                     :poscode, :image_source, :_destroy,
                     options_attributes: option_attr]
    product_categories_attr = %i[id category_id categorizeable_type _destroy]
    params.require(:value_meal).permit(
      permitted_params,
      variances_attributes: variance_attr,
      product_categories_attributes: product_categories_attr
    )
  end

  def permitted_params
    %i[
      name is_breakfast category_id schedule_id value_meal_id discounted_rice
      image_source alacarte_id availability active_at
    ]
  end

  def set_value_meal
    @value_meal = ValueMeal.find_by(id: params[:id])
  end

  def toggle_display
    value = params[:display] == "true" ? Time.zone.today : nil
    @value_meal.update(displayed_at: value)
    render json: { message: "Success." }, status: :ok
  end

  def fetch_meal_for(category:, search:, default_menu:)
    category ||= default_menu
    {}.tap do |meals|
      meals[:data] = if search
                       ValueMeal.search(keyword: search, category: category)
                     else
                       ValueMeal.sort_by(category: category)
                     end
      meals[:category] = category
    end
  end

  def active?(status:)
    status ? Time.zone.now : nil
  end
end
