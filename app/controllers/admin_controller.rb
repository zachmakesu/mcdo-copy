# Base Controller for resources requiring Admin
class AdminController < ApplicationController
  layout "admin"
  def index
    @users = User.all
    @meals = ValueMeal.all
    @coupons = Coupon.all.limit(3)
    @orders_count = Order.count
    @orders_today = Order.new_today
    @displayed_orders_today = @orders_today.limit(5)
  end
end
