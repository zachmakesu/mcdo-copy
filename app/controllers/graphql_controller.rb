# Lone graphql controller that handles every request
class GraphqlController < ApplicationController
  protect_from_forgery with: :null_session
  before_action :authenticate_query
  def execute
    variables = ensure_hash(params[:variables])
    query = params[:query]
    operation_name = params[:operationName]
    result = McdoWebSchema.execute(
      query,
      variables: variables,
      context: context,
      operation_name: operation_name
    )
    render json: result
  end

  # Handle form data, JSON body, or a blank value
  private def ensure_hash(ambiguous_param)
    case ambiguous_param
    when String
      if ambiguous_param.present?
        ensure_hash(JSON.parse(ambiguous_param))
      else
        {}
      end
    when Hash, ActionController::Parameters then ambiguous_param
    when nil then {}
    else
      raise ArgumentError, "Unexpected parameter: #{ambiguous_param}"
    end
  end

  private

  def authenticate_query
    return unless request.headers["Authorization"] =~ /Bearer (.+)/
    jwt = Regexp.last_match(1)
    decoded_jwt = JWTManager.decode(jwt: jwt)
    @jwt_error = decoded_jwt.error_message
    user_id = decoded_jwt.payload&.fetch("user_id", nil)
    @user = Customer.find_by(cds_id: user_id)
  end

  def context
    {
      jwt_error: @jwt_error,
      current_user: @user,
      browser: browser
    }
  end
end
