module FilterHelper
  def complete_date_format(date:)
    date.try(:strftime, "%B %d, %Y")
  end

  def complete_date_time_short(date:)
    date.try(:strftime, "%b %d, %Y %I:%M %p")
  end
end
