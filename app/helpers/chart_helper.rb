module ChartHelper
  # dummy data for chart
  def orders_count
    # https://github.com/ankane/groupdate
    # {
    #   Sat, 28 May 2016 => 50,
    #   Sun, 29 May 2016 => 100,
    #   Mon, 30 May 2016 => 34
    # }
    Order.group_by_day(:created_at).count
  end

  def daily_registrations_chart
    column_chart(
      [
        generate_stacks(name: "Android", stack: "stack1"),
        generate_stacks(name: "IOS", stack: "stack2"),
        generate_stacks(name: "Web", stack: "stack3")
      ],
      height: "500px",
      colors: ["#000000", "#F5CB13", "#C40513"],
      width: "100%",
      legend: false
    )
  end

  def generate_stacks(name:, stack:)
    {
      name: name,
      stack: stack,
      data: [["Mar 08", 1], ["Mar 09", 2], ["Mar 10", 3], ["Mar 11", 4]]
    }
  end
end
