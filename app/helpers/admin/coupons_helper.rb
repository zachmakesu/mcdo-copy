module Admin::CouponsHelper
  def coupon_classifications
    Coupon::CLASSIFICATIONS
  end

  def coupon_types
    Coupon::TYPES
  end
end
