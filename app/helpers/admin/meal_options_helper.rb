module Admin::MealOptionsHelper
  def meal_options
    ::MealOption.all
  end

  def meal_options_sizes
    ::MealOption::SIZES
  end

  def meal_options_classifications
    ::MealOption::CLASSIFICATIONS
  end

  def fetch_meal_options_by(type:, size: nil)
    MealOption.fetch_by(type: type, size: size)
  end
end
