module Admin::HomeContentsHelper
  def home_contents
    HomeContent.all
  end

  def empty_layouts_for(contents:)
    default_layout_count = 9
    layouts = initialize_layout_content(
      contents: contents,
      default_count: default_layout_count
    )
    if contents.count < default_layout_count
      (contents.to_a << layouts).flatten.compact.sort_by(&:sequence_id)
    else
      contents
    end
  end

  def initialize_layout_content(contents:, default_count:)
    (1..default_count).to_a.map do |count|
      HomeContent.new(sequence_id: count) if contents.map(&:sequence_id)
                                                     .exclude?(count)
    end
  end

  def grouped_home_contents
    collections = home_contents.order(sequence_id: :asc)
    contents = empty_layouts_for(contents: collections)
    contents.in_groups_of(3).collect
  end

  def generate_layout_column_item(styles:, image:, content:)
    data = { layout: "", id: content.sequence_id }
    content_tag :div, class: styles, style: image, data: data do
      content_tag :div, class: "level has-100-percent-height" do
        left = generate_column_item_for(content: content.name, position: "left")
        right = generate_column_item_for(
          content: content.sequence_id, position: "right"
        )
        left + right
      end
    end
  end

  def generate_layout_column(content:)
    styles = "column is-black-bg has-3-opacity "\
             "has-100px-height layout-image-container"
    image = "background-image:url(#{content.image_source_web.url})"
    generate_layout_column_item(styles: styles, image: image, content: content)
  end

  def column_style_for(position:)
    style_sequence = " layout-sequence-number is-size-7"
    style_name = " white-text font-is-akzidenz-bold is-size-6"
    styles = "has-text-centered"
    layout_style = position == "right" ? style_sequence : style_name
    styles + layout_style
  end

  def generate_column_item_for(content:, position:)
    styles = column_style_for(position: position)
    content_tag :div, class: "level-#{position}" do
      content_tag :div, class: "level-item" do
        content_tag :p, content.to_s.upcase, class: styles
      end
    end
  end
end
