module Admin::ValueMealsHelper
  def value_meals
    ::ValueMeal.all
  end

  def generate_status(active:)
    active ? "Active" : "Inactive"
  end
end
