module Admin::SectionHelper
  def sections
    Section.all
  end

  def fetch_section(type:)
    categories = Section::CATEGORIES
    Section.send(type) if categories.include?(type)
  rescue NoMethodError
    nil
  end
end
