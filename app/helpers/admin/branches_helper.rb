module Admin::BranchesHelper
  def branch_offerings
    ::Branch::OFFERINGS
  end

  def grouped_offerings(offerings:)
    offerings&.split(";")&.in_groups_of(3)&.collect(&:compact)
  end

  def generate_branch_offering_item(item:)
    styles = "has-30px-border-radius has-5px-padding is-light-gray-bg"
    content_tag :div, class: "tile is-child" do
      content_tag :span, item, class: styles
    end
  end
end
