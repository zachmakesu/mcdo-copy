module Admin::ProductsHelper
  def product_categories
    {
      ValueMeal: "Value Meals",
      MealOption: "Meal Options",
      Alacarte: "Alacartes"
    }
  end
end
