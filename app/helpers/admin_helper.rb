module AdminHelper
  def meal_type(type:)
    case type
    when "meal_option" then "MealOption"
    when "value_meal"  then "ValueMeal"
    when "alacarte"    then "Alacarte"
    else ""
    end
  end

  def current_date
    Time.zone.today.strftime("%b %d, %Y")
  end

  def new_users_count_today
    User.new_today.count
  end

  def new_users_count_this_month
    User.new_this_month.count
  end

  def photo_upload_label_for(photo_is_present:)
    photo_is_present ? "Change Photo" : "Upload Photo"
  end

  def product_type_for(type:)
    alacarte = "Alacarte"
    meal_option = "MealOption"
    case type
    when alacarte then alacarte
    when meal_option then meal_option
    else "ValueMeal"
    end
  end

  def products_for(type:)
    case type
    when "Alacarte" then alacartes
    when "MealOption" then meal_options
    else value_meals
    end
  end

  def available_hours
    AvailableHour.all
  end
end
