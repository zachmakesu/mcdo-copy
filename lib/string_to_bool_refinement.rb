# Refinement for String to  Boolean Validation
module StringToBoolRefinement
  refine String do
    def to_bool
      arg = self
      return true   if arg == true  || arg =~ /(true|t|yes|y|1)$/i
      return false  if arg == false || arg.blank? || arg =~ /(false|f|no|n|0)$/i
      raise ArgumentError, "invalid value for Boolean: \'#{arg}\'"
    end
  end
end
