require "test_helper"

class NotificationTest < ActiveSupport::TestCase
  setup do
    @notification = build(:notification)
    @invalid_notification = build(:invalid_notification)
  end

  test "valid notification" do
    assert @notification.valid?
  end

  test "invalid notification" do
    refute @invalid_notification.valid?
  end
end
