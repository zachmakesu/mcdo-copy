require "test_helper"

class CustomerTest < ActiveSupport::TestCase
  setup do
    @customer = build(:customer)
    @invalid_customer = build(:invalid_customer)
  end

  test "valid customer" do
    assert @customer.valid?
  end

  test "invalid customer" do
    refute @invalid_customer.valid?
  end
end
