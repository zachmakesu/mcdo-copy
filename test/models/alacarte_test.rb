require "test_helper"

class AlacarteTest < ActiveSupport::TestCase
  setup do
    @alacarte = build(:alacarte)
    @invalid_alacarte = build(:invalid_alacarte)
  end

  test "valid alacarte" do
    assert @alacarte.valid?
  end

  test "invalid alacarte" do
    refute @invalid_alacarte.valid?
  end

  test "search valid product" do
    @alacarte.save
    searched_product = Alacarte.search(keyword: @alacarte.name)
    assert_includes searched_product, @alacarte, "should include searched product"
  end

  test "search invalid product" do
    @alacarte.save
    searched_product = Alacarte.search(keyword: Faker::Name.name)
    assert_empty searched_product
  end

  test "#bundle?" do
    alacarte = create(:alacarte_bundle)
    assert alacarte.bundle?, "should return true if alacarte is bundle"
  end
end
