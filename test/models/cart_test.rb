require "test_helper"

class CartTest < ActiveSupport::TestCase
  setup do
    @customer = create(:customer)
    @cart = build(:cart, customer_id: @customer.id)
  end

  test "valid cart" do
    assert @cart.valid?
  end

  test "invalid cart" do
    @cart.customer_id = nil
    refute @cart.valid?
  end
end
