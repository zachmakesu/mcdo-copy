require "test_helper"

class CartsVarianceTest < ActiveSupport::TestCase
  setup do
    @customer = create(:customer)
    @section = create(:section)
    @meal_option = create(:meal_option)
    @bundle_meal_option = create(:meal_option)
    @ac_variance = create(:variance, variable: create(:alacarte))
    @vm_variance = create(:variance, variable: create(:value_meal))
    @ac_bundle_variance = create(:variance, variable: create(:alacarte_bundle))
    @vm_variance.options.create(meal_option: @meal_option, section: @section)
    @ac_bundle_variance.variable.product_bundle.options.create(meal_option: @bundle_meal_option, section: @section)
    @cart = create(:cart, customer_id: @customer.id)
  end

  test "add valid variance to cart" do
    assert_difference -> { @cart.reload.all_items.count }, 2 do
      assert_difference -> { @cart.value_meals.count }, 1 do
        item = @cart.items.build(variance: @vm_variance, best_with: @vm_variance.best_with, best_with_quantity: 1)
        item.meal_options << @meal_option
        item.save
      end
      assert_difference -> { @cart.alacartes.count }, 1 do
        @cart.items.create(variance: @ac_variance, best_with: @vm_variance.best_with, best_with_quantity: 1)
      end
    end
  end

  test "valid multiple existence of uniq attributes" do
    @cart.items.create(variance: @ac_variance)
    assert @cart.items.build(variance: @ac_variance, best_with: @ac_variance.best_with, best_with_quantity: 1).valid?
    assert @cart.items.build(variance: @ac_variance, best_with: @ac_variance.best_with, best_with_quantity: 2).valid?

    @cart.items.build(variance: @vm_variance).tap { |i| i.meal_options << @meal_option }.save
    assert @cart.items.build(variance: @vm_variance, best_with: @vm_variance.best_with, best_with_quantity: 1).tap { |i| i.meal_options << @meal_option }.valid?
    assert @cart.items.build(variance: @vm_variance, best_with: @vm_variance.best_with, best_with_quantity: 3).tap { |i| i.meal_options << @meal_option }.valid?
  end

  test "invalid multiple existence of variance per cart" do
    @cart.items.create(variance: @ac_variance)
    refute @cart.items.build(variance: @ac_variance).valid?

    item = @cart.items.build(variance: @vm_variance)
    item.meal_options << @meal_option
    item.save

    item2 = @cart.items.build(variance: @vm_variance)
    item2.meal_options << @meal_option
    refute item2.valid?
  end

  test "valid with value meal with meal option" do
    item = @cart.items.build(variance: @vm_variance)
    item.meal_options << @meal_option
    assert item.valid?
  end

  test "invalid item value meal without meal option" do
    refute @cart.items.build(variance: @vm_variance).valid?
  end

  test "invalid item value meal with invalid meal option" do
    item = @cart.items.build(variance: @vm_variance)
    item.meal_options << create(:meal_option)
    refute item.valid?
  end

  test "valid alacarte" do
    assert @cart.items.build(variance: @ac_variance).valid?
  end

  test "invalid alacarte with meal option" do
    item = @cart.items.build(variance: @ac_variance)
    item.meal_options << @meal_option
    refute item.valid?
  end

  test "alacarte with invalid meal option" do
    item = @cart.items.build(variance: @ac_bundle_variance)
    item.meal_options << @meal_option
    refute item.valid?
  end

  test "invalid alacarte without meal option" do
    item = @cart.items.build(variance: @ac_bundle_variance)
    refute item.valid?
  end

  test "alacarte with valid meal option" do
    item = @cart.items.build(variance: @ac_bundle_variance)
    item.meal_options << @bundle_meal_option
    assert item.valid?
  end

  test "invalid item best_with_quantity without best_with " do
    refute @cart.items.build(variance: @ac_variance, best_with: false, best_with_quantity: 1).valid?
  end

  test "invalid item best_with without quantity" do
    refute @cart.items.build(variance: @ac_variance, best_with: true).valid?
    item = @cart.items.build(variance: @vm_variance, best_with: true)
    item.meal_options << @meal_option
    refute item.valid?
  end

  test "valid item best_with with quantity" do
    assert @cart.items.build(variance: @ac_variance, best_with: true, best_with_quantity: 1).valid?
    assert @cart.items.build(variance: @vm_variance, best_with: true, best_with_quantity: 1).tap { |i| i.meal_options << @meal_option }.valid?
  end
end
