require "test_helper"

class CategoryTest < ActiveSupport::TestCase
  setup do
    @category = build(:category)
    @invalid_category = build(:invalid_category)
  end

  test "valid category" do
    assert @category.valid?
  end

  test "invalid category" do
    refute @invalid_category.valid?
  end
end
