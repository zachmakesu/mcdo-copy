require "test_helper"

class VarianceTest < ActiveSupport::TestCase
  setup do
    @value_meal = create(:value_meal)
    @variance = build(:variance, variable_id: @value_meal.id, variable_type: "ValueMeal")
    @variances = build_list(:variance, 2)
    @ac_bundle_variance = build(:variance, variable: create(:alacarte_bundle))
  end

  test "valid variance" do
    assert @variance.valid?
    assert @ac_bundle_variance.valid?
  end

  test "search valid variance" do
    @variance.save
    searched_product = Variance.search(keyword: @variance.name)
    assert_includes searched_product, @variance
  end

  test "search invalid product" do
    @variance.save
    searched_product = Variance.search(keyword: Faker::Name.name)
    assert_empty searched_product
  end

  test ".new_today" do
    # Create New Variances Dated on Current Day
    @variances.map do |v|
      v[:variable_id] = @value_meal.id
      v[:variable_type] = ValueMeal
    end
    @variances.map(&:save)
    # Create New Variance Dated Yesterday
    prev_day = Time.zone.yesterday
    @variance.created_at = prev_day
    @variance.save
    query = Variance.new_today
    assert_equal query.count, @variances.count
  end

  test "#bundle?" do
    assert @ac_bundle_variance.bundle?
  end

  test "false #bundle?" do
    refute @variance.bundle?
  end
end
