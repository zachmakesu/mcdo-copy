require "test_helper"

class ValueMealTest < ActiveSupport::TestCase
  setup do
    @category = create(:mcsaver_category)
    @value_meal = build(:value_meal, category_id: @category.id)
    @invalid_value_meal = build(:invalid_value_meal)
  end

  test "valid value_meal" do
    assert @value_meal.valid?
  end

  test "invalid value_meal" do
    refute @invalid_value_meal.valid?
  end

  test "search valid product" do
    @value_meal.save
    searched_product = ValueMeal.search(keyword: @value_meal.name, category: "mcsaver")
    assert_includes searched_product, @value_meal, "should include searched product"
  end

  test "search invalid product" do
    @value_meal.save
    searched_product = ValueMeal.search(keyword: Faker::Name.name, category: Faker::Lorem.word)
    assert_empty searched_product
  end

  test "#activate!" do
    @value_meal.save
    @value_meal.activate!
    assert @value_meal.active_at.present?
  end

  test "#deactivate!" do
    @value_meal.save
    @value_meal.deactivate!
    assert_nil @value_meal.active_at
  end

  test "#bundle?" do
    refute @value_meal.bundle?, "should return false since vm is not a bundle"
  end
end
