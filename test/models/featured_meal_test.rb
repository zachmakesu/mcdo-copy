require "test_helper"

class FeaturedMealTest < ActiveSupport::TestCase
  setup do
    @category = create(:mcsaver_category)
    @value_meal = create(:value_meal, category_id: @category.id)
    @alacarte = create(:alacarte)
  end

  test ".filter_by" do
    @value_meal.create_featured_meal
    @alacarte.create_featured_meal
    collection = FeaturedMeal.filter_by("ValueMeal").map(&:featureable)
    assert_includes(collection, @value_meal)
    refute_includes(collection, @alacarte)
  end
end
