require "test_helper"

class PromoTest < ActiveSupport::TestCase
  setup do
    @promo = build(:promo)
    @invalid_promo = build(:invalid_promo)
    @inactive_unpublished_promo = create(
      :promo,
      is_active: false,
      is_published: false,
      name: Faker::Lorem.word
    )
  end

  test "valid promo" do
    assert @promo.valid?
  end

  test "invalid promo" do
    refute @invalid_promo.valid?
  end

  test ".active_scope" do
    @promo.is_active = true
    @promo.save
    collection = Promo.active_scope(active: true)
    assert_includes(collection, @promo)
    refute_includes(collection, @inactive_promo)
  end

  test ".active_scope nil argument" do
    @promo.is_active = true
    @promo.save
    query = Promo.all
    collection = Promo.active_scope(active: nil)
    assert_equal(collection, query)
  end

  test ".published_scope" do
    @promo.is_published = true
    @promo.save
    collection = Promo.published_scope(published: true)
    assert_includes(collection, @promo)
    refute_includes(collection, @inactive_unpublished_promo)
  end

  test ".published_scope nil argument" do
    @promo.is_published = true
    @promo.save
    query = Promo.all
    collection = Promo.published_scope(published: nil)
    assert_equal(collection, query)
  end
end
