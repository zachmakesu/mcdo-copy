require "test_helper"

class NewsTest < ActiveSupport::TestCase
  setup do
    @news = build(:news)
    @invalid_news = build(:invalid_news)
  end

  test "valid news" do
    assert @news.valid?
  end

  test "invalid news" do
    refute @invalid_news.valid?
  end
end
