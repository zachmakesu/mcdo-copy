require "test_helper"

class MealOptionTest < ActiveSupport::TestCase
  setup do
    @meal_option = build(:meal_option)
    create_list(:meal_option, 7)
  end

  test "convenience methods for checking size" do
    @meal_option.size = "Small"
    assert @meal_option.small?
    refute @meal_option.large?
    @meal_option.size = "Medium"
    assert @meal_option.medium?
  end

  test "convenience methods for checking classification" do
    @meal_option.classification = "Drink"
    assert @meal_option.drink?
  end

  test "search valid product" do
    @meal_option.save
    searched_product = MealOption.search(keyword: @meal_option.name, type: @meal_option.classification)
    assert_includes searched_product, @meal_option, "should include searched product"
  end

  test "search invalid product" do
    @meal_option.save
    searched_product = MealOption.search(keyword: Faker::Name.name, type: Faker::Name.name)
    assert_empty searched_product
  end

  test ".fetch_by drinks" do
    drinks = MealOption.limit(3)
    small_drinks_count = MealOption.drink.small.count
    assert_equal MealOption.fetch_by(type: "drink", size: "small").count, small_drinks_count
    drinks.limit(2).each { |drink| drink.update(size: "medium") }
    assert_equal MealOption.fetch_by(type: "drink", size: "medium").count, 2
    drinks.limit(2).each { |drink| drink.update(size: "large") }
    assert_equal MealOption.fetch_by(type: "drink", size: "large").count, 2
  end

  test ".fetch_by fries" do
    MealOption.all.each do |meal|
      meal.update(classification: "fries", size: "small")
    end
    fries = MealOption.limit(3)
    small_fries_count = MealOption.fries.small.count
    assert_equal MealOption.fetch_by(type: "fries", size: "small").count, small_fries_count
    fries.limit(2).each { |f| f.update(size: "medium") }
    assert_equal MealOption.fetch_by(type: "fries", size: "medium").count, 2
    fries.limit(2).each { |f| f.update(size: "large") }
    assert_equal MealOption.fetch_by(type: "fries", size: "large").count, 2
  end

  test ".fetch_by special fries" do
    MealOption.all.each do |meal|
      meal.update(classification: "special_fries", size: "small")
    end
    special_fries = MealOption.limit(3)
    small_special_fries_count = MealOption.special_fries.small.count
    assert_equal MealOption.fetch_by(type: "special_fries", size: "small").count, small_special_fries_count
    special_fries.limit(2).each { |fries| fries.update(size: "medium") }
    assert_equal MealOption.fetch_by(type: "special_fries", size: "medium").count, 2
    special_fries.limit(2).each { |fries| fries.update(size: "large") }
    assert_equal MealOption.fetch_by(type: "special_fries", size: "large").count, 2
  end

  test ".fetch_by sides" do
    MealOption.all.each { |meal| meal.update(classification: "sides") }
    create(:meal_option, classification: "fries")
    sides_count = MealOption.sides.count
    assert_equal MealOption.fetch_by(type: "sides").count, sides_count
  end

  test ".fetch_by sauce" do
    MealOption.all.each { |meal| meal.update(classification: "sauce") }
    create(:meal_option, classification: "fries")
    sauce_count = MealOption.sauce.count
    assert_equal MealOption.fetch_by(type: "sauce").count, sauce_count
  end
end
