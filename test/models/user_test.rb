require "test_helper"

class UserTest < ActiveSupport::TestCase
  setup do
    @user = build(:user)
    prev_month = Time.zone.today - 1.month
    @employee = create(:employee, created_at: prev_month)
    @new_users = build_list(:user, 2)
    @users = @new_users.map do |x|
      x.email = Faker::Internet.unique.email
      x.save
    end
  end
  test "user factory is valid" do
    assert @user.valid?
  end
  test ".new_today" do
    query = User.new_today
    assert_equal query.count, @users.count
  end
  test ".new_this_month" do
    query = User.new_this_month
    assert_equal query.count, @users.count, "only fetch user created on current month"
  end
end
