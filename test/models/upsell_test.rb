require "test_helper"

class UpsellTest < ActiveSupport::TestCase
  setup do
    @alacarte = create(:alacarte)
    @upsell = Upsell.new(alacarte_id: @alacarte.id)
    @upsell.save
  end

  test "valid upsell" do
    assert @upsell.valid?
  end

  test "search valid product" do
    searched_product = Upsell.search(keyword: @alacarte.name)
    assert_includes searched_product.map(&:product), @alacarte
  end

  test "search invalid product" do
    searched_product = Upsell.search(keyword: Faker::Name.name)
    assert_empty searched_product
  end
end
