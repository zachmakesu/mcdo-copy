require "test_helper"

class BranchTest < ActiveSupport::TestCase
  setup do
    @branch = build(:branch)
    @invalid_branch = build(:invalid_branch)
  end

  test "valid branch" do
    assert @branch.valid?
  end

  test "invalid branch" do
    refute @invalid_branch.valid?
  end

  test "search valid product" do
    @branch.save
    searched_product = Branch.search(keyword: @branch.name)
    assert_includes searched_product, @branch, "should include searched product"
  end
  test "search invalid product" do
    @branch.save
    searched_product = Branch.search(keyword: Faker::Name.name)
    assert_empty searched_product
  end
end
