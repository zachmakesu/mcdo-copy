require "test_helper"

class CouponTest < ActiveSupport::TestCase
  setup do
    @coupon = build(:coupon)
    @invalid_coupon = build(:invalid_coupon)
  end

  test "valid coupon" do
    assert @coupon.valid?
  end

  test "invalid coupon" do
    refute @invalid_coupon.valid?
  end

  test "search valid coupon" do
    @coupon.save
    searched_coupon = Coupon.search(keyword: @coupon.name)
    assert_includes searched_coupon, @coupon, "should include searched coupon"
  end

  test "search invalid coupon" do
    @coupon.save
    searched_coupon = Coupon.search(keyword: Faker::Name.name)
    assert_empty searched_coupon
  end
end
