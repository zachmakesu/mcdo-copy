# This has to come first
require_relative "./support/rails"

# Load everything else from test/support
Dir[File.expand_path("../support/**/*.rb", __FILE__)].each { |rb| require(rb) }

# Minitest::Reporters adds color and progress bar to the test runner
require "minitest/reporters"
Minitest::Reporters.use!(
  [
    Minitest::Reporters::SpecReporter.new,
    Minitest::Reporters::JUnitReporter.new
  ],
  ENV,
  Minitest.backtrace_filter
)

include FactoryBot::Syntax::Methods

# https://github.com/plataformatec/devise/issues/3913#issuecomment-174737364
class ActionDispatch::IntegrationTest
  def sign_in(user)
    post user_session_path \
      "user[email]"    => user.email,
      "user[password]" => user.password
  end
end
