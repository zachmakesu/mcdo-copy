require "application_system_test_case"

class LayoutHelperTest < ApplicationSystemTestCase
  test "rendered page contains devise layout requiring authentication" do
    visit("/")
    assert_selector("html>head+body")
    assert_selector("body p")
    assert_match(/Log in/, page.title)
  end
end
