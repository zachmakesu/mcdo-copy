# == Schema Information
#
# Table name: product_bundles
#
#  alacarte_id :integer
#  created_at  :datetime         not null
#  id          :integer          not null, primary key
#  max_drinks  :integer          default(0), not null
#  max_fries   :integer          default(0), not null
#  max_rice    :integer          default(0), not null
#  updated_at  :datetime         not null
#

FactoryBot.define do
  factory :product_bundle do
    max_drinks  3
    max_fries   3
    max_rice    3
  end
end
