# == Schema Information
#
# Table name: customers
#
#  cds_id     :integer
#  created_at :datetime         not null
#  id         :integer          not null, primary key
#  updated_at :datetime         not null
#

FactoryBot.define do
  factory :customer do
    cds_id 1
  end

  factory :invalid_customer, parent: :customer do
    cds_id nil
  end
end
