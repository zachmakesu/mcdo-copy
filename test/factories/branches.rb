# == Schema Information
#
# Table name: branches
#
#  address      :text
#  area_id      :integer
#  created_at   :datetime         not null
#  description  :text             default("")
#  id           :integer          not null, primary key
#  image_source :string
#  lat          :float
#  lng          :float
#  name         :string
#  offerings    :string
#  status       :string
#  updated_at   :datetime         not null
#
# Indexes
#
#  index_branches_on_area_id  (area_id)
#
# Foreign Keys
#
#  fk_rails_...  (area_id => areas.id)
#

FactoryBot.define do
  factory :branch do
    area
    name "Branch 51"
    description "Branch 51"
    status "1"
  end

  factory :invalid_branch, parent: :branch do
    name nil
  end
end
