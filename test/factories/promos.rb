# == Schema Information
#
# Table name: promos
#
#  category          :string
#  content           :text
#  created_at        :datetime         not null
#  id                :integer          not null, primary key
#  image_source      :string
#  is_active         :boolean          default(FALSE), not null
#  is_published      :boolean          default(FALSE), not null
#  link              :string
#  name              :string
#  tags              :string
#  updated_at        :datetime         not null
#  validity_end_at   :datetime
#  validity_start_at :datetime
#

FactoryBot.define do
  factory :promo do
    category "regular"
    content Faker::Lorem.sentence
    is_active true
    is_published true
    link Faker::Internet.url
    name Faker::Name.name
    tags Faker::Name.name
    validity_start_at Faker::Date.forward(2)
    validity_end_at Faker::Date.forward(3)
  end

  factory :invalid_promo, parent: :promo do
    name nil
  end
end
