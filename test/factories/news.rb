# == Schema Information
#
# Table name: news
#
#  category     :integer
#  created_at   :datetime         not null
#  description  :text
#  displayed_at :datetime
#  id           :integer          not null, primary key
#  image_source :string
#  start_date   :datetime
#  title        :string
#  updated_at   :datetime         not null
#

FactoryBot.define do
  factory :news do
    title "New menu"
    description "Spicy Burger"
    image_source { File.open(Rails.root.join("test", "assets", "logo.png")) }
    start_date Time.zone.today
    category ::News.categories[:newsfeed]
  end

  factory :invalid_news, parent: :news do
    title nil
    description nil
    image_source nil
    category nil
    start_date nil
  end
end
