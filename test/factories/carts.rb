# == Schema Information
#
# Table name: carts
#
#  created_at  :datetime         not null
#  customer_id :integer
#  expires_at  :datetime
#  id          :integer          not null, primary key
#  updated_at  :datetime         not null
#
# Indexes
#
#  index_carts_on_customer_id  (customer_id)
#
# Foreign Keys
#
#  fk_rails_...  (customer_id => customers.id)
#

FactoryBot.define do
  factory :cart do
    customer nil
    expires_at "2018-02-27 15:30:10"
  end
end
