# == Schema Information
#
# Table name: home_contents
#
#  availability     :string
#  created_at       :datetime         not null
#  groupable_id     :integer
#  groupable_type   :string
#  id               :integer          not null, primary key
#  image_source_app :string
#  image_source_web :string
#  name             :string
#  sequence_id      :integer
#  updated_at       :datetime         not null
#
# Indexes
#
#  index_home_contents_on_groupable_type_and_groupable_id  (groupable_type,groupable_id)
#

FactoryBot.define do
  factory :home_content do
    availability "regular"
    name         Faker::Name.name
    sequence_id  Faker::Number.unique.number(2)
  end
end
