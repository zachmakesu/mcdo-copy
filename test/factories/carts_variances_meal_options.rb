# == Schema Information
#
# Table name: carts_variances_meal_options
#
#  carts_variance_id :integer
#  created_at        :datetime         not null
#  id                :integer          not null, primary key
#  meal_option_id    :integer
#  quantity          :integer          default(1)
#  updated_at        :datetime         not null
#
# Indexes
#
#  index_carts_variances_meal_options_on_carts_variance_id  (carts_variance_id)
#  index_carts_variances_meal_options_on_meal_option_id     (meal_option_id)
#
# Foreign Keys
#
#  fk_rails_...  (carts_variance_id => carts_variances.id)
#  fk_rails_...  (meal_option_id => meal_options.id)
#

FactoryBot.define do
  factory :carts_variances_meal_option do
    carts_variance nil
    meal_option nil
  end
end
