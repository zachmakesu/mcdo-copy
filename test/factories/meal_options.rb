# == Schema Information
#
# Table name: meal_options
#
#  classification :string
#  code_name      :string
#  created_at     :datetime         not null
#  displayed_at   :datetime
#  group_id       :integer
#  id             :integer          not null, primary key
#  image_source   :string
#  is_default     :boolean          default(FALSE), not null
#  long_name      :string
#  name           :string
#  poscode        :string
#  price          :float
#  size           :string
#  updated_at     :datetime         not null
#

FactoryBot.define do
  factory :meal_option do
    long_name "Breakfast Orange Juice"
    name "Orange Juice"
    code_name "BFOJ12"
    poscode "7258"
    price 0
    size "small"
    classification "drink"
    group_id 1
    image_source "https://placehold.it/50x50"
    is_default Faker::Boolean.boolean
  end
end
