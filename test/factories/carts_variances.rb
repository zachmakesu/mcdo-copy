# == Schema Information
#
# Table name: carts_variances
#
#  best_with          :boolean          default(FALSE)
#  best_with_quantity :integer          default(0)
#  cart_id            :integer
#  created_at         :datetime         not null
#  id                 :integer          not null, primary key
#  quantity           :integer          default(1)
#  updated_at         :datetime         not null
#  variance_id        :integer
#
# Indexes
#
#  index_carts_variances_on_cart_id      (cart_id)
#  index_carts_variances_on_variance_id  (variance_id)
#
# Foreign Keys
#
#  fk_rails_...  (cart_id => carts.id)
#  fk_rails_...  (variance_id => variances.id)
#

FactoryBot.define do
  factory :carts_variance do
    cart nil
    variance nil
  end
end
