# == Schema Information
#
# Table name: orders
#
#  cart_id                 :integer
#  created_at              :datetime         not null
#  delivery_address_id     :integer
#  delivery_fee_history_id :integer
#  id                      :integer          not null, primary key
#  order_id                :string
#  payment_type            :string
#  platform                :string
#  reference_number        :string
#  updated_at              :datetime         not null
#
# Indexes
#
#  index_orders_on_cart_id                  (cart_id)
#  index_orders_on_delivery_fee_history_id  (delivery_fee_history_id)
#
# Foreign Keys
#
#  fk_rails_...  (cart_id => carts.id)
#  fk_rails_...  (delivery_fee_history_id => delivery_fee_histories.id)
#

FactoryBot.define do
  factory :order do
    status "MyString"
  end
end
