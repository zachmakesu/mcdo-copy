# == Schema Information
#
# Table name: sections
#
#  created_at :datetime         not null
#  id         :integer          not null, primary key
#  title      :string
#  updated_at :datetime         not null
#

FactoryBot.define do
  factory :section do
    sequence(:title) { Section::CATEGORIES.sample.titleize }
  end
end
