FactoryBot.define do
  factory :user do
    first_name  "Golden"
    middle_name "Acres"
    last_name   "Development Corporation"
    email       "gadc@example.com"
    password    "PasswordPlease"
  end
  factory :admin, class: "User" do
    first_name  "Sudo"
    middle_name "Root"
    last_name   "Admin"
    email       "admin@mcdo.dev"
    password    "GreedIsGood"
  end
  factory :employee, class: "User" do
    first_name  Faker::Name.first_name
    middle_name Faker::Name.name
    last_name   Faker::Name.last_name
    email       Faker::Internet.email
    password    "GreedIsGood"
  end
end
