# == Schema Information
#
# Table name: categories
#
#  category_id :integer
#  created_at  :datetime         not null
#  id          :integer          not null, primary key
#  is_active   :boolean          default(FALSE), not null
#  name        :string
#  sequence_id :integer
#  updated_at  :datetime         not null
#

FactoryBot.define do
  factory :category do
    name "Breakfast"
    category_id 4
    sequence_id 1
  end
  factory :mcsaver_category, parent: :category do
    name "McSaver Meals"
  end
  factory :invalid_category, parent: :category do
    name nil
  end
end
