# == Schema Information
#
# Table name: notifications
#
#  category          :string
#  created_at        :datetime         not null
#  description       :text
#  id                :integer          not null, primary key
#  image_source      :string
#  short_description :string
#  title             :string
#  updated_at        :datetime         not null
#

FactoryBot.define do
  factory :notification do
    title             Faker::Name.name
    short_description Faker::Name.name
    description       Faker::Lorem.sentence
    category          "default"
  end
  factory :invalid_notification, parent: :notification do
    title             nil
  end
end
