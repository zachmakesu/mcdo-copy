# == Schema Information
#
# Table name: variances
#
#  best_with     :integer
#  code_name     :string
#  created_at    :datetime         not null
#  group_id      :integer
#  id            :integer          not null, primary key
#  image_source  :string
#  menu_name     :string
#  name          :string
#  poscode       :string
#  price         :float
#  updated_at    :datetime         not null
#  variable_id   :integer
#  variable_type :string
#

FactoryBot.define do
  factory :variance do
    menu_name     Faker::Commerce.product_name
    code_name     Faker::Commerce.product_name
    name          Faker::Commerce.product_name
    poscode       Faker::Commerce.promotion_code
    price         Faker::Commerce.price
    best_with     Faker::Number.number(4)
  end
end
