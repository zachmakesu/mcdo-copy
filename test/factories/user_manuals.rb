# == Schema Information
#
# Table name: user_manuals
#
#  created_at   :datetime         not null
#  description  :string
#  id           :integer          not null, primary key
#  image_source :string
#  sequence_id  :integer
#  updated_at   :datetime         not null
#

FactoryBot.define do
  factory :user_manual do
    description Faker::Lorem.sentence
  end
end
