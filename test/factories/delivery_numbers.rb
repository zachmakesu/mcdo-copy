# == Schema Information
#
# Table name: delivery_numbers
#
#  branch_id  :integer
#  created_at :datetime         not null
#  id         :integer          not null, primary key
#  number     :string
#  updated_at :datetime         not null
#
# Indexes
#
#  index_delivery_numbers_on_branch_id  (branch_id)
#
# Foreign Keys
#
#  fk_rails_...  (branch_id => branches.id)
#

FactoryBot.define do
  factory :delivery_number do
    branch
    number "09191234567"
  end
end
