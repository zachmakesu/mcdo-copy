# == Schema Information
#
# Table name: delivery_fees
#
#  created_at :datetime         not null
#  enabled    :boolean          default(TRUE)
#  id         :integer          not null, primary key
#  updated_at :datetime         not null
#

FactoryBot.define do
  factory :delivery_fee do
    enabled false
  end
end
