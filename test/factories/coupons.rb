# == Schema Information
#
# Table name: coupons
#
#  classification      :string
#  created_at          :datetime         not null
#  description         :text
#  detail_image_source :string
#  discount            :float
#  id                  :integer          not null, primary key
#  image_source        :string
#  name                :string
#  poscode             :string
#  updated_at          :datetime         not null
#  usage_count         :integer
#  usage_type          :string
#  validity_end_at     :datetime
#  validity_start_at   :datetime
#

FactoryBot.define do
  factory :coupon do
    classification    "both"
    description       Faker::Lorem.sentence
    name              Faker::Name.name
    usage_count       Faker::Number.number(2)
    usage_type        "single"
    validity_start_at Faker::Date.forward(2)
    validity_end_at   Faker::Date.forward(3)
  end
  factory :invalid_coupon, parent: :coupon do
    name nil
  end
end
