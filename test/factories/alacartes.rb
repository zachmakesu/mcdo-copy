# == Schema Information
#
# Table name: alacartes
#
#  best_with        :integer
#  category_id      :integer
#  created_at       :datetime         not null
#  description      :text
#  discounted_rice  :boolean
#  displayed_at     :datetime
#  group_id         :integer
#  id               :integer          not null, primary key
#  image_source     :string
#  legacy_sections  :string
#  name             :string
#  product_order_id :integer
#  schedule_id      :integer
#  sequence_id      :integer
#  updated_at       :datetime         not null
#

FactoryBot.define do
  factory :alacarte do
    name "MyString"
    group_id 1
    legacy_sections "MyString"
    image_source "MyString"
    best_with 1
    description "MyText"
    schedule
    product_order_id 1
    discounted_rice false
    sequence_id 123
  end

  factory :invalid_alacarte, parent: :alacarte do
    name nil
    schedule_id nil
  end

  factory :alacarte_bundle, parent: :alacarte do
    after(:create) do |ac|
      create(:product_bundle, alacarte_id: ac.id)
    end
  end
end
