# == Schema Information
#
# Table name: value_meals
#
#  active_at       :datetime
#  alacarte_id     :integer
#  availability    :string
#  category_id     :integer
#  created_at      :datetime         not null
#  discounted_rice :boolean
#  displayed_at    :datetime
#  id              :integer          not null, primary key
#  image_source    :string
#  is_breakfast    :boolean
#  legacy_sections :string
#  name            :string
#  order_id        :integer
#  schedule_id     :integer
#  sequence_id     :integer
#  updated_at      :datetime         not null
#

FactoryBot.define do
  factory :value_meal do
    alacarte
    name "MyString"
    legacy_sections "MyString"
    schedule
    order_id 1
    discounted_rice false
    is_breakfast false
    sequence_id 123
  end

  factory :invalid_value_meal, parent: :value_meal do
    name nil
    schedule_id nil
  end
end
