# == Schema Information
#
# Table name: schedules
#
#  created_at :datetime         not null
#  end_at     :time
#  id         :integer          not null, primary key
#  start_at   :time
#  title      :string
#  updated_at :datetime         not null
#

FactoryBot.define do
  factory :schedule do
    title "4:00 - 10:30"
    start_at Time.zone.parse("4:00").strftime("%H:%M:%S")
    end_at Time.zone.parse("10:30").strftime("%H:%M:%S")
  end
end
