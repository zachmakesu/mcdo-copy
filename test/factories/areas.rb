# == Schema Information
#
# Table name: areas
#
#  created_at  :datetime         not null
#  description :text             default("")
#  id          :integer          not null, primary key
#  name        :string
#  updated_at  :datetime         not null
#

FactoryBot.define do
  factory :area do
    name "Area 51"
    description "Area 51"
  end
end
