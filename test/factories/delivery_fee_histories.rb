# == Schema Information
#
# Table name: delivery_fee_histories
#
#  created_at      :datetime         not null
#  delivery_fee_id :integer
#  id              :integer          not null, primary key
#  label           :integer          default("custom")
#  updated_at      :datetime         not null
#  value           :float            default(0.0)
#
# Indexes
#
#  index_delivery_fee_histories_on_delivery_fee_id  (delivery_fee_id)
#
# Foreign Keys
#
#  fk_rails_...  (delivery_fee_id => delivery_fees.id)
#

FactoryBot.define do
  factory :delivery_fee_history do
    delivery_fee_history nil
    fee 1.5
  end
end
