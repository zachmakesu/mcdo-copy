require "test_helper"

class Admin::NewsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @admin = create(:admin)
    @news = create(:news)
    @valid_params = {
      news: {
        title: "New Menu",
        description: "Spicy Burger",
        image_source: fixture_file_upload(Rails.root.join("test", "assets", "logo.png")),
        start_date: Time.zone.today,
        category: "promo"
      }
    }
    @invalid_params = {
      news: {
        category: "promo"
      }
    }
    sign_in @admin
  end

  test "index" do
    get admin_news_index_path
    assert_response :success
    assert_equal News.all, assigns(:news)
  end

  test "show" do
    get admin_news_path(@news.id)
    assert_response :success
  end

  test "new" do
    get new_admin_news_path
    assert_response :success
  end

  test "create" do
    post admin_news_index_path, params: @valid_params
    assert_redirected_to admin_news_path(News.last)
    assert_equal News.last, assigns(:news)
    assert_equal "Success", flash[:notice]
  end

  test "Failing create" do
    post admin_news_index_path, params: @invalid_params
    assert_template :new
    assert_not_equal News.last, assigns(:news)
  end

  test "edit" do
    get edit_admin_news_path(@news.id)
    assert_response :success
  end

  test "update" do
    patch admin_news_path(@news.id), params: @valid_params
    assert_redirected_to admin_news_path(@news)
    assert_equal "Success", flash[:notice]
  end

  test "toggle_display" do
    put admin_news_path(@news.id), params: { display: "true" }, xhr: true
    body = JSON.parse(@response.body)
    assert_equal body["message"], "Success."
  end

  test "destroy" do
    delete admin_news_path(@news.id)
    assert_redirected_to admin_news_index_path
    assert_equal "Successfully deleted", flash[:notice]
  end
end
