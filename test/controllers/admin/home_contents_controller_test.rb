require "test_helper"

class Admin::HomeContentsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @category = create(:category)
    @content = create(:home_content, groupable_type: "Category", groupable_id: @category.id)
    @admin = create(:admin)
    @valid_params = {
      home_content: {
        name: Faker::Name.name,
        groupable_type: "Category",
        groupable_id: @category.id
      }
    }
    sign_in @admin
  end

  test "index" do
    get admin_home_contents_path
    assert_response :success
  end

  test "new" do
    get new_admin_home_content_path
    assert_response :success
  end

  test "create" do
    @valid_params[:sequence] = @content.sequence_id # set to existing position
    post admin_home_contents_path, params: @valid_params
    assert_redirected_to admin_home_contents_path
    assert_equal HomeContent.last, assigns(:content)
    assert_operator @content.reload.sequence_id, :>, HomeContent.last.sequence_id, "ensure position is reordered"
    assert_equal "Success", flash[:notice]
  end

  test "edit" do
    get edit_admin_home_content_path(@content.id)
    assert_response :success
  end

  test "update" do
    patch admin_home_content_path(@content.id), params: @valid_params
    assert_redirected_to admin_home_contents_path
    assert_equal HomeContent.last.sequence_id, @content.sequence_id, "ensure position is not reordered"
    assert_equal "Success", flash[:notice]
  end

  test "destroy" do
    delete admin_home_content_path(@content.id)
    assert_redirected_to admin_home_contents_path
    assert_equal "Success", flash[:notice]
  end
end
