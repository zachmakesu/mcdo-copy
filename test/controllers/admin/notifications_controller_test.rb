require "test_helper"

class Admin::NotificationsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @notification = create(:notification)
    @admin = create(:admin)
    @valid_params = {
      notification: {
        title: Faker::Name.name,
        short_description: Faker::Name.name,
        image_source: fixture_file_upload(Rails.root.join("test", "assets", "logo.png")),
        description: Faker::Lorem.sentence,
        category: "default"
      }
    }
    sign_in @admin
  end

  test "index" do
    get admin_notifications_path
    assert_response :success
  end

  test "new" do
    get new_admin_notification_path
    assert_response :success
  end

  test "create" do
    post admin_notifications_path, params: @valid_params
    assert_redirected_to admin_notifications_path
    assert_equal Notification.last, assigns(:notification)
    assert_equal "Success", flash[:notice]
  end

  test "edit" do
    get edit_admin_notification_path(@notification.id)
    assert_response :success
  end

  test "update" do
    patch admin_notification_path(@notification.id), params: @valid_params
    assert_redirected_to admin_notifications_path
    assert_equal "Success", flash[:notice]
  end

  test "destroy" do
    delete admin_notification_path(@notification.id)
    assert_redirected_to admin_notifications_path
    assert_equal "Success", flash[:notice]
  end
end
