require "test_helper"

class Admin::PromosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @promo = create(:promo)
    @admin = create(:admin)
    @valid_params = {
      promo: {
        category: "regular",
        content: Faker::Lorem.sentence,
        is_active: true,
        is_published: true,
        link: Faker::Internet.url,
        name: Faker::Name.name,
        tags: Faker::Name.name,
        validity_start_at: Faker::Date.forward(2),
        validity_end_at: Faker::Date.forward(3)
      }
    }
    sign_in @admin
  end

  test "index" do
    get admin_promos_path
    assert_response :success
  end

  test "new" do
    get new_admin_promo_path
    assert_response :success
  end

  test "create" do
    post admin_promos_path, params: @valid_params
    assert_redirected_to admin_promos_path
    assert_equal Promo.last, assigns(:promo)
    assert_equal "Success", flash[:notice]
  end

  test "edit" do
    get edit_admin_promo_path(@promo.id)
    assert_response :success
  end

  test "update" do
    patch admin_promo_path(@promo.id), params: @valid_params
    assert_redirected_to admin_promos_path
    assert_equal "Success", flash[:notice]
  end

  test "destroy" do
    delete admin_promo_path(@promo.id)
    assert_redirected_to admin_promos_path
    assert_equal "Success", flash[:notice]
  end
end
