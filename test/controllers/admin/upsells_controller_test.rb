require "test_helper"

class Admin::UpsellsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @admin = create(:admin)
    @alacarte = create(:alacarte)
    @upsell_params = {
      upsell: { alacarte_id: @alacarte.id }
    }
    @upsell = Upsell.create(@upsell_params[:upsell])
    sign_in @admin
  end
  test "index" do
    get admin_upsells_path
    assert_response :success
  end

  test "new" do
    get new_admin_upsell_path
    assert_response :success
  end

  test "create" do
    post admin_upsells_path, params: @upsell_params
    assert_equal Upsell.last, assigns(:upsell)
    assert_equal "Successfully Created", flash[:notice]
  end

  test "edit" do
    get edit_admin_upsell_path(@upsell.id)
    assert_response :success
  end

  test "update" do
    patch admin_upsell_path(@upsell.id), params: @upsell_params
    assert_redirected_to admin_upsells_path(@upsell)
    assert_equal "Successfully Updated", flash[:notice]
  end

  test "destroy" do
    delete admin_upsell_path(@upsell.id)
    assert_redirected_to admin_upsells_path
    assert_equal "Successfully deleted", flash[:notice]
  end
end
