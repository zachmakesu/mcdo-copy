require "test_helper"

class Admin::ValueMealsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @admin = create(:admin)
    @value_meal = create(:value_meal)
    @category = create(:mcsaver_category)
    @section = create(:section)
    @schedule = create(:schedule)
    @meal_option = create(:meal_option)
    @valid_params = {
      value_meal: {
        name: "ValueMeal 51",
        category_id: @category.id,
        image_source: fixture_file_upload(Rails.root.join("test", "assets", "logo.png")),
        schedule_id: @schedule.id,
        variances_attributes: [
          {
            menu_name: "ValueMeal 51",
            name: "ValueMeal 51",
            code_name: "VM51",
            poscode: "VM51",
            price: 123.00
          }
        ],
        options_attributes: [
          {
            choice_id: @meal_option.id,
            section_id: @section.id
          }
        ]
      }
    }
    @invalid_params = {
      value_meal: {
        name: nil
      }
    }
    Section::CATEGORIES.each do |s|
      Section.find_or_initialize_by(title: s.titleize).save
    end
    sign_in @admin
  end

  test "index" do
    get admin_value_meals_path
    assert_response :success
  end

  test "show" do
    get admin_value_meal_path(@value_meal.id)
    assert_response :success
  end

  test "new" do
    get new_admin_value_meal_path
    assert_response :success
  end

  test "create" do
    post admin_value_meals_path, params: @valid_params
    assert_redirected_to admin_value_meals_path
    assert_equal ValueMeal.last, assigns(:value_meal)
    assert_equal "Success", flash[:notice]
  end

  test "Failing create" do
    post admin_value_meals_path, params: @invalid_params
    assert_template :new
    assert_not_equal ValueMeal.last, assigns(:value_meal)
  end

  test "edit" do
    get edit_admin_value_meal_path(@value_meal.id)
    assert_response :success
  end

  test "update" do
    patch admin_value_meal_path(@value_meal.id), params: @valid_params
    assert_redirected_to admin_value_meals_path
    assert_equal "Success", flash[:notice]
  end

  test "destroy" do
    delete admin_value_meal_path(@value_meal.id)
    assert_redirected_to admin_value_meals_path
    assert_equal "Successfully deleted", flash[:notice]
  end

  test "toggle_display" do
    put admin_value_meal_path(@value_meal.id), params: { display: "true" }, xhr: true
    body = JSON.parse(@response.body)
    assert_equal body["message"], "Success."
  end
end
