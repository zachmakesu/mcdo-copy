require "test_helper"

class Admin::OrdersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @admin = create(:admin)
    sign_in @admin
  end
  test "index" do
    get admin_orders_path
    assert_response :success
  end
end
