require "test_helper"

class Admin::VariancesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @admin = create(:admin)
    @meal_option = create(:meal_option)
    @variance = create(:variance, variable_id: @meal_option.id, variable_type: "MealOption")
    @valid_params = {
      variance: {
        name: Faker::Name.name,
        menu_name: Faker::Name.name,
        image_source: fixture_file_upload(Rails.root.join("test", "assets", "logo.png")),
        poscode: Faker::Commerce.promotion_code,
        variable_id: @meal_option.id,
        variable_type: "MealOption"
      }
    }
    sections = %w[
      Beverage
      Fries
      Burger
      Dessert
      Sauce
      Best\ With
      Special\ Fries
      Sides
    ]
    sections.each { |i| create(:section, title: i) }
    sign_in @admin
  end

  test "index" do
    get admin_variances_path
    assert_response :success
  end

  test "new" do
    get new_admin_variance_path
    assert_response :success
  end

  test "create" do
    post admin_variances_path, params: @valid_params
    assert_redirected_to admin_variances_path
    assert_equal Variance.last, assigns(:variance)
    assert_equal "Success", flash[:notice]
  end

  test "edit" do
    get edit_admin_variance_path(@variance.id)
    assert_response :success
  end

  test "update" do
    patch admin_variance_path(@variance.id), params: @valid_params
    assert_redirected_to admin_variances_path
    assert_equal "Success", flash[:notice]
  end

  test "destroy" do
    delete admin_variance_path(@variance.id)
    assert_redirected_to admin_variances_path
    assert_equal "Successfully deleted", flash[:notice]
  end
end
