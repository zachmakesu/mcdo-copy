require "test_helper"

class Admin::UserManualsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @admin = create(:admin)
    @user_manual = create(:user_manual)
    @valid_params = {
      user_manual: {
        description: Faker::Lorem.sentence,
        image_source: fixture_file_upload(
          Rails.root.join("test", "assets", "logo.png")
        )
      }
    }
    sign_in @admin
  end

  test "index" do
    get admin_user_manuals_path
    assert_response :success
  end

  test "new" do
    get new_admin_user_manual_path
    assert_response :success
  end

  test "create" do
    post admin_user_manuals_path, params: @valid_params
    assert_redirected_to admin_user_manuals_path
    assert_equal UserManual.last, assigns(:user_manual)
    assert_equal "Success!", flash[:notice]
  end

  test "edit" do
    get edit_admin_user_manual_path(@user_manual.id)
    assert_response :success
  end

  test "update" do
    patch admin_user_manual_path(@user_manual.id), params: @valid_params
    assert_redirected_to admin_user_manuals_path
    assert_equal "Success!", flash[:notice]
  end

  test "destroy" do
    delete admin_user_manual_path(@user_manual.id)
    assert_redirected_to admin_user_manuals_path
    assert_equal "Success!", flash[:notice]
  end
end
