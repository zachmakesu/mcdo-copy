require "test_helper"

class Admin::BranchesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @admin = create(:admin)
    @branch = create(:branch)
    @area = create(:area)
    @valid_params = {
      branch: {
        name: "Branch 51",
        description: "Branch 51",
        area_id: @area.id
      }
    }
    @invalid_params = {
      branch: {
        description: "Branch 51"
      }
    }
    sign_in @admin
  end

  test "index" do
    get admin_branches_path
    assert_response :success
  end

  test "new" do
    get new_admin_branch_path
    assert_response :success
  end

  test "create" do
    post admin_branches_path, params: @valid_params
    assert_redirected_to admin_branch_path(Branch.last)
    assert_equal Branch.last, assigns(:branch)
    assert_equal "Success", flash[:notice]
  end

  test "Failing create" do
    post admin_branches_path, params: @invalid_params
    assert_template :new
    assert_not_equal Branch.last, assigns(:branch)
  end

  test "edit" do
    get edit_admin_branch_path(@branch.id)
    assert_response :success
  end

  test "update" do
    patch admin_branch_path(@branch.id), params: @valid_params
    assert_redirected_to admin_branches_path
    assert_equal "Success", flash[:notice]
  end

  test "destroy" do
    delete admin_branch_path(@branch.id)
    assert_redirected_to admin_branches_path
    assert_equal "Successfully deleted", flash[:notice]
  end
end
