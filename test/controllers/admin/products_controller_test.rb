require "test_helper"

class Admin::ProductsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @admin = create(:admin)
    @value_meal = create(:value_meal)
    sign_in @admin
  end
  test "index" do
    get admin_products_path
    assert_response :success
  end

  test "#featured_meal" do
    post set_meal_as_featured_admin_product_path(@value_meal.id), params: { type: "ValueMeal" }
    json_response = JSON.parse(response.body)
    assert(json_response["success"])
    message = "#{@value_meal.name} Successfully set as featured!"
    assert_equal(json_response["message"], message)
  end

  test "When meal is already featured" do
    @value_meal.create_featured_meal
    post set_meal_as_featured_admin_product_path(@value_meal.id), params: { type: "ValueMeal" }
    json_response = JSON.parse(response.body)
    refute(json_response["success"])
    message = "#{@value_meal.name} is already a Featured Meal"
    assert_equal(json_response["message"], message)
  end

  test "#featured_meal with invalid type" do
    type = Faker::Lorem.word
    post set_meal_as_featured_admin_product_path(@value_meal.id), params: { type: type }
    json_response = JSON.parse(response.body)
    refute(json_response["success"])
    message = "#{type} is not a valid featured meal type!"
    assert_equal(json_response["message"], message)
  end

  test "#unfeatured_meal" do
    @value_meal.create_featured_meal
    delete set_meal_as_unfeatured_admin_product_path(@value_meal.id, type: "ValueMeal")
    json_response = JSON.parse(response.body)
    assert(json_response["success"])
    message = "#{@value_meal.name} Successfully set as unfeatured!"
    assert_equal(json_response["message"], message)
  end

  test "#unfeatured_meal with invalid type" do
    type = Faker::Lorem.word
    delete set_meal_as_unfeatured_admin_product_path(@value_meal.id, type: type)
    json_response = JSON.parse(response.body)
    refute(json_response["success"])
    message = "#{type} is not a valid featured meal type!"
    assert_equal(json_response["message"], message)
  end
end
