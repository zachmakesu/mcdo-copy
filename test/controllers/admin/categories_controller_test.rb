require "test_helper"

class Admin::CategoriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @category = create(:category)
    @admin = create(:admin)
    @valid_params = {
      category: {
        name: Faker::Name.name,
        sequence_id: Faker::Number.number(2),
        is_active: true
      }
    }
    sign_in @admin
  end

  test "index" do
    get admin_categories_path
    assert_response :success
  end

  test "new" do
    get new_admin_category_path
    assert_response :success
  end

  test "create" do
    post admin_categories_path, params: @valid_params
    assert_redirected_to admin_categories_path
    assert_equal Category.last, assigns(:category)
    assert_equal "Success", flash[:notice]
  end

  test "edit" do
    get edit_admin_category_path(@category.id)
    assert_response :success
  end

  test "update" do
    params = @valid_params
    params[:sequence_id] = Faker::Number.unique.number(3)
    patch admin_category_path(@category.id), params: params
    assert_redirected_to admin_categories_path
    assert_equal "Success", flash[:notice]
  end

  test "destroy" do
    delete admin_category_path(@category.id)
    assert_redirected_to admin_categories_path
    assert_equal "Success", flash[:notice]
  end
end
