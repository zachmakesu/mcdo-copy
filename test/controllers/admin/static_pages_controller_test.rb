require "test_helper"

class Admin::StaticPagesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @admin = create(:admin)
    sign_in @admin
  end
  test "index" do
    get admin_static_pages_path
    assert_response :success
  end
end
