require "test_helper"

class Admin::DeliveryNumbersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @admin = create(:admin)
    @area = create(:area)
    @branch = create(:branch)
    @delivery_number = create(:delivery_number)
    sign_in @admin
  end
  test "edit" do
    get edit_admin_delivery_number_path(@delivery_number)
    assert_response :success
  end
  test "update" do
    put admin_delivery_number_path(@delivery_number), params: {
      delivery_number: { number: "1234" }
    }
    assert_response :redirect
    assert_equal @delivery_number.reload.number, "1234"
  end
end
