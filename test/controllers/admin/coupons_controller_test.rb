require "test_helper"

class Admin::CouponsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @admin = create(:admin)
    @meal_option = create(:meal_option)
    @alacarte = create(:alacarte)
    @coupon = create(:coupon)
    @valid_params = {
      coupon: {
        name: Faker::Name.name,
        description: Faker::Lorem.sentence,
        usage_coun: Faker::Number.number,
        usage_type: "single",
        classification: "both",
        image_source: fixture_file_upload(Rails.root.join("test", "assets", "logo.png")),
        detail_image_source: fixture_file_upload(Rails.root.join("test", "assets", "logo.png")),
        products_attributes: [
          {
            productable_id: @meal_option.id,
            productable_type: "MealOption",
            sub_products_attributes: [
              {
                productable_id: @alacarte.id,
                productable_type: "Alacarte"
              }
            ]
          }
        ]
      }
    }
    @invalid_params = {
      coupon: {
        usage_type: Faker::Name.name
      }
    }
    sign_in @admin
  end

  test "index" do
    get admin_coupons_path
    assert_response :success
  end

  test "new" do
    get new_admin_coupon_path
    assert_response :success
  end

  test "create" do
    post admin_coupons_path, params: @valid_params
    assert_equal Coupon.last, assigns(:coupon)
    assert_includes Coupon.last.products.map(&:productable_id), @meal_option.id
    assert_includes Coupon.last.sub_products.map(&:productable_id), @alacarte.id
    assert_equal "Successfully Created", flash[:notice]
  end

  test "Failing create" do
    assert_raises(ArgumentError) { post admin_coupons_path, params: @invalid_params }
  end

  test "edit" do
    get edit_admin_coupon_path(@coupon.id)
    assert_response :success
  end

  test "update" do
    patch admin_coupon_path(@coupon.id), params: @valid_params
    assert_redirected_to admin_coupons_path
    assert_equal "Success", flash[:notice]
  end

  test "destroy" do
    delete admin_coupon_path(@coupon.id)
    assert_redirected_to admin_coupons_path
    assert_equal "Successfully deleted", flash[:notice]
  end
end
