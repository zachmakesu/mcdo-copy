require "test_helper"

class Admin::UsersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @admin = create(:admin)
    @user = create(:user)
    sign_in @admin
    password = Faker::Internet.password
    @valid_params = {
      user: {
        first_name: Faker::Name.first_name,
        last_name: Faker::Name.last_name,
        email: Faker::Internet.email,
        password: password,
        password_confirmation: password
      }
    }
  end
  test "index" do
    get admin_users_path
    assert_response :success
  end

  test "new" do
    get new_admin_user_path
    assert_response :success
  end

  test "create" do
    post admin_users_path, params: @valid_params
    assert_redirected_to admin_users_path
    assert_equal User.last, assigns(:user)
    assert_equal "Success", flash[:notice]
  end

  test "edit" do
    get edit_admin_user_path(@user.id)
    assert_response :success
  end

  test "update" do
    patch admin_user_path(@user.id), params: @valid_params
    assert_redirected_to admin_users_path
    assert_equal "Success", flash[:notice]
  end

  test "destroy" do
    delete admin_user_path(@user.id)
    assert_redirected_to admin_users_path
    assert_equal "Success", flash[:notice]
  end
end
