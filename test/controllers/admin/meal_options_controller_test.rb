require "test_helper"

class Admin::MealOptionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @admin = create(:admin)
    sign_in @admin
    @meal_option = create(:meal_option)
    @valid_params = {
      meal_option: {
        name: Faker::Name.unique.name,
        code_name: Faker::Commerce.promotion_code,
        long_name: Faker::Commerce.product_name,
        image_source: fixture_file_upload(Rails.root.join("test", "assets", "logo.png")),
        group_id: Faker::Number.number(2),
        size: "small",
        classification: "drink",
        is_default: Faker::Boolean.boolean,
        poscode: Faker::Commerce.promotion_code,
        price: Faker::Commerce.price
      }
    }
    @invalid_params = {
      meal_option: {
        name: nil
      }
    }
  end
  test "index" do
    get admin_meal_options_path
    assert_response :success
  end

  test "new" do
    get new_admin_meal_option_path
    assert_response :success
  end

  test "create" do
    post admin_meal_options_path, params: @valid_params
    assert_equal MealOption.last, assigns(:meal_option)
    assert_redirected_to admin_meal_option_path(MealOption.last)
    assert_equal "Success", flash[:notice]
  end

  test "Failing create" do
    post admin_meal_options_path, params: @invalid_params
    assert_template :new
  end

  test "edit" do
    get edit_admin_meal_option_path(@meal_option.id)
    assert_response :success
  end

  test "update" do
    patch admin_meal_option_path(@meal_option.id), params: @valid_params
    assert_equal "Success", flash[:notice]
  end

  test "destroy" do
    delete admin_meal_option_path(@meal_option.id)
    assert_redirected_to admin_meal_options_path
    assert_equal "Successfully deleted", flash[:notice]
  end
end
