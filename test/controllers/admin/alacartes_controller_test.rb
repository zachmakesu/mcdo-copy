require "test_helper"

class Admin::AlacartesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @admin = create(:admin)
    @alacarte = create(:alacarte)
    @category = create(:category)
    @section = create(:section)
    @schedule = create(:schedule)
    @meal_option = create(:meal_option)
    @valid_params = {
      alacarte: {
        name: "Alacarte 51",
        description: "Alacarte 51",
        category_id: @category.id,
        image_source: fixture_file_upload(Rails.root.join("test", "assets", "logo.png")),
        schedule_id: @schedule.id,
        variances_attributes: [
          {
            menu_name: "Alacarte 51",
            name: "Alacarte 51",
            code_name: "A51",
            poscode: "A51",
            price: 123.00
          }
        ],
        options_attributes: [
          {
            choice_id: @meal_option.id,
            section_id: @section.id
          }
        ]
      }
    }
    @invalid_params = {
      alacarte: {
        name: nil
      }
    }
    sign_in @admin
  end

  test "index" do
    get admin_alacartes_path
    assert_response :success
  end

  test "show" do
    get admin_alacarte_path(@alacarte.id)
    assert_response :success
  end

  test "new" do
    get new_admin_alacarte_path
    assert_response :success
  end

  test "create" do
    post admin_alacartes_path, params: @valid_params
    assert_redirected_to admin_alacartes_path
    assert_equal Alacarte.last, assigns(:alacarte)
    assert_equal "Success", flash[:notice]
  end

  test "Failing create" do
    post admin_alacartes_path, params: @invalid_params
    assert_template :new
    assert_not_equal Alacarte.last, assigns(:alacarte)
  end

  test "edit" do
    get edit_admin_alacarte_path(@alacarte.id)
    assert_response :success
  end

  test "update" do
    patch admin_alacarte_path(@alacarte.id), params: @valid_params
    assert_redirected_to admin_alacartes_path
    assert_equal "Success", flash[:notice]
  end

  test "destroy" do
    delete admin_alacarte_path(@alacarte.id)
    assert_redirected_to admin_alacartes_path
    assert_equal "Successfully deleted", flash[:notice]
  end

  test "toggle_display" do
    put admin_alacarte_path(@alacarte.id), params: { display: "true" }, xhr: true
    body = JSON.parse(@response.body)
    assert_equal body["message"], "Success."
  end
end
