require "test_helper"

class Admin::CouponUtilizationsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @admin = create(:admin)
    sign_in @admin
  end
  test "index" do
    get admin_coupon_utilizations_path
    assert_response :success
  end
end
