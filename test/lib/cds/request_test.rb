require "test_helper"

class CDS::RequestTest < ActiveSupport::TestCase
  setup do
    @subject = CDS::Request
    @request = @subject.new(api: CDS::API.new)
  end

  test "it should be initialized with api subclass" do
    assert_raise ArgumentError do
      @subject.new
    end
    refute_nil @request
  end

  test "it has #default_headers" do
    headers = {
      "Content-Type" => "application/json"
    }
    assert_equal @request.default_headers, headers
  end
end
