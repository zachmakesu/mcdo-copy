require "test_helper"

class CDS::Model::ProvinceTest < ActiveSupport::TestCase
  setup do
    @subject = CDS::Model::Province
    @params = {
      id: "123"
    }
    @province = @subject.new(params: @params)
  end
  test "it has id as its #id" do
    assert_equal @province.id, "123"
  end
  test "it has province as its #prefix" do
    assert_equal @province.prefix, "province"
  end
  test "it has provinces as its .all_key" do
    assert_equal @subject.all_key, "provinces"
  end
  test "it has province123 as its #key" do
    assert_equal @province.key, "province123"
  end
end
