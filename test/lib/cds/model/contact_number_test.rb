require "test_helper"

class CDS::Model::ContactNumberTest < ActiveSupport::TestCase
  setup do
    @subject = CDS::Model::ContactNumber
    @params = {
      id: "123"
    }
    @contact_number = @subject.new(params: @params)
  end
  test "it has id as its #id" do
    assert_equal @contact_number.id, "123"
  end
  test "it has contact_number as its #prefix" do
    assert_equal @contact_number.prefix, "contact_number"
  end
  test "it has contact_number123 as its #key" do
    assert_equal @contact_number.key, "contact_number123"
  end
end
