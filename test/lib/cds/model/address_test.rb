require "test_helper"

class CDS::Model::AddressTest < ActiveSupport::TestCase
  setup do
    @subject = CDS::Model::Address
    @params = {
      id: "123"
    }
    @address = @subject.new(params: @params)
  end
  test "it has id as its #id" do
    assert_equal @address.id, "123"
  end
  test "it has address as its #prefix" do
    assert_equal @address.prefix, "address"
  end
  test "it has address123 as its #key" do
    assert_equal @address.key, "address123"
  end
end
