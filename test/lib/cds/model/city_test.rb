require "test_helper"

class CDS::Model::CityTest < ActiveSupport::TestCase
  setup do
    @subject = CDS::Model::City
    @params = {
      id: "123"
    }
    @city = @subject.new(params: @params)
  end
  test "it has id as its #id" do
    assert_equal @city.id, "123"
  end
  test "it has city as its #prefix" do
    assert_equal @city.prefix, "city"
  end
  test "it has city as its .all_key" do
    assert_equal @subject.all_key, "cities"
  end
  test "it has city123 as its #key" do
    assert_equal @city.key, "city123"
  end
end
