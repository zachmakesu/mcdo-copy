require "test_helper"

class CDS::Model::UserProfileTest < ActiveSupport::TestCase
  setup do
    @subject = CDS::Model::UserProfile
    @params = {
      user_id: "123"
    }
    @user_profile = @subject.new(params: @params)
  end
  test "it has user_id as its #id" do
    assert_equal @user_profile.id, "123"
  end
  test "it has user_profile as its #prefix" do
    assert_equal @user_profile.prefix, "user_profile"
  end
  test "it has user_profile123 as its #key" do
    assert_equal @user_profile.key, "user_profile123"
  end
end
