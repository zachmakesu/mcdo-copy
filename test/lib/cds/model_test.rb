require "test_helper"

class CDS::ModelTest < ActiveSupport::TestCase
  setup do
    @subject = CDS::Model
  end
  test "it raises NotImplementedError for #id" do
    assert_raise NotImplementedError do
      @subject.new(params: {}).id
    end
  end
  test "it raises NotImplementedError for #prefix" do
    assert_raise NotImplementedError do
      @subject.new(params: {}).prefix
    end
  end
end
