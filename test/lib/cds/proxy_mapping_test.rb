require "test_helper"

class CDS::ProxyMappingTest < ActiveSupport::TestCase
  setup do
    @subject = CDS::ProxyMapping
  end
  test "it maps api action to api path" do
    action = "get_user_profile"
    mapping = @subject.new(action: action)
    assert_equal mapping.path, "User"
  end
end
