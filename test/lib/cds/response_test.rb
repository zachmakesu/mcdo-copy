require "test_helper"

class CDS::ResponseTest < ActiveSupport::TestCase
  setup do
    @subject = CDS::Response
  end
  test "it requires data on initialization" do
    assert_raise ArgumentError do
      @subject.new
    end
  end
  test "it has #object property that is an OStruct representation of its data" do
    status = 404
    error = "Not Found"
    response = @subject.new(data: { status: status, error: error })
    assert_equal response.object.status, status
    assert_equal response.object.error, error
  end
  test "it returns false for #success unless object status is 200" do
    status = 404
    response = @subject.new(data: { status: status })
    refute response.success?
  end
  test "it has #error if unsuccessful" do
    status = 404
    error = "Not Found"
    response = @subject.new(data: { status: status, error: error })
    assert_equal response.error, error
  end
end
