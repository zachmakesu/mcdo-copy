require "test_helper"

class CDS::APITest < ActiveSupport::TestCase
  setup do
    @subject = CDS::API.new
    @config = @subject.config
  end

  test "it expects #action from subclasses so empty by default" do
    assert_equal @subject.action, ""
  end

  test "it expects #params from subclasses so empty hash by default" do
    assert_equal @subject.params, {}
  end

  test "it has CDS::Config instance as default configuration" do
    assert_instance_of CDS::Config, @subject.config
  end

  test "it has #data payload" do
    assert_equal @subject.data, action: "",
                                api_key: @config.public_key,
                                contents: {},
                                hostname: @config.host_name,
                                source: @config.source_name,
                                timestamp: @config.timestamp
  end

  test "it has #data payload in correct order" do
    assert_equal @subject.data.keys, %i[
      api_key
      timestamp
      source
      hostname
      action
      contents
    ]
  end

  test "it has #signature property" do
    key = @config.private_key
    data = @subject.data.to_json
    digest = OpenSSL::Digest.new("sha256")
    hmac = OpenSSL::HMAC.digest(digest, key, data)
    signature = Base64.strict_encode64(hmac)
    assert_equal @subject.signature, signature
  end

  test "it has complete #body property" do
    assert_equal @subject.body, data: @subject.data,
                                signature: @subject.signature
  end
end
