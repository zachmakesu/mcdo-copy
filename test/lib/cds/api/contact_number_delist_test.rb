require "test_helper"

class CDS::API::ContactNumberDelistTest < ActiveSupport::TestCase
  setup do
    @subject = CDS::API::ContactNumberDelist
    @action = "delist_user_contact_no"
    @user_id = "userid"
    @ids = [1]
  end

  test "it has delist_user_contact_no as action" do
    assert_equal @subject.new(user_id: @user_id, ids: @ids).action, @action
  end

  test "it requires userid and contact information for params" do
    refute_nil @subject.new(user_id: @user_id, ids: @ids)
  end

  test "it has valid contact number for params" do
    contact_number = @subject.new(user_id: @user_id, ids: @ids)
    first_number = contact_number.params[:contact_numbers].first
    assert_equal first_number[:id], 1
  end

  test "it has #data payload" do
    subject = @subject.new(user_id: @user_id, ids: @ids)
    refute_empty subject.data
    assert_equal subject.data[:action], @action
  end
end
