require "test_helper"

class CDS::API::UserAddressDelistTest < ActiveSupport::TestCase
  setup do
    @subject = CDS::API::UserAddressDelist
    @action = "delist_user_address"
    @user_id = "userid"
    @ids = [1]
  end

  test "it has delist_user_address as action" do
    assert_equal @subject.new(user_id: @user_id, ids: @ids).action, @action
  end

  test "it requires userid and address formation for params" do
    refute_nil @subject.new(user_id: @user_id, ids: @ids)
  end

  test "it has valid address for params" do
    address = @subject.new(user_id: @user_id, ids: @ids)
    first_address = address.params[:address].first
    assert_equal first_address[:id], 1
  end

  test "it has #data payload" do
    subject = @subject.new(user_id: @user_id, ids: @ids)
    refute_empty subject.data
    assert_equal subject.data[:action], @action
  end
end
