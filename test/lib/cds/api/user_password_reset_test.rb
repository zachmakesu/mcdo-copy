require "test_helper"

class CDS::API::UserPasswordResetTest < ActiveSupport::TestCase
  setup do
    @subject = CDS::API::UserPasswordReset
    @action = "reset_user_password"
    @params = {
      email_address: "email@email.com"
    }
  end

  test "it has reset_user_password as action" do
    assert_equal @subject.new(@params).action, @action
  end

  test "it requires user email for params" do
    refute_nil @subject.new(@params)
  end

  test "it has #data payload" do
    subject = @subject.new(@params)
    refute_empty subject.data
    assert_equal subject.data[:action], @action
  end
end
