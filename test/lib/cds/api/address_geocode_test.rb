require "test_helper"

class CDS::API::AddressGeocodeTest < ActiveSupport::TestCase
  setup do
    @subject = CDS::API::AddressGeocode
    @action = "save_latlong"
    @params = {
      lat: 0,
      lng: 0,
      address_id: 1
    }
  end

  test "it has save_latlong as action" do
    assert_equal @subject.new(@params).action, @action
  end

  test "it requires lat, lng, and address_id for params" do
    refute_nil @subject.new(@params)
  end

  test "it has #data payload" do
    subject = @subject.new(@params)
    refute_empty subject.data
    assert_equal subject.data[:action], @action
  end
end
