require "test_helper"

class CDS::API::ContactNumberUpdateTest < ActiveSupport::TestCase
  setup do
    @subject = CDS::API::ContactNumberUpdate
    @action = "update_user_contact_no"
    @user_id = 123
    @contact_numbers = [
      {
        contact_number: "09191234567",
        extension_number: "01",
        delisted: 0
      }.stringify_keys
    ]
  end

  test "it has update_user_contact_no as action" do
    assert_equal @subject.new(user_id: @user_id, numbers: @contact_numbers).action, @action
  end

  test "it requires userid and contact information for params" do
    refute_nil @subject.new(user_id: @user_id, numbers: @contact_numbers)
  end

  test "it has valid contact number for params" do
    contact_number = @subject.new(user_id: @user_id, numbers: @contact_numbers)
    first_number = contact_number.params[:contact_numbers].first
    assert_equal first_number["contact_number"], "09191234567"
    assert_equal first_number["extension_number"], "01"
    assert_equal first_number["delisted"], 0
  end

  test "it has #data payload" do
    subject = @subject.new(user_id: @user_id, numbers: @contact_numbers)
    refute_empty subject.data
    assert_equal subject.data[:action], @action
  end
end
