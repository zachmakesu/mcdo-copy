require "test_helper"

class CDS::API::EmailValidateTest < ActiveSupport::TestCase
  setup do
    @subject = CDS::API::EmailValidate
    @action = "email_validation"
    @params = {
      user_id: 1,
      email_address: "email@email.com",
      validate: 1
    }
  end

  test "it has email_validation as action" do
    assert_equal @subject.new(@params).action, @action
  end

  test "it requires user id, email and validate for params" do
    refute_nil @subject.new(@params)
  end

  test "it has #data payload" do
    subject = @subject.new(@params)
    refute_empty subject.data
    assert_equal subject.data[:action], @action
  end
end
