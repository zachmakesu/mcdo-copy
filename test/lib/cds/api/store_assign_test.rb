require "test_helper"

class CDS::API::StoreAssignTest < ActiveSupport::TestCase
  setup do
    @subject = CDS::API::StoreAssign
    @action = "assign_store"
    @params = {
      city_name: "MAKATI CITY",
      address: "Makati"
    }
  end

  test "it has assign_store as action" do
    assert_equal @subject.new(@params).action, @action
  end

  test "it requires city name and address for params" do
    refute_nil @subject.new(@params)
  end

  test "it has #data payload" do
    subject = @subject.new(@params)
    refute_empty subject.data
    assert_equal subject.data[:action], @action
  end
end
