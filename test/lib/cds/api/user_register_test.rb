require "test_helper"

class CDS::API::UserRegisterTest < ActiveSupport::TestCase
  setup do
    @subject = CDS::API::UserRegister
    @action = "user_registration"
    @params = {
      email:    "example@email.com",
      password: "password",
      confirm_password: "password"
    }
  end

  test "it has user_registration as action" do
    assert_equal @subject.new(@params).action, @action
  end

  test "it requires email, password, and password confirmation for params" do
    assert_equal @subject.new(@params).params, @params
  end

  test "it has #data payload" do
    subject = @subject.new(@params)
    refute_empty subject.data
    assert_equal subject.data[:action], @action
  end
end
