require "test_helper"

class CDS::API::UserProfileGetTest < ActiveSupport::TestCase
  setup do
    @subject = CDS::API::UserProfileGet
    @action = "get_user_profile"
    @params = {
      user_id:    "userid"
    }
  end

  test "it has get_user_profile as action" do
    assert_equal @subject.new(@params).action, @action
  end

  test "it requires user_id for params" do
    assert_equal @subject.new(@params).params, @params
  end

  test "it has #data payload" do
    user_login = @subject.new(@params)
    refute_empty user_login.data
    assert_equal user_login.data[:action], @action
  end
end
