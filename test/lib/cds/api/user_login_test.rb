require "test_helper"

class CDS::API::UserLoginTest < ActiveSupport::TestCase
  setup do
    @subject = CDS::API::UserLogin
    @action = "user_login"
    @params = {
      email:    "example@email.com",
      password: "password"
    }
  end

  test "it has user_login as action" do
    assert_equal @subject.new(@params).action, @action
  end

  test "it requires email and password for params" do
    assert_equal @subject.new(@params).params, @params
  end

  test "it has #data payload" do
    user_login = @subject.new(@params)
    refute_empty user_login.data
    assert_equal user_login.data[:action], @action
  end
end
