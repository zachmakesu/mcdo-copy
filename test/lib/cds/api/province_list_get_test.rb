require "test_helper"

class CDS::API::ProvinceListGetTest < ActiveSupport::TestCase
  setup do
    @subject = CDS::API::ProvinceListGet
    @action = "get_province_list"
  end

  test "it has reset_user_password as action" do
    assert_equal @subject.new.action, @action
  end

  test "it has #data payload" do
    subject = @subject.new
    refute_empty subject.data
    assert_equal subject.data[:action], @action
  end
end
