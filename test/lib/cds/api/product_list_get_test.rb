require "test_helper"

class CDS::API::ProductListGetTest < ActiveSupport::TestCase
  setup do
    @subject = CDS::API::ProductListGet
    @action = "get_product_list"
  end

  test "it has get_product_list as action" do
    assert_equal @subject.new.action, @action
  end

  test "it has #data payload" do
    subject = @subject.new
    refute_empty subject.data
    assert_equal subject.data[:action], @action
  end
end
