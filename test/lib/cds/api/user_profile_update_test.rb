require "test_helper"

class CDS::API::UserProfileUpdateTest < ActiveSupport::TestCase
  setup do
    @subject = CDS::API::UserProfileUpdate
    @action = "update_user_profile"
    @user_id = 123
    @params = {
      salutation:   "Mr.",
      firstName:   "Elliot",
      middleName:  "Edward",
      lastName:    "Alderson",
      birthdate:   "1991/01/01",
      subscription: 1
    }
  end

  test "it has update_user_profile as action" do
    assert_equal @subject.new(user_id: @user_id, profile: @params).action, @action
  end

  test "it requires user information for params" do
    refute_nil @subject.new(user_id: @user_id, profile: @params)
  end

  test "it correctly maps keys for special case e.g. firstname: first_name" do
    user_profile = @subject.new(user_id: @user_id, profile: @params)
    assert_equal user_profile.params[:firstname], @params[:firstName]
    assert_equal user_profile.params[:lastname], @params[:lastName]
  end

  test "it has #data payload" do
    user_login = @subject.new(user_id: @user_id, profile: @params)
    refute_empty user_login.data
    assert_equal user_login.data[:action], @action
  end
end
