require "test_helper"

class CDS::API::EmailVerifyTest < ActiveSupport::TestCase
  setup do
    @subject = CDS::API::EmailVerify
    @action = "is_verified_email"
    @params = {
      email_address: "email@email.com"
    }
  end

  test "it has is_verified_email as action" do
    assert_equal @subject.new(@params).action, @action
  end

  test "it requires email for params" do
    refute_nil @subject.new(@params)
  end

  test "it has #data payload" do
    subject = @subject.new(@params)
    refute_empty subject.data
    assert_equal subject.data[:action], @action
  end
end
