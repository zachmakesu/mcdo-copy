require "test_helper"

class CDS::API::StoreStatusCheckTest < ActiveSupport::TestCase
  setup do
    @subject = CDS::API::StoreStatusCheck
    @action = "check_store_status"
    @params = {
      store_id: 1
    }
  end

  test "it has check_store_status as action" do
    assert_equal @subject.new(@params).action, @action
  end

  test "it requires store_id for params" do
    refute_nil @subject.new(@params)
  end

  test "it has #data payload" do
    subject = @subject.new(@params)
    refute_empty subject.data
    assert_equal subject.data[:action], @action
  end
end
