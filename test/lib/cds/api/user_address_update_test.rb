require "test_helper"

class CDS::API::UserAddressUpdateTest < ActiveSupport::TestCase
  setup do
    @subject = CDS::API::UserAddressUpdate
    @action = "update_user_address"
    @user = 123
    @params = [
      {
        status: "primary",
        type: "residential",
        addressLabel: "Residence",
        companyName: "E-Corp",
        building: "E-Corp Bldg.",
        floor: 1,
        area: "Square",
        street: "St",
        landmark: "Landmark",
        cityId: 1,
        addressRemarks: "Example",
        geotagX: 1,
        geotagY: 2,
        delisted: 0,
        isVerified: 1,
        addressId: 1
      }.stringify_keys
    ]
  end

  test "it has update_user_address as action" do
    assert_equal @subject.new(user_id: @user_id,
                              addresses: @params).action, @action
  end

  test "it requires user address for params" do
    refute_nil @subject.new(user_id: @user_id, addresses: @params)
  end

  test "it correctly maps keys for special case e.g. address_label: label" do
    user_address = @subject.new(user_id: @user_id,
                                addresses: @params)
    address_params = user_address.addresses.first
    assert_equal address_params[:addressLabel], @params.first[:addressLabel]
    assert_equal address_params[:addressRemarks], @params.first[:addressRemarks]
  end

  test "it has #data payload" do
    subject = @subject.new(user_id: @user_id,
                           addresses: @params)
    refute_empty subject.data
    assert_equal subject.data[:action], @action
  end

  test "it changes cityId to city" do
    subject = @subject.new(user_id: @user_id, addresses: @params)
    assert_equal @params.first[:city_id], subject.params[:address].first[:city]
    assert_nil subject.params[:address].first[:cityId]
  end
end
