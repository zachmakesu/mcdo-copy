require "test_helper"

class CDS::API::FBUserRegistrationTest < ActiveSupport::TestCase
  setup do
    @subject = CDS::API::FBUserRegistration
    @action = "fb_user_registration"
    @params = {
      email_address: "example@email.com"
    }
  end

  test "it has fb_user_registration as action" do
    assert_equal @subject.new(@params).action, @action
  end

  test "it requires email for params" do
    assert_equal @subject.new(@params).params, @params
  end

  test "it has #data payload" do
    user_login = @subject.new(@params)
    refute_empty user_login.data
    assert_equal user_login.data[:action], @action
  end
end
