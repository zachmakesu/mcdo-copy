require "test_helper"

class CDS::API::SimpleUserRegisterTest < ActiveSupport::TestCase
  setup do
    @subject = CDS::API::SimpleUserRegister
    @action = "user_registration"
    @params = {
      email:    "example@email.com",
      password: "password",
      confirm_password: "password"
    }.stringify_keys
  end

  test "it has simple_user_registration as action" do
    assert_equal @subject.new(credentials: @params).action, @action
  end

  test "it requires email, name, password, and password confirmation for params" do
    @params["email"] = nil
    refute_nil @subject.new(credentials: @params).params
  end

  test "it has #data payload" do
    subject = @subject.new(credentials: @params)
    refute_empty subject.data
    assert_equal subject.data[:action], @action
  end
end
