require "test_helper"

class CDS::API::UserPasswordUpdateTest < ActiveSupport::TestCase
  setup do
    @subject = CDS::API::UserPasswordUpdate
    @action = "update_user_password"
    @params = {
      user_id:          "userid",
      old_password:     "oldpassword",
      new_password:     "newpassword",
      confirm_password: "newpassword"
    }
  end

  test "it has update_user_password as action" do
    assert_equal @subject.new(@params).action, @action
  end

  test "it requires user credential for params" do
    refute_nil @subject.new(@params)
  end

  test "it has #data payload" do
    subject = @subject.new(@params)
    refute_empty subject.data
    assert_equal subject.data[:action], @action
  end
end
