require "test_helper"

class CDS::API::DeliveryStatusCheckTest < ActiveSupport::TestCase
  setup do
    @subject = CDS::API::DeliveryStatusCheck
    @action = "customer_delivery_order_status"
    @params = {
      order_id: 1
    }
  end

  test "it has customer_delivery_order_status as action" do
    assert_equal @subject.new(@params).action, @action
  end

  test "it requires order_id for params" do
    refute_nil @subject.new(@params)
  end

  test "it has #data payload" do
    subject = @subject.new(@params)
    refute_empty subject.data
    assert_equal subject.data[:action], @action
  end
end
