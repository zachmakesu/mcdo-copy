require "test_helper"

class CDS::API::UserPasswordViaTokenResetTest < ActiveSupport::TestCase
  setup do
    @subject = CDS::API::UserPasswordViaTokenReset
    @action = "forget_password"
    @params = {
      token: "token",
      password: "password",
      confirm_password: "confirm_password"
    }
  end

  test "it has forget_password as action" do
    assert_equal @subject.new(@params).action, @action
  end

  test "it requires reset token and password for params" do
    refute_nil @subject.new(@params)
  end

  test "it correctly maps keys for special case e.g. password_reset_token: token" do
    subject = @subject.new(@params)
    assert_equal subject.params[:password_reset_token], @params[:token]
  end

  test "it has #data payload" do
    subject = @subject.new(@params)
    refute_empty subject.data
    assert_equal subject.data[:action], @action
  end
end
