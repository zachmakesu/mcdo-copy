require "test_helper"

class CDS::API::UserAddressAddTest < ActiveSupport::TestCase
  setup do
    @subject = CDS::API::UserAddressAdd
    @action = "add_user_address"
    @user_id = 123
    @params = [
      {
        status: "primary",
        type: "residential",
        label: "Residence",
        company_name: "E-Corp",
        building: "E-Corp Bldg.",
        floor: 1,
        area: "Square",
        street: "St",
        landmark: "Landmark",
        cityId: 1,
        remarks: "Example",
        geotag_x: 1,
        geotag_y: 2,
        delisted: 0,
        is_verified: 1
      }.stringify_keys
    ]
  end

  test "it has add_user_address as action" do
    assert_equal @subject.new(user_id: @user_id, addresses: @params).action, @action
  end

  test "it requires user address for params" do
    refute_nil @subject.new(user_id: @user_id, addresses: @params)
  end

  test "it correctly maps keys for special case e.g. address_label: label" do
    user_address = @subject.new(user_id: @user_id, addresses: @params)
    address_params = user_address.params[:address].first
    assert_equal address_params[:address_label], @params.first[:label]
    assert_equal address_params[:landmark], @params.first[:landmark]
  end

  test "it has #data payload" do
    subject = @subject.new(user_id: @user_id, addresses: @params)
    refute_empty subject.data
    assert_equal subject.data[:action], @action
  end

  test "it changes cityId to city" do
    subject = @subject.new(user_id: @user_id, addresses: @params)
    assert_equal @params.first[:city_id], subject.params[:address].first[:city]
    assert_nil subject.params[:address].first[:cityId]
  end
end
