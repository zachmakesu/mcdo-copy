require "test_helper"

class CDS::ConfigTest < ActiveSupport::TestCase
  setup do
    @subject = CDS::Config.new
  end

  test "it has #public_key" do
    assert_equal @subject.public_key, "cds_public_key"
  end

  test "it has #private_key" do
    assert_equal @subject.private_key, "cds_private_key"
  end

  test "it has #host_name" do
    assert_equal @subject.host_name, "cds_host_name"
  end

  test "it has #source_name" do
    assert_equal @subject.source_name, "cds_source_name"
  end

  test "it has #timestamp" do
    refute_nil @subject.timestamp
  end
end
