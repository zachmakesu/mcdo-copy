require "test_helper"

class CDS::ProxyRequestTest < ActiveSupport::TestCase
  setup do
    @subject = CDS::ProxyRequest
    @request = @subject.new(api: CDS::API.new)
  end

  test "it should be initialized with api subclass" do
    assert_raise ArgumentError do
      @subject.new
    end
    refute_nil @request
  end

  test "it has extracted #body from api" do
    source_name = ENV.fetch("CDS_PROXY_SOURCE_NAME")
    assert_equal @request.body, source: source_name
  end

  test "it has #url built from api action mapping" do
    api = CDS::API::UserProfileGet.new(user_id: "")
    subject = CDS::ProxyRequest.new(api: api)
    base_url = ENV.fetch("CDS_PROXY_API_URL")
    assert_equal subject.url, "#{base_url}/#{User}"
  end
end
