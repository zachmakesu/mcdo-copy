require "test_helper"

class JWTManagerTest < ActiveSupport::TestCase
  setup do
    @subject = JWTManager
  end
  test "it returns jwt for successful encoding" do
    payload = {
      "user_id": "1991",
      "exp": 1_515_011_838
    }
    expected_jwt = "eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoiMTk5MSIsImV4cCI6MTUxNTAxMTgzOH0.VDkn32kFIWe4wI0Ix0TdHhSbSvEDlU1L09_Ajco_cF8"
    jwt = @subject.encode(payload: payload)
    assert_equal jwt.jwt, expected_jwt
    assert_nil jwt.error_message
  end
  test "it returns correct error message for expired JWT" do
    # {
    #   "user_id": "1991",
    #   "exp": 15149969
    # }
    jwt = @subject.decode(jwt: "eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoiMTk5MSIsImV4cCI6MTUxNDk5Njl9.K3Mzv7zNipEjyRjmoZtNs9mskVKI9SIlFTLIcVvgUQc")
    message = "JWT is expired, please trying logging in again!"
    assert_equal jwt.error_message, message
    assert_nil jwt.payload
  end
  # Invalid meaning incorrect header(?)
  test "it returns correct error message for invalid JWT" do
    jwt = @subject.decode(jwt: "eyJhGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoiMTk5MSIsImV4cCI6MTUxNDk5Njl9.K3Mzv7zNipEjyRjmoZtNs9mskVKI9SIlFTLIcVvgUQc")
    message = "JWT is invalid and cannot be decoded."
    assert_equal jwt.error_message, message
    assert_nil jwt.payload
  end
  # Failed decoding verification meaning malformed signature(?)
  test "it returns correct error message for failed decoding verification" do
    jwt = @subject.decode(jwt: "eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoiMTk5MSIsImV4cCI6MTUxNDk5Njl9.K3Mzv7zNipEjyRjmoZtNs9mskVKI9SIlFTLIcVvg")
    message = "JWT cannot be verified."
    assert_equal jwt.error_message, message
    assert_nil jwt.payload
  end
end
