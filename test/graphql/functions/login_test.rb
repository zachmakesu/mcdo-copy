require "test_helper"

class Functions::LoginTest < ActiveSupport::TestCase
  def perform(args={})
    Functions::Login.new.call(nil, args, cookies: {})
  end

  setup do
    @user = create(:user)
  end

  # TODO : Stub requests to api since we're no longer
  # authenticating using User model
  test "valid user login" do
    # result = perform(
    #   email: @user.email,
    #   password: @user.password
    # )
    # assert result.present?
    # assert result.jwt.present?
    # assert result.user_id.present?
  end

  test "invalid password" do
    # result = perform(
    #   email: @user.email,
    #   password: "WrongPassword"
    # )
    # assert_equal result.message, "User password is incorrect"
  end

  test "missing credentials" do
    # result = perform
    # assert_equal result.message, "User with email does not exist"
  end
end
