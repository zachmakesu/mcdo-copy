require "test_helper"

class Resolvers::AuthRequiredResolverTest < ActiveSupport::TestCase
  test "it returns the custom jwt error if jwt_error is present" do
    jwt_error = "Something went wrong with jwt"
    ctx = { jwt_error: jwt_error }
    resolver = Resolvers::AuthRequiredResolver.new(->(_obj, _args, _ctx) {})
    exception = assert_raises(Resolvers::AuthRequiredResolverJWTError) do
      resolver.call(nil, nil, ctx)
    end
    assert_equal jwt_error, exception.message
  end

  test "it returns error if current_user is missing in context" do
    current_user = nil
    ctx = { current_user: current_user }
    auth_required_error = "Authorization is required for this operation."
    resolver = Resolvers::AuthRequiredResolver.new(->(_obj, _args, _ctx) {})
    exception = assert_raises(Resolvers::AuthRequiredResolverUnauthorizedError) do
      resolver.call(nil, nil, ctx)
    end
    assert_equal auth_required_error, exception.message
  end

  # Authentication is "valid" if there are no jwt errors and current_user exists
  test "it returns the value passed to resolver if authentication is valid" do
    current_user = create(:user)
    ctx = { current_user: current_user }
    resolver = Resolvers::AuthRequiredResolver.new(->(_obj, _args, _ctx) { current_user })
                                              .call(nil, nil, ctx)
    assert_equal resolver, current_user
  end
end
