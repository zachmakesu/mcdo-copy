source "https://rubygems.org"
ruby "2.5.0"

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end

gem "active_type", ">= 0.3.2"
gem "autoprefixer-rails", ">= 5.0.0.1"
gem "bcrypt", "~> 3.1.7"
gem "bootsnap", ">= 1.1.0", :require => false
gem "coffee-rails", "~> 4.2"
gem "devise"
gem "dotenv-rails", ">= 2.0.0"
gem 'graphql', '~> 1.7', '>= 1.7.7'
gem "jquery-rails"
gem "jwt"
gem "mail", ">= 2.6.3"
gem "marco-polo"
gem "pg", "~> 0.18"
gem "pgcli-rails"
gem "rails", "5.1.4"
gem "rbnacl", "~> 3.4", :require => false
gem "rbnacl-libsodium", :require => false
gem "redis-objects"
gem "sass-rails", "~> 5.0"
gem "secure_headers", "~> 3.0"
gem "sidekiq", ">= 4.2.0"
gem 'carrierwave', '~> 1.0'
gem "turbolinks", "~> 5"
gem "typhoeus"
gem 'sweetalert-rails'
gem "cocoon"
gem 'carrierwave-i18n'
gem 'kaminari'
gem 'graphql-errors'
gem "font-awesome-rails"
gem "tabs_on_rails"
gem 'exception_notification'
gem "tagsinput-rails"
gem 'acts_as_list'
gem "chartkick"
gem 'groupdate'
gem 'geocoder'
gem 'browser'

group :production, :staging do
  gem "postmark-rails"
  gem "unicorn"
  gem "unicorn-worker-killer"
end

group :development do
  gem "annotate", ">= 2.5.0"
  gem "awesome_print"
  gem "bcrypt_pbkdf", :require => false
  gem "better_errors"
  gem "binding_of_caller", "~> 0.8.0"
  gem "brakeman", :require => false
  gem "bundler-audit", ">= 0.5.0", :require => false
  gem "capistrano", "~> 3.6", :require => false
  gem "capistrano-bundler", "~> 1.2", :require => false
  gem "capistrano-mb", "~> 0.33.0", :require => false
  gem "capistrano-rails", :require => false
  gem "capistrano-rbenv", "~> 2.1"
  gem "foreman"
  gem "guard", ">= 2.2.2", :require => false
  gem "guard-livereload", :require => false
  gem "guard-minitest", :require => false
  gem "letter_opener"
  gem "listen", ">= 3.0.5"
  gem "overcommit", ">= 0.37.0", :require => false
  gem "puma", "~> 3.7"
  gem "rack-livereload"
  gem "rb-fsevent", :require => false
  gem "rubocop", ">= 0.50.0", :require => false
  gem "simplecov", :require => false
  gem "spring"
  gem "sshkit", "~> 1.8", :require => false
  gem "spring-watcher-listen", "~> 2.0.0"
  gem "terminal-notifier", :require => false
  gem "terminal-notifier-guard", :require => false
  gem "xray-rails", ">= 0.1.18"
  gem "scss_lint", require: false
end

group :test do
  gem "capybara", "~> 2.13"
  gem "connection_pool"
  gem "launchy"
  gem "minitest-ci", ">= 3.3.0", :require => false
  gem "minitest-reporters"
  gem "mocha"
  gem "selenium-webdriver"
  gem "shoulda-context"
  gem "shoulda-matchers", ">= 3.0.1"
end

group :development, :test do
  gem 'factory_bot_rails'
  gem "rails-controller-testing"
  gem 'faker', :git => 'https://github.com/stympy/faker.git', :branch => 'master'
end

# http://bundler.io/v1.5/groups.html
group :development, :staging do
  gem 'graphiql-rails'
end
