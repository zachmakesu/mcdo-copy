# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180404040659) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "alacartes", force: :cascade do |t|
    t.integer "category_id"
    t.string "name"
    t.integer "group_id"
    t.string "legacy_sections"
    t.string "image_source"
    t.integer "best_with"
    t.text "description"
    t.integer "schedule_id"
    t.integer "product_order_id"
    t.boolean "discounted_rice"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "displayed_at"
    t.integer "sequence_id"
  end

  create_table "areas", force: :cascade do |t|
    t.string "name"
    t.text "description", default: ""
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "available_hours", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "branches", force: :cascade do |t|
    t.bigint "area_id"
    t.string "name"
    t.text "description", default: ""
    t.string "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "image_source"
    t.text "address"
    t.float "lat"
    t.float "lng"
    t.string "offerings"
    t.index ["area_id"], name: "index_branches_on_area_id"
  end

  create_table "carts", force: :cascade do |t|
    t.bigint "customer_id"
    t.datetime "expires_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["customer_id"], name: "index_carts_on_customer_id"
  end

  create_table "carts_variances", force: :cascade do |t|
    t.bigint "cart_id"
    t.bigint "variance_id"
    t.integer "quantity", default: 1
    t.boolean "best_with", default: false
    t.integer "best_with_quantity", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["cart_id"], name: "index_carts_variances_on_cart_id"
    t.index ["variance_id"], name: "index_carts_variances_on_variance_id"
  end

  create_table "carts_variances_meal_options", force: :cascade do |t|
    t.bigint "carts_variance_id"
    t.bigint "meal_option_id"
    t.integer "quantity", default: 1
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["carts_variance_id"], name: "index_carts_variances_meal_options_on_carts_variance_id"
    t.index ["meal_option_id"], name: "index_carts_variances_meal_options_on_meal_option_id"
  end

  create_table "categories", force: :cascade do |t|
    t.string "name"
    t.integer "category_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "sequence_id"
    t.boolean "is_active", default: false, null: false
  end

  create_table "coupon_products", force: :cascade do |t|
    t.bigint "coupon_id"
    t.string "productable_type"
    t.bigint "productable_id"
    t.bigint "main_product_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["coupon_id"], name: "index_coupon_products_on_coupon_id"
    t.index ["main_product_id"], name: "index_coupon_products_on_main_product_id"
    t.index ["productable_type", "productable_id"], name: "index_coupon_products_on_productable_type_and_productable_id"
  end

  create_table "coupons", force: :cascade do |t|
    t.string "name"
    t.string "classification"
    t.string "usage_type"
    t.text "description"
    t.bigint "usage_count"
    t.datetime "validity_start_at"
    t.datetime "validity_end_at"
    t.string "image_source"
    t.string "detail_image_source"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.float "discount"
    t.string "poscode"
  end

  create_table "customers", force: :cascade do |t|
    t.integer "cds_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "delivery_fee_histories", force: :cascade do |t|
    t.bigint "delivery_fee_id"
    t.integer "label", default: 0
    t.float "value", default: 0.0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["delivery_fee_id"], name: "index_delivery_fee_histories_on_delivery_fee_id"
  end

  create_table "delivery_fees", force: :cascade do |t|
    t.boolean "enabled", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "delivery_numbers", force: :cascade do |t|
    t.bigint "branch_id"
    t.string "number"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["branch_id"], name: "index_delivery_numbers_on_branch_id"
  end

  create_table "favorite_meals", force: :cascade do |t|
    t.integer "customer_id"
    t.integer "meal_id"
    t.string "meal_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "featured_meals", force: :cascade do |t|
    t.string "featureable_type"
    t.bigint "featureable_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["featureable_type", "featureable_id"], name: "index_featured_meals_on_featureable_type_and_featureable_id"
  end

  create_table "home_contents", force: :cascade do |t|
    t.string "name"
    t.integer "sequence_id"
    t.string "groupable_type"
    t.bigint "groupable_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "availability"
    t.string "image_source_app"
    t.string "image_source_web"
    t.index ["groupable_type", "groupable_id"], name: "index_home_contents_on_groupable_type_and_groupable_id"
  end

  create_table "meal_options", force: :cascade do |t|
    t.string "name"
    t.string "long_name"
    t.string "code_name"
    t.string "poscode"
    t.float "price"
    t.string "size"
    t.string "classification"
    t.integer "group_id"
    t.string "image_source"
    t.boolean "is_default", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "displayed_at"
  end

  create_table "news", force: :cascade do |t|
    t.string "title"
    t.string "image_source"
    t.text "description"
    t.datetime "start_date"
    t.datetime "displayed_at"
    t.integer "category"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "notifications", force: :cascade do |t|
    t.string "title"
    t.text "description"
    t.string "short_description"
    t.string "image_source"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "category"
  end

  create_table "orders", force: :cascade do |t|
    t.bigint "cart_id"
    t.string "payment_type"
    t.string "reference_number"
    t.integer "delivery_address_id"
    t.string "order_id"
    t.string "platform"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "delivery_fee_history_id"
    t.index ["cart_id"], name: "index_orders_on_cart_id"
    t.index ["delivery_fee_history_id"], name: "index_orders_on_delivery_fee_history_id"
  end

  create_table "product_bundles", force: :cascade do |t|
    t.integer "alacarte_id"
    t.integer "max_fries", default: 0, null: false
    t.integer "max_drinks", default: 0, null: false
    t.integer "max_rice", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "product_categories", force: :cascade do |t|
    t.integer "category_id"
    t.integer "categorizeable_id"
    t.string "categorizeable_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "product_options", force: :cascade do |t|
    t.integer "choice_id"
    t.integer "section_id"
    t.integer "meal_id"
    t.string "meal_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "promos", force: :cascade do |t|
    t.string "name"
    t.string "category"
    t.string "link"
    t.string "tags"
    t.string "image_source"
    t.boolean "is_active", default: false, null: false
    t.boolean "is_published", default: false, null: false
    t.datetime "validity_start_at"
    t.datetime "validity_end_at"
    t.text "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "schedules", force: :cascade do |t|
    t.string "title"
    t.time "start_at"
    t.time "end_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "sections", force: :cascade do |t|
    t.string "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "upsells", force: :cascade do |t|
    t.bigint "alacarte_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["alacarte_id"], name: "index_upsells_on_alacarte_id"
  end

  create_table "user_manuals", force: :cascade do |t|
    t.string "image_source"
    t.string "description"
    t.integer "sequence_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "first_name", default: ""
    t.string "middle_name", default: ""
    t.string "last_name", default: ""
    t.string "salutation", default: ""
    t.string "user_id"
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "avatar"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "value_meals", force: :cascade do |t|
    t.integer "alacarte_id"
    t.integer "category_id"
    t.string "name"
    t.string "legacy_sections"
    t.integer "schedule_id"
    t.integer "order_id"
    t.boolean "discounted_rice"
    t.boolean "is_breakfast"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "displayed_at"
    t.string "image_source"
    t.datetime "active_at"
    t.string "availability"
    t.integer "sequence_id"
  end

  create_table "variances", force: :cascade do |t|
    t.string "menu_name"
    t.string "code_name"
    t.string "name"
    t.string "poscode"
    t.float "price"
    t.integer "variable_id"
    t.string "variable_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "image_source"
    t.integer "group_id"
    t.integer "best_with"
  end

  add_foreign_key "branches", "areas"
  add_foreign_key "carts", "customers"
  add_foreign_key "carts_variances", "carts"
  add_foreign_key "carts_variances", "variances"
  add_foreign_key "carts_variances_meal_options", "carts_variances"
  add_foreign_key "carts_variances_meal_options", "meal_options"
  add_foreign_key "delivery_fee_histories", "delivery_fees"
  add_foreign_key "delivery_numbers", "branches"
  add_foreign_key "orders", "carts"
  add_foreign_key "orders", "delivery_fee_histories"
end
