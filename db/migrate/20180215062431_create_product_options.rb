class CreateProductOptions < ActiveRecord::Migration[5.1]
  def change
    create_table :product_options do |t|
      t.integer :choice_id
      t.integer :section_id
      t.integer :meal_id
      t.string :meal_type
      t.timestamps
    end
  end
end
