class CreateNotifications < ActiveRecord::Migration[5.1]
  def change
    create_table :notifications do |t|
      t.string :title
      t.text   :description
      t.string :short_description
      t.string :image_source
      t.timestamps
    end
  end
end
