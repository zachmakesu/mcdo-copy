class CreateMealOptions < ActiveRecord::Migration[5.1]
  def change
    create_table :meal_options do |t|
      t.string :name
      t.string :long_name
      t.string :code_name
      t.string :poscode
      t.float :price
      t.string :size
      t.string :classification
      t.integer :group_id
      t.string :image_source
      t.boolean :is_default

      t.timestamps
    end
  end
end
