class CreateHomeContents < ActiveRecord::Migration[5.1]
  def change
    create_table :home_contents do |t|
      t.string     :name
      t.integer    :sequence_id
      t.string     :image_source
      t.references :groupable, polymorphic: true
      t.timestamps
    end
  end
end
