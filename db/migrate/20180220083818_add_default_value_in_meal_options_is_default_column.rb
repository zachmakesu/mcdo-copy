class AddDefaultValueInMealOptionsIsDefaultColumn < ActiveRecord::Migration[5.1]
  def self.up
    change_column :meal_options, :is_default, :boolean, null: false, default: false
  end

  def self.down
    change_column :meal_options, :is_default, :boolean
  end
end
