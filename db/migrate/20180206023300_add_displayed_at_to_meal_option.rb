class AddDisplayedAtToMealOption < ActiveRecord::Migration[5.1]
  def change
    add_column :meal_options, :displayed_at, :datetime
  end
end
