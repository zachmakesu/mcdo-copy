class CreateDeliveryNumbers < ActiveRecord::Migration[5.1]
  def change
    create_table :delivery_numbers do |t|
      t.references :branch, foreign_key: true
      t.string :number

      t.timestamps
    end
  end
end
