class AddIsActiveAndAvailabilityInValueMealsTable < ActiveRecord::Migration[5.1]
  def change
    add_column :value_meals, :active_at, :datetime
    add_column :value_meals, :availability, :string
  end
end
