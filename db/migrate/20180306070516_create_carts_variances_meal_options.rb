class CreateCartsVariancesMealOptions < ActiveRecord::Migration[5.1]
  def change
    create_table :carts_variances_meal_options do |t|
      t.references :carts_variance, foreign_key: true
      t.references :meal_option, foreign_key: true
      t.integer    :quantity, default: 1

      t.timestamps
    end
  end
end
