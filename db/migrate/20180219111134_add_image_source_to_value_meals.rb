class AddImageSourceToValueMeals < ActiveRecord::Migration[5.1]
  def change
    add_column :value_meals, :image_source, :string
  end
end
