class CreateAvailableHours < ActiveRecord::Migration[5.1]
  def change
    create_table :available_hours do |t|
      t.string :name
      t.timestamps
    end
  end
end
