class AddReferenceDeliveryFeeHistoryToOrder < ActiveRecord::Migration[5.1]
  def change
    add_reference :orders, :delivery_fee_history, foreign_key: true
  end
end
