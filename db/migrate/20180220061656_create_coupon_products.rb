class CreateCouponProducts < ActiveRecord::Migration[5.1]
  def change
    create_table :coupon_products do |t|
      t.references :coupon
      t.references :productable, polymorphic: true, index: true
      t.references :main_product
      t.timestamps
    end
  end
end
