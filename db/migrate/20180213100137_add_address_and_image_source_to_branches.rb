class AddAddressAndImageSourceToBranches < ActiveRecord::Migration[5.1]
  def change
    add_column :branches, :image_source, :string
    add_column :branches, :address, :text
  end
end
