class CreateUserManuals < ActiveRecord::Migration[5.1]
  def change
    create_table :user_manuals do |t|
      t.string :image_source
      t.string :description
      t.integer :sequence_id
      t.timestamps
    end
  end
end
