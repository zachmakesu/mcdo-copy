class AddOfferingsColumnInBranchModel < ActiveRecord::Migration[5.1]
  def change
    add_column :branches, :offerings, :string
  end
end
