class RemoveDuplicateMealIdOnValueMeals < ActiveRecord::Migration[5.1]
  def up
    remove_column :value_meals, :meal_id
  end

  def down
    add_column :value_meals, :meal_id, :integer
  end
end
