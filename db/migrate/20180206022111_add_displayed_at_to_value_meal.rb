class AddDisplayedAtToValueMeal < ActiveRecord::Migration[5.1]
  def change
    add_column :value_meals, :displayed_at, :datetime
  end
end
