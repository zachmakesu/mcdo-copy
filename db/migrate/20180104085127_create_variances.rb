class CreateVariances < ActiveRecord::Migration[5.1]
  def change
    create_table :variances do |t|
      t.string :menu_name
      t.string :code_name
      t.string :name
      t.string :poscode
      t.float :price
      t.integer :variable_id
      t.string :variable_type

      t.timestamps
    end
  end
end
