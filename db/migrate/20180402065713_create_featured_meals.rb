class CreateFeaturedMeals < ActiveRecord::Migration[5.1]
  def change
    create_table :featured_meals do |t|
      t.references :featureable, polymorphic: true, index: true
      t.timestamps
    end
  end
end
