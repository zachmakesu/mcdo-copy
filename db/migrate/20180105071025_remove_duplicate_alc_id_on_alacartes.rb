class RemoveDuplicateAlcIdOnAlacartes < ActiveRecord::Migration[5.1]
  def up
    remove_column :alacartes, :alc_id
  end
  def down
    add_column :alacartes, :alc_id, :integer
  end
end
