class UpdateHomeContentMigration < ActiveRecord::Migration[5.1]
  def self.up
    add_column :home_contents, :availability, :string
    add_column :home_contents, :image_source_app, :string
    add_column :home_contents, :image_source_web, :string
    remove_column :home_contents, :image_source
  end

  def self.down
    add_column :home_contents, :image_source, :string
    remove_column :home_contents, :availability
    remove_column :home_contents, :image_source_app
    remove_column :home_contents, :image_source_web
  end
end
