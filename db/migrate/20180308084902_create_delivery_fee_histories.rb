class CreateDeliveryFeeHistories < ActiveRecord::Migration[5.1]
  def change
    create_table :delivery_fee_histories do |t|
      t.references :delivery_fee, foreign_key: true
      t.integer :label, default: 0
      t.float :value, default: 0

      t.timestamps
    end
  end
end
