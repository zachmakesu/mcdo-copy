class CreateProductCategories < ActiveRecord::Migration[5.1]
  def change
    create_table :product_categories do |t|
      t.integer :category_id
      t.integer :categorizeable_id
      t.string :categorizeable_type
      t.timestamps
    end
  end
end
