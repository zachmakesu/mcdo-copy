class CreateOrders < ActiveRecord::Migration[5.1]
  def change
    create_table :orders do |t|
      t.references  :cart, foreign_key: true
      t.string      :payment_type
      t.string      :reference_number
      t.integer     :delivery_address_id
      t.string      :order_id
      t.string      :platform

      t.timestamps
    end
  end
end
