class RenameImageToImageSourceFromVariances < ActiveRecord::Migration[5.1]
  def change
    rename_column :variances, :image, :image_source
  end
end
