class CreateNews < ActiveRecord::Migration[5.1]
  def change
    create_table :news do |t|
      t.string :title
      t.string :image_source
      t.text :description
      t.datetime :start_date
      t.datetime :displayed_at
      t.integer :category
      t.timestamps
    end
  end
end
