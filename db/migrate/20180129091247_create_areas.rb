class CreateAreas < ActiveRecord::Migration[5.1]
  def change
    create_table :areas do |t|
      t.string :name
      t.text :description, default: ""

      t.timestamps
    end
  end
end
