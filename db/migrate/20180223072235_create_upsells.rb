class CreateUpsells < ActiveRecord::Migration[5.1]
  def change
    create_table :upsells do |t|
      t.references :alacarte
      t.timestamps
    end
  end
end
