class CreateCartsVariances < ActiveRecord::Migration[5.1]
  def change
    create_table :carts_variances do |t|
      t.references  :cart, foreign_key: true
      t.references  :variance, foreign_key: true
      t.integer     :quantity, default: 1
      t.boolean     :best_with, default: false
      t.integer     :best_with_quantity, default: 0

      t.timestamps
    end
  end
end
