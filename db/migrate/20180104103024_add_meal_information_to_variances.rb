class AddMealInformationToVariances < ActiveRecord::Migration[5.1]
  def change
    add_column :variances, :image, :string
    add_column :variances, :group_id, :integer
    add_column :variances, :best_with, :integer
  end
end
