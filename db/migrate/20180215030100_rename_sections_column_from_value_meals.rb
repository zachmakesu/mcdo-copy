class RenameSectionsColumnFromValueMeals < ActiveRecord::Migration[5.1]
  def change
    rename_column :value_meals, :sections, :legacy_sections
  end
end
