class CreateDeliveryFees < ActiveRecord::Migration[5.1]
  def change
    create_table :delivery_fees do |t|
      t.boolean :enabled, default: true

      t.timestamps
    end
  end
end
