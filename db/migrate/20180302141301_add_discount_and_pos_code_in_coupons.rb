class AddDiscountAndPosCodeInCoupons < ActiveRecord::Migration[5.1]
  def change
    add_column :coupons, :discount, :float
    add_column :coupons, :poscode, :string
  end
end
