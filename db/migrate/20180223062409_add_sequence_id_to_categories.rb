class AddSequenceIdToCategories < ActiveRecord::Migration[5.1]
  def change
    add_column :categories, :sequence_id, :integer
  end
end
