class RenameSectionsColumnFromAlacartes < ActiveRecord::Migration[5.1]
  def change
    rename_column :alacartes, :sections, :legacy_sections
  end
end
