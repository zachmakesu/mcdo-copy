class CreateValueMeals < ActiveRecord::Migration[5.1]
  def change
    create_table :value_meals do |t|
      t.integer :meal_id
      t.integer :alacarte_id
      t.integer :category_id
      t.string :name
      t.string :sections
      t.integer :schedule_id
      t.integer :order_id
      t.boolean :discounted_rice
      t.boolean :is_breakfast

      t.timestamps
    end
  end
end
