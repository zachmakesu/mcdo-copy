class AddCategoryInNotificationTable < ActiveRecord::Migration[5.1]
  def self.up
    add_column :notifications, :category, :string
  end

  def self.down
    remove_column :notifications, :category
  end
end
