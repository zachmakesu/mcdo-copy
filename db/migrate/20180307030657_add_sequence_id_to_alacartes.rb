class AddSequenceIdToAlacartes < ActiveRecord::Migration[5.1]
  def change
    add_column :alacartes, :sequence_id, :integer
  end
end
