class AddDisplayedAtToAlacarte < ActiveRecord::Migration[5.1]
  def change
    add_column :alacartes, :displayed_at, :datetime
  end
end
