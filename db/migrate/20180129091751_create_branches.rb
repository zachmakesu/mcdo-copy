class CreateBranches < ActiveRecord::Migration[5.1]
  def change
    create_table :branches do |t|
      t.references :area, foreign_key: true
      t.string :name
      t.text :description, default: ""
      t.string :status

      t.timestamps
    end
  end
end
