class CreateProductBundles < ActiveRecord::Migration[5.1]
  def change
    create_table :product_bundles do |t|
      t.integer :alacarte_id
      t.integer :max_fries, null: false, default: 0
      t.integer :max_drinks, null: false, default: 0
      t.integer :max_rice, null: false, default: 0
      t.timestamps
    end
  end
end
