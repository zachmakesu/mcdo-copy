class AddLatitudeAndLongitudeToBranches < ActiveRecord::Migration[5.1]
  def change
    add_column :branches, :lat, :float
    add_column :branches, :lng, :float
  end
end
