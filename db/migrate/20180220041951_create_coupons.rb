class CreateCoupons < ActiveRecord::Migration[5.1]
  def change
    create_table :coupons do |t|
      t.string    :name
      t.string    :classification
      t.string    :usage_type
      t.text      :description
      t.bigint    :usage_count
      t.datetime  :validity_start_at
      t.datetime  :validity_end_at
      t.string    :image_source
      t.string    :detail_image_source
      t.timestamps
    end
  end
end
