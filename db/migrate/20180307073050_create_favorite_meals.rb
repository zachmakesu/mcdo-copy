class CreateFavoriteMeals < ActiveRecord::Migration[5.1]
  def change
    create_table :favorite_meals do |t|
      t.integer :customer_id
      t.integer :meal_id
      t.string :meal_type
      t.timestamps
    end
  end
end
