class CreateAlacartes < ActiveRecord::Migration[5.1]
  def change
    create_table :alacartes do |t|
      t.integer :alc_id
      t.integer :category_id
      t.string :name
      t.integer :group_id
      t.string :sections
      t.string :image_source
      t.integer :best_with
      t.text :description
      t.integer :schedule_id
      t.integer :product_order_id
      t.boolean :discounted_rice

      t.timestamps
    end
  end
end
