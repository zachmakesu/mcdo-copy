class CreatePromos < ActiveRecord::Migration[5.1]
  def change
    create_table :promos do |t|
      t.string    :name
      t.string    :category
      t.string    :link
      t.string     :tags
      t.string     :image_source
      t.boolean   :is_active , null: false, default: :false
      t.boolean   :is_published , null: false, default: :false
      t.datetime :validity_start_at
      t.datetime :validity_end_at
      t.text      :content
      t.timestamps
    end
  end
end
