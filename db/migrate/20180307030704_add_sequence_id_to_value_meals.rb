class AddSequenceIdToValueMeals < ActiveRecord::Migration[5.1]
  def change
    add_column :value_meals, :sequence_id, :integer
  end
end
