models_to_seed = %w[users categories sections schedules meal_options alacartes value_meals delivery_numbers available_hours delivery_fees]
models_to_seed.each do |model|
  require_relative "models/#{model}"
end
