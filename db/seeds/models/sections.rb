if Section.count.zero?
  sections = %w[
    Beverage
    Fries
    Burger
    Dessert
    Sauce
    Best\ With
    Special\ Fries
    Sides
  ]

  puts "Start seeding of Section model"

  sections.each do |section|
    Section.create(title: section)
    print "."
  end
  puts "DONE"
else
  puts "Skipped seeding of Section model."
end
puts "Finished seeding the Section Model"
