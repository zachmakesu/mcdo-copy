if Category.count.zero?
  file = File.read(Rails.root.join("db", "seeds", "json", "products_sync.json"))
  data = JSON.parse(file)
  menus = data.fetch("menu")
  puts "Start seeding of Category model."
  ActiveRecord::Base.transaction do
    menus.each.with_index(1) do |menu, index|
      valid_category = Category.where(category_id: menu["category_id"].to_i).count.zero? && menu["category_name"].present?
      next unless valid_category
      next if menu["category_id"].to_i == 22
      menu["category_name"] = case menu["category_id"].to_i
                              when 11 then "Fries 'N McFloat Combos"
                              when 4 then "Breakfast"
                              else menu["category_name"]
                              end
      c = Category.new
      c.id          = menu["category_id"].to_i
      c.name        = menu["category_name"]
      c.category_id = menu["category_id"].to_i
      c.sequence_id = index
      if c.save
        print "."
      else
        puts "ERROR: #{c.errors.full_messages.to_sentence}"
      end
    end
    category_names = ["McShare Box", "McSaver Meals", "Extra Value Meal", "A la carte"]
    max_id = Category.maximum(:id)
    max_sequence_id = Category.maximum(:sequence_id)
    category_names.each.with_index(1) do |name, index|
      c             = Category.new
      c.id          = max_id + index
      c.sequence_id = max_sequence_id + index
      c.name = name
      if c.save
        print "."
      else
        puts "ERROR: #{c.errors.full_messages.to_sentence}"
        puts "Aborted seeding of Category model."
        break
      end
    end
  end
  categories = Category.all
  categories.each do |category|
    id = case category.name
         when "McShare Box" then 1
         when "McSaver Meals" then 3
         when "Extra Value Meal" then 3
         when "Burger" then 4
         when "Chicken" then 5
         when "Fries 'N McFloat Combos" then 6
         when "A la carte" then 7
         when "Drink" then 8
         when "Dessert" then 9
         when "Breakfast" then 10
         when "Happy Meals" then 11
         when "Add ons" then 12
         end
    category.update(sequence_id: id)
  end
  puts "DONE"
else
  puts "Skipped seeding of Category model."
end
puts "Finished seeding the Category model."
