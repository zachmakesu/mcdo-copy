if !DeliveryFee.exists?
  puts "Start seeding of Delivery Fee Model with default fee"
  if d = DeliveryFee.create
    puts "Finished seeding the Delivery Fees model"
  else
    puts "ERROR: #{d.errors.full_messages.to_sentence}"
    puts "Aborted seeding of Delivery Fee"
  end
else
  puts "Skipped seeding of Delivery Fee"
end

if !DeliveryFeeHistory.custom.exists?
  puts "Start seeding of Delivery Fee History Model (Custom)"
  if d = DeliveryFee.first.history.create(value: 40)
    puts "Finished seeding the Delivery Fee History Model (Custom)"
  else
    puts "ERROR: #{d.errors.full_messages.to_sentence}"
    puts "Aborted seeding of Delivery Fee History (Custom)"
  end
else
  puts "Skipped seeding of Delivery Fee History (Custom)"
end

if !DeliveryFeeHistory.zero_fee.exists?
  puts "Start seeding of Delivery Fee History Model (Zero Fee)"
  if d = DeliveryFee.first.history.zero_fee.create
    puts "Finished seeding the Delivery Fee History Model (Zero Fee)"
  else
    puts "ERROR: #{d.errors.full_messages.to_sentence}"
    puts "Aborted seeding of Delivery Fee History (Zero Fee)"
  end
else
  puts "Skipped seeding of Delivery Fee History (Zero Fee)"
end
