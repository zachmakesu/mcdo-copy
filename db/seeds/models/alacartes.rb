require 'net/http'

if Alacarte.count.zero?
  file = File.read(Rails.root.join("db", "seeds", "json", "products_sync.json"))
  data = JSON.parse(file)
  menus = data.fetch("menu")
  puts "Start seeding of Alacarte model."
  def image_source_valid?(image_source)
    begin
      case Net::HTTP.get_response(URI.parse(image_source))
        when Net::HTTPSuccess then true
        else false
      end
    rescue => e
      puts e
      false
    end
  end
  ActiveRecord::Base.transaction do
    menus.each.with_index(1) do |menu, index|
      a                             = Alacarte.new
      a.id                          = menu["alc_id"].to_i
      a.category_id                 = menu["category_id"].to_i
      a.name                        = menu["product_name"]
      a.group_id                    = menu["group_id"].to_i
      a.legacy_sections             = menu["sections"]
      a.remote_image_source_url     = menu["image_source"] if image_source_valid?(menu["image_source"])
      a.best_with                   = menu["best_with"].to_i
      a.description                 = menu["description"]
      a.schedule_id                 = menu["schedule_id"]
      a.product_order_id            = menu["product_order_id"]
      a.discounted_rice             = menu["discount_rice"].to_i
      a.displayed_at                = Date.today
      a.sequence_id                 = index

      if a.save
        variance_keys      = menu["menu_name"].keys
        variance_keys.each do |variance|
          v                         = a.variances.build
          v.name                    = variance
          v.code_name               = menu["code_name"][variance]
          v.menu_name               = menu["menu_name"][variance]
          v.poscode                 = menu["poscode"][variance]
          v.price                   = menu["price"][variance]
          v.remote_image_source_url = menu["image_source"] if image_source_valid?(menu["image_source"])
          if v.save
            sections = menu["sections"].split(",")
            sections.each do |section|
              title_id = case section
                         when "F" then Section.fries.id
                         when "D" then Section.beverage.id
                         when "B" then Section.burger.id
                         when "S" then Section.sauce.id
                         when "I" then Section.dessert.id
                         else
                           @section = Section.create(title: section)
                           @section.id
                         end
              legacy_title = case section
                             when "F" then "fries"
                             when "D" then "drink"
                             when "S" then "sauce"
                             end
              meal_options = MealOption.where(group_id: a.group_id, classification: legacy_title)
              meal_options.each do |meal|
                o             = v.options.build
                o.choice_id   = meal.id
                o.section_id  = title_id
                o.save
              end
            end
          end
        end
        c = a.product_categories.build
        c.category_id = case a.category_id.to_i
                        when 22 then Category.fries_and_float.id
                        when 10 then Category.dessert.id
                        else a.category_id.to_i
                        end
        if c.save
          keywords =
            {
              "fries n" => Category.fries_and_float.id,
              "mcshare" => Category.mcshare.id,
              "bundle" => Category.mcshare.id,
              "10-pc. McNuggets a la carte" => Category.mcshare.id,
              "20pc McNuggets a la carte" => Category.mcshare.id,
              "mcsaver" => Category.mcsaver.id,
              "happy meal" => Category.happy_meal.id,
              "carte" => Category.alacarte.id
            }
          keywords.each_pair do |name, id|
            a.product_categories.create(category_id: id) if a.name.downcase.include?(name.downcase)
          end
          bundle_category = Category.mcshare
          if a.categories.include?(bundle_category)
            # 318, 369 bundle for 3
            # 319 370, 371 bundle for 4
            max_drinks = case a.id
                         when 318, 369 then 3
                         when 319, 370, 371 then 4
                         else 0
                         end
            max_fries = case a.id
                        when 318, 369 then 1
                        when 319, 370, 371 then 2
                        else 0
                        end
            max_rice = case a.id
                       when 318, 369 then 3
                       when 319, 370, 371 then 4
                       else 0
                       end
            pb = ProductBundle.create(alacarte_id: a.id, max_fries: max_fries, max_drinks: max_drinks, max_rice: max_rice)
            drink_options = MealOption.where(group_id: 1)
            drink_options.each do |drink|
              pbo = pb.options.build
              pbo.choice_id = drink.id
              pbo.section_id = Section.beverage.id
              pbo.save
            end
            #creating of bff fries as productOption
            pb.options.create(choice_id: 331, section_id: Section.fries.id)
          # seed product bundle for fries 'n mcfloat bundle
          # 149 bff bundle and 153 is medium fries n mcfloat
          elsif [148, 153].include?(a.id)
            max_drinks = case a.id
                         when 148 then 3
                         else 1
                         end
            pb = ProductBundle.create(alacarte_id: a.id, max_fries: 1, max_drinks: max_drinks, max_rice: 0)
            mcfloats_ids = [12, 13, 408, 409]
            mcfloats_ids.each do |float_id|
              pb.options.create(choice_id: float_id, section_id: Section.beverage.id)
            end
          end
        end
        print "."
      else
        puts "ERROR: #{a.errors.full_messages.to_sentence}"
        puts "Aborted seeding of Alacarte model."
        break
      end
    end
  end
  puts "DONE"
else
  puts "Skipped seeding of Alacarte model."
end
puts "Finished seeding the Alacarte model."
