require "net/http"

if MealOption.count.zero?
  file = File.read(Rails.root.join("db", "seeds", "json", "products_sync.json"))
  data = JSON.parse(file)
  options = data.fetch("options")
  puts "Start seeding of MealOption model."
  def image_source_valid?(image_source)
    begin
      case Net::HTTP.get_response(URI.parse(image_source))
        when Net::HTTPSuccess then true
        else false
      end
    rescue => e
      puts e
      false
    end
  end
  ActiveRecord::Base.transaction do
    options.each_pair do |classification, meal_options|
      meal_options.each do |option|
        mo                          = MealOption.new
        mo.id                       = option["option_id"].to_i
        mo.long_name                = option["option_name"]
        mo.name                     = option["product_name"]
        mo.code_name                = option["item_code"]
        mo.poscode                  = option["poscode"]
        mo.price                    = option["price"].to_f
        mo.size                     = option["size"]
        mo.classification           = option["type"]
        mo.group_id                 = option["group_id"]
        mo.remote_image_source_url  = option["image"] if image_source_valid?(option["image"])
        mo.is_default               = option["is_default"].to_i
        mo.displayed_at             = Date.today

        if mo.save
          print "."
        else
          puts "ERROR: #{mo.errors.full_messages.to_sentence}"
          puts "Aborted seeding of MealOption model."
          break
        end
      end
    end
  end
  puts "DONE"
else
  puts "Skipped seeding of MealOption model."
end
puts "Finished seeding the MealOption model."
