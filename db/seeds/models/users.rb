users = [
  "dennis@gorated.com",
  "isaac@gorated.ph",
  "jaime@gorated.ph",
  "kevinc@gorated.ph",
  "mcdave@gorated.ph",
  "peterjohn@gorated.ph",
  "ragde@gorated.ph",
  "rommel@gorated.com",
  "zhiena@gorated.ph"
]
if User.count.zero?
  puts "Start seeding of User model."
  users.each do |user|
    u = User.new
    u.email = user
    u.password = "golabs2018"
    if u.save
      print "."
    else
      puts "ERROR: #{u.errors.full_messages.to_sentence}"
      puts "Aborted seeding of User model."
      break
    end
  end
  puts "DONE"
else
  puts "Skipped seeding of User model."
end
puts "Finished seeding the User model."
