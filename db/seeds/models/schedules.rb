if Schedule.count.zero?
  schedules = [
    {
      id: 1,
      start: "4:00",
      end: "10:30"
    },
    {
      id: 2,
      start: "00:00:01",
      end: "23:59:59"
    },
    {
      id: 3,
      start: "5:00",
      end: "2:00"
    },
    {
      id: 4,
      start: "10:30",
      end: "4:00"
    }
  ]

  def parse_time(time)
    Time.parse(time).strftime("%H:%M:%S")
  end

  puts "Start seeding of Schedule model"

  schedules.each do |schedule|
    s = schedule[:start]
    e = schedule[:end]
    Schedule.create(id: schedule[:id], title: "#{s} - #{e}", start_at: parse_time(s), end_at: parse_time(e))
    print "."
  end
  puts "DONE"
else
  puts "Skipped seeding of Schedule model."
end
puts "Finished seeding the Schedule Model"
