if ValueMeal.count.zero?
  file = File.read(Rails.root.join("db", "seeds", "json", "products_sync.json"))
  data = JSON.parse(file)
  meals = data.fetch("meals")
  puts "Start seeding of ValueMeal model."
  def image_source_valid?(image_source)
    begin
      case Net::HTTP.get_response(URI.parse(image_source))
        when Net::HTTPSuccess then true
        else false
      end
    rescue => e
      puts e
      false
    end
  end
  ActiveRecord::Base.transaction do
    meals.each.with_index(1) do |meal, index|
      vm                          = ValueMeal.new
      vm.id                       = meal["meal_id"].to_i
      vm.alacarte_id              = meal["alacarte_id"].to_i
      vm.category_id              = meal["category_id"].to_i
      vm.name                     = meal["product_name"]
      vm.legacy_sections          = meal["sections"]
      vm.remote_image_source_url  = meal["images"]["small"] if image_source_valid?(meal["images"]["small"])
      vm.schedule_id              = meal["schedule_id"]
      vm.order_id                 = meal["order_id"]
      vm.discounted_rice          = meal["discount_rice"].to_i
      vm.displayed_at             = Date.today
      vm.sequence_id              = index

      if vm.save
        variance_keys    = meal["code_name"].keys
        variance_keys.each do |variance|
          v                         = vm.variances.build
          v.name                    = variance
          v.best_with               = meal["best_with"][variance]
          v.code_name               = meal["code_name"][variance]
          v.group_id                = meal["group_id"][variance]
          v.remote_image_source_url = meal["images"][variance] if image_source_valid?(meal["images"][variance])
          v.poscode                 = meal["poscode"][variance]
          v.price                   = meal["price"][variance]
          if v.save
            sections = meal["sections"].split(",")
            sections.each do |section|
              title_id = case section
                         when "F" then Section.fries.id
                         when "D" then Section.beverage.id
                         when "B" then Section.burger.id
                         when "S" then Section.sauce.id
                         when "I" then Section.dessert.id
                         else
                           @section = Section.create(title: section)
                           @section.id
                         end
              legacy_title = case section
                             when "F" then "fries"
                             when "D" then "drink"
                             when "S" then "sauce"
                             end
              meal_options = MealOption.where(group_id: v.group_id, classification: legacy_title)
              meal_options.each do |meal|
                o             = v.options.build
                o.choice_id   = meal.id
                o.section_id  = title_id
                o.save
              end
            end
          end
        end
        c = vm.product_categories.build
        c.category_id = case vm.category_id.to_i
                        when 22 then Category.fries_and_float.id
                        when 10 then Category.dessert.id
                        else vm.category_id.to_i
                        end
        if c.save
          keywords =
            {
              "fries n" => Category.fries_and_float.id,
              "mcshare" => Category.mcshare.id,
              "mcsaver" => Category.mcsaver.id,
              "happy meal" => Category.happy_meal.id,
              "carte" => Category.alacarte.id
            }
          keywords.each_pair do |name, id|
            vm.product_categories.create(category_id: id) if vm.name.downcase.include?(name.downcase)
          end
        end
        print "."
      else
        puts "ERROR: #{vm.errors.full_messages.to_sentence}"
        puts "Aborted seeding of ValueMeal model."
        break
      end
    end
  end
  puts "DONE"
else
  puts "Skipped seeding of ValueMeal model."
end
puts "Finished seeding the ValueMeal model."
