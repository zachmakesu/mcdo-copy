hours = [
  "regular",
  "breakfast",
]
if AvailableHour.count.zero?
  puts "Start seeding of Availale Hour model."
  hours.each do |hour|
    h = AvailableHour.new
    h.name = hour
    if h.save
      print "."
    else
      puts "ERROR: #{h.errors.full_messages.to_sentence}"
      puts "Aborted seeding of Available Hour model."
      break
    end
  end
  puts "DONE"
else
  puts "Skipped seeding of Available Hour model."
end
puts "Finished seeding the Available Hour model."
