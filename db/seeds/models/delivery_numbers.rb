def tz(date:)
  tz = "Singapore"
  ActiveSupport::TimeZone[tz].parse(date)
end

if DeliveryNumber.count.zero?
  file = File.read(Rails.root.join("db", "seeds", "json", "app_delivery_numbers.json"))
  data = JSON.parse(file)
  areas = data.fetch("data")
  puts "Start seeding of Area, Branch, and DeliveryNumber model."
  ActiveRecord::Base.transaction do
    areas.each do |a|
      area = Area.new
      area.id = a["area_id"]
      area.name = a["area_name"]
      area.description = a["description"]
      area.created_at = tz(date: a["created_at"])
      area.updated_at = tz(date: a["updated_at"])
      if area.save
        a["stores"].each do |store|
          branch = area.branches.build
          branch.id = store["store_id"]
          branch.address = store["address"]
          branch.offerings = store["offerings"]&.join(",")
          branch.name = store["store_name"]
          branch.description = store["description"]
          branch.status = store["status"]
          branch.created_at = tz(date: store["created_at"])
          branch.updated_at = tz(date: store["updated_at"])
          if branch.save
            store["delivery_numbers"].each do |n|
              number = branch.delivery_numbers.build
              number.id = n["number_id"]
              number.number = n["number"]
              number.created_at = tz(date: n["created_at"])
              number.updated_at = tz(date: n["updated_at"])
              if number.save
                print "."
              end
            end
          end
        end
      else
        puts "ERROR: #{area.errors.full_messages.to_sentence}"
        puts "Aborted seeding of DeliveryNumber model."
        break
      end
    end
  end
  puts "DONE"
else
  puts "Skipped seeding of Area, Branch, and DeliveryNumber model."
end
puts "Finished seeding of Area, Branch, and DeliveryNumber model."
