if %i[development? staging?].any? { |e| Rails.env.send(e) }
  GraphiQL::Rails.config.headers["Authorization"] = ->(context) { "Bearer #{context.cookies['_graphiql_token']}" }
end
