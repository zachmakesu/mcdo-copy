require "exception_notification/rails"

ExceptionNotification.configure do |config|
  unless Rails.env.development? || Rails.env.test?
    email_hash =
      {
        email_prefix: "[ERROR] McDo (#{Rails.env})",
        sender_address: "Notifier <gorated.mcdo.staging@gmail.com>",
        exception_recipients: %w[kevinc@gorated.ph redjoker011@gmail.com jaime@gorated.ph rommel@gorated.com]
      }
    config.add_notifier :email, email_hash
  end
end
