class MenuTabBuilder < TabsOnRails::Tabs::Builder
  def open_tabs(options={})
    @context.tag("ul", options, true)
  end

  def close_tabs(*)
    "</ul>"
  end

  def tab_for(tab, name, options, item_options={})
    item_class = item_options[:class]
    item_options[:class] = (current_tab?(tab) ? "#{item_class} is-active #{'active-meal-menu' if item_options[:nav_for] == 'meals'}" : item_class.to_s)
    if item_options[:tab_type] == "menu"
      @context.link_to((@context.image_tag(item_options[:image].to_s, class: "menu-item--side__icon") + name), options, item_options)
    else
      @context.link_to(name, options, item_options)
    end
  end
end
