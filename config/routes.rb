Rails.application.routes.draw do
  mount Sidekiq::Web => "/sidekiq" # monitoring console
  post "/graphql", to: "graphql#execute"

  devise_for :users, skip: %i[passwords registrations], controllers: {
    sessions: "admin/sessions"
  }

  authenticate :user do
    root "admin#index"
    namespace :admin do
      resources :products, only: :index do
        collection do
          get "fetch_by_type", as: "fetch_products_by_type", to: "products#fetch_by_type"
          get "featured_meals", to: "products#featured_meals"
        end
        member do
          post   "set_as_featured",   as: "set_meal_as_featured",   to: "products#featured_meal"
          delete "set_as_unfeatured", as: "set_meal_as_unfeatured", to: "products#unfeatured_meal"
        end
      end
      put "value_meals/:id/update_status", as: "update_value_meal_status", to: "value_meals#update_status"
      resources :alacartes
      resources :branches, except: :show
      resources :coupons, except: :show
      resources :delivery_numbers, only: %i[edit update]
      resources :meal_options, except: :show do
        collection do
          get "fetch_sorted_meals", to: "meal_options#fetch_sorted_meals"
        end
      end
      resources :news
      resources :orders, only: :index
      resources :promos, except: :show
      resources :static_pages, only: :index
      resources :upsells, except: :show
      resources :users, except: :show
      resources :value_meals
      resources :coupon_utilizations, only: :index
      resources :variances, except: :show
      resources :notifications, except: :show
      resources :categories, except: :show
      resources :home_contents, except: :show
      resources :user_manuals, except: :show
    end
    if %i[development? staging?].any? { |e| Rails.env.send(e) }
      mount GraphiQL::Rails::Engine, at: "/graphiql", graphql_path: "/graphql"
    end
  end
end
