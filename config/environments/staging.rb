# Staging configuration is identical to production, with some overrides
# for hostname, etc.

require_relative "./production"

Rails.application.configure do
  config.action_mailer.delivery_method = :smtp
  config.action_mailer.smtp_settings =
    {
      address:              "smtp.gmail.com",
      port:                 587,
      domain:               "gmail.com",
      user_name:            ENV["GMAIL_USERNAME"],
      password:             ENV["GMAIL_PASSWORD"],
      authentication:       "plain",
      enable_starttls_auto: true
    }

  config.action_mailer.raise_delivery_errors = true
  config.action_mailer.default_url_options = {
    :host => "mcdo-staging.gorated.ph",
    :protocol => "https"
  }
  config.action_mailer.asset_host = "https://mcdo-staging.gorated.ph"

  config.action_controller.asset_host = "https://mcdo-staging.gorated.ph"

  config.action_controller.default_url_options = {
    :host => "mcdo-staging.gorated.ph",
    :protocol => "https"
  }

  # serve static assets to render carrierwave images
  config.serve_static_files = true
end
