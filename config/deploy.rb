set :application, "mcdo-web"
set :repo_url, "git@bitbucket.org:gorated/mcdo-web.git"

set :rbenv_type, :user # or :system, depends on your rbenv setup
set :rbenv_ruby, File.read(".ruby-version").strip
set :rbenv_map_bins, %w[rake gem bundle ruby rails]

# Project-specific overrides go here.
# For list of variables that can be customized, see:
# https://github.com/mattbrictson/capistrano-mb/blob/master/lib/capistrano/tasks/defaults.rake

fetch(:mb_recipes) << "sidekiq"
fetch(:mb_aptitude_packages)["redis-server@ppa:chris-lea/redis-server"] = :redis

# TODO : Investigate error on mb:bundler:gem_install task
# Manually run bundle install instead of using mb's bundler task
fetch(:mb_recipes).delete("bundler")
append :linked_dirs, ".bundle"

set :linked_dirs, fetch(:linked_dirs) + %w[public/system public/uploads]

set :mb_dotenv_keys, %w[
  cds_api_url
  cds_proxy_api_url
  cds_public_key
  cds_private_key
  cds_host_name
  cds_source_name
  cds_proxy_source_name
  jwt_hmac_secret
  postmark_api_key
  rails_secret_key_base
  redis_url
  sidekiq_web_password
  sidekiq_web_username
  gmail_username
  gmail_password
  googlemap_api_key
]

after "deploy:published", "bundler:clean"
