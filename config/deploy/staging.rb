set :branch, ENV.fetch("CAPISTRANO_BRANCH", "development")
set :mb_sidekiq_concurrency, 1

server "ec2-54-255-138-181.ap-southeast-1.compute.amazonaws.com",
       :user => "deployer",
       :roles => %w[app backup cron db redis sidekiq web]
